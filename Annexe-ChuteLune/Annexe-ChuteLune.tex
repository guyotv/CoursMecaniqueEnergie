\myclearpage

\chapter{Chute de la Lune}\label{chutelunecirculaire}

\section{Introduction}

\lettrine{L}{e calcul} ci-dessous constitue une approche simplifiée du calcul de l'accélération de la Lune basé sur sa chute libre sur la Terre.  Il est valable dans la mesure d'un angle \(\alpha\) très petit. Dans le cas d'un mouvement de rotation de la Lune autour de la Terre d'une durée d'une seconde, il est donc justifié. Mais cela n'enlève rien à la généralité du raisonnement puisque celui-ci est valable pour tout temps petit.

\section{Accélération}\label{chutedelalune}

Considérons la figure \ref{schemachutelune} qui décrit successivement le mouvement en ligne droite d'une Lune qui n'est pas soumise à l'attraction de la Terre, puis sa chute libre vers la Terre pendant le même temps de \SI{1}{\second}. Le mouvement de rotation circulaire de la Lune autour de la Terre est ainsi compris comme un mouvement simultanément balistique (MRU sur AB et MRUA sur BC) et central puisque BC a la direction Terre-Lune. Il s'agit donc d'un modèle plus juste, mais plus complexe, que celui présenté au paragraphe \ref{chutelune}.

\begin{figure}[t]
\caption{Chute de la lune\label{schemachutelune}}
\bigskip
\centering
\input{Annexe-ChuteLune/Images/SchemaChuteLune}
\bigskip
\end{figure}

Pythagore et la géométrie évidente du problème présenté dans la figure \ref{schemachutelune} permettent d'écrire~:
\begin{align*}
BC&=\sqrt{AD^2+AB^2}-CD\\
&=\sqrt{AD^2+AB^2}-AD\\
&=\sqrt{AD^2\cdot (1+\frac{AB^2}{AD^2})}-AD\\
&=AD\cdot \sqrt{1+(\frac{AB}{AD})^2}-AD
\end{align*}
Il faut maintenant faire appel à une opération mathématique qui consiste à traduire une fonction complexe comme la racine carrée en une somme infinie de termes simples. Il s'agit d'un développement limité. Dans le même temps, pour ne pas devoir traiter une somme infinie, il faut effectuer une approximation qui va permettre de ne garder que quelques termes de cette somme. Cette simplification est justifiée par la petitesse du terme \(AB\) comparé à la distance Terre-Lune \(AD\). Plus précisément, on a alors~:
\[\sqrt{1+x}=1+\frac{1}{2}\cdot x-\frac{1}{2\cdot 4}\cdot x^2+\frac{1\cdot 3}{2\cdot 4\cdot 6}\cdot x^3-\dots\]
pour \(|x|\ll 1\). L'approximation consiste à ne retenir que les deux premiers termes~:
\[\sqrt{1+x}\cong1+\frac{1}{2}\cdot x\]
Ainsi, en raison du fait que~:
\[(\frac{AB}{AD})^2\ll 1\]
on peut écrire, à fortiori, la suite du calcul précédent~:
\begin{align*}
BC&=AD\cdot \sqrt{1+(\frac{AB}{AD})^2}-AD\\
&=AD\cdot (1+\frac{1}{2}\cdot (\frac{AB}{AD})^2)-AD\\
&=AD+AD\cdot \frac{1}{2}\cdot (\frac{AB}{AD})^2-AD\\
&=AD\cdot \frac{1}{2}\cdot (\frac{AB}{AD})^2
\end{align*}
Or, on peut calculer la vitesse angulaire sur un temps de \SI{1}{\second} par~:
\[\omega(t=1\,s)=\frac{\alpha}{t}=\frac{\alpha}{1}=\alpha\]
En considérant un angle \(\alpha\) petit, on a d'autre part~:
\[\alpha\cong \tan(\alpha)=\frac{AB}{AD}\]
Ce qui permet d'écrire~:
\[\omega=\frac{AB}{AD}\]
En remplaçant \(AD\) par \(d_{Terre-Lune}\), on peut finalement trouver \(BC\)~:
\begin{equation}\label{equation1}
BC=d_{Terre-Lune}\cdot \frac{1}{2}\cdot \omega^2
\end{equation}
Mais, si on fait l'hypothèse d'une chute libre de la Lune en MRUA pendant une seconde, on a~:
\begin{equation}\label{equation2}
BC=\frac{1}{2}\cdot a\cdot t^2=\frac{1}{2}\cdot a
\end{equation}
Si on considère maintenant le mouvement de rotation de la Lune pendant une période \(T\), soit pendant \SI{27,33}{jours}, sur un tour (\(2\cdot \pi\,rad\)) autour de la Terre, on peut écrire~:
\[\omega=\frac{2\cdot \pi}{T}\]
Et en réunissant alors les équations \ref{equation1} et \ref{equation2}, on a~:
\begin{align*}
\frac{1}{2}\cdot a&=\frac{1}{2}\cdot d_{Terre-Lune}\cdot \omega^2\;\Rightarrow\\
a&=d_{Terre-Lune}\cdot \omega^2=d_{Terre-Lune}\cdot (\frac{2\cdot \pi}{T})^2\\
&=3,844\cdot 10^8\cdot (\frac{2\cdot \pi}{27,33\cdot 24\cdot 3600})^2\\
&=\SI{2,72e-3}{\metre\per\second\squared}=\SI{2,72}{\milli\metre\per\second\squared}
\end{align*}
On peut comparer cette valeur à la valeur exacte donnée à la fin du paragraphe \ref{chutelibredelalune}~:
\[a=\SI{2,7e-3}{\metre\per\second\squared}\]

\section{Force de gravitation}

On peut aller plus loin en déterminant le rapport r de l'accélération de la lune à l'accélération terrestre~:
\[r=\frac{g}{a}=\frac{9,81}{2,72\cdot 10^{-3}}=3607\]
et en remarquant que c'est précisément le rapport des carrés des distances \(d_{Terre-Lune}\) et \(R_{Terre}\)~:
\[r=\frac{d_{Terre-Lune}^2}{R_{Terre}^2}=\frac{(3,844\cdot 10^8)^2}{(6,371\cdot 10^6)^2}=3640\]
La conséquence en est que l'accélération due au poids est inversement proportionnelle au carré de la distance. Comme \(F=m\cdot a\), la force de gravitation est aussi inversement proportionnelle au carré des distances~:
\[F\sim \frac{1}{r^2}\]
On voit apparaître là la loi de la gravitation universelle.