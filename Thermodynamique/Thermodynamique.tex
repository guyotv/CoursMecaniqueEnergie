\myclearpage

\chapter{Thermodynamique\index{energie@énergie}}\label{chapthermo}

\section{Introduction}
\lettrine{L}{e but de ce chapitre} est de parcourir quelques notions et domaines simples de la thermodynamique. Température et chaleur seront abordées avec l'objectif d'étudier la transmission de chaleur à travers divers matériaux de construction pour la maison. On verra aussi le principe de fonctionnement de quelques moteurs thermiques.

\section{Température}
La notion de température\index{temperature@température} n'est pas facile. Même si chacun la conçoit aisément, elle fait référence à ce qui est froid et ce qui est chaud et par là à la notion de chaleur\index{chaleur}. Mais température et chaleur sont deux choses différentes qu'il faut bien distinguer. Nous allons nous intéresser aux deux. Commençons par la température.

\subsection{Celsius}
L'une des manières d'établir une mesure de température consiste à utiliser deux température clairement définies et à diviser l'intervalle qu'elles délimitent. Pour des raisons que nous verrons par la suite, la température de fusion de la glace et celle de vaporisation de l'eau sont relativement faciles à mesurer. Elles ont donc tout naturellement été choisies pour définir le \emph{degré centigrade}\index{Celsius}\index{degre@degré!centigrade} ou \emph{degré Celsius}\index{degre@degré!Celsius}, abrégé par \SI{}{\celsius}. Un intervalle de \SI{100}{\celsius} a été choisi entre ces deux mesures. Ainsi, dans cette unité, la glace fond à \SI{0}{\celsius} et l'eau devient de la vapeur à \SI{100}{\celsius}.
\begin{align*}
\text{Fusion de la glace}\;&\longleftrightarrow\;\SI{0}{\celsius}\\
\text{Vaporisation de l'eau}\;&\longleftrightarrow\;\SI{100}{\celsius}
\end{align*}

\subsection{Fahrenheit}
Une autre échelle, comportant \SI{180}{\Fahrenheit} entre la fusion de la glace et la vaporisation de l'eau, pose une température de \SI{32}{\Fahrenheit} à la fusion. Cette échelle est dite de Fahrenheit\index{Fahrenheit}\index{degre@degré!Fahrenheit}. On a donc~:
\begin{align*}
\text{Fusion de la glace}\;&\longleftrightarrow\;\SI{32}{\Fahrenheit}\\
\text{Vaporisation de l'eau}\;&\longleftrightarrow\;\SI{212}{\Fahrenheit}
\end{align*}
Cela montre que non seulement la définition du zéro de la température mais aussi celle de l'unité de température sont arbitraire.

Il existe bien entendu une correspondance entre les deux échelles qui est donnée par~:
\[T_F=\frac{9}{5}\cdot T_C+32\]
En effet, à la température \(T_C=\SI{0}{\celsius}\) correspond une température \(T_F=\SI{32}{\Fahrenheit}\). De plus comme à un intervalle de température de \SI{100}{\celsius} correspond \SI{180}{\Fahrenheit}, le rapport d'intervalle de température est de \(180/100=9/5\).

Les deux échelles de Celsius et de Fahrenheit reposent donc sur deux états particuliers de la matière. En réalité, il s'agit de transitions d'état. Pour la fusion\index{fusion} de la glace, c'est la transformation de la glace en eau et pour la vaporisation\index{vaporisation}, la transformation de l'eau en vapeur. Nous verrons par la suite que lors de ces changements d'état la température reste particulièrement constante.

\subsection{Kelvin}
L'unité de température du système international\index{systeme@système!international} est le Kelvin\index{Kelvin} dont le symbole est \SI{}{\kelvin}. Il s'agit d'une unité basée sur la notion d'agitation moléculaire. Nous verrons dans le paragraphe suivant que la température est une mesure de l'agitation moléculaire. Celle-ci correspond elle-même à l'énergie cinétique moyenne des molécules. On peut donc imaginer une échelle de température dont le zéro corresponde à une agitation moléculaire nulle (pour autant que cela soit envisageable), c'est-à-dire à une énergie cinétique nulle. Cette échelle est celle du Kelvin. Son zéro, théorique, se situe à \SI{-273,15}{\celsius} et à un intervalle de température de \SI{1}{\kelvin} correspond \SI{1}{\celsius}. Ainsi, \SI{20}{\celsius} correspondent à une température de \SI{293.15}{\kelvin}.

\subsection{Agitation moléculaire\index{agitation moléculaire}}
Une manière étonnante de s'imaginer le monde qui nous entoure est de le considérer sous sa forme moléculaire. De notre point de vue macroscopique, tous les objets qui font partie de notre environnement sont solides et immobiles. Or, la température est intimement liés au mouvement des molécules ou autres atomes. Plus précisément, la température est une mesure de l'énergie cinétique moyenne des molécules ou atomes d'une matière. Pour un gaz comme l'air, par exemple, le lien entre l'énergie cinétique moyenne des molécules \(E_{cin}\) et la température \(T\), exprimée en \si{\kelvin}, est obtenue grâce à la théorie cinétique des gaz parfaits et donnée par l'intermédiaire de la constante de Boltzmann \(k=\SI{1,381e-23}{\joule\per\kelvin}\)~:
\begin{align}
E_{cin}&=\frac{3}{2}\cdot k\cdot T\;\Rightarrow\nonumber\\
\frac{1}{2}\cdot m\cdot v^2&=\frac{3}{2}\cdot k\cdot T\;\Rightarrow\nonumber\\
v&=\sqrt{\frac{3\cdot k\cdot T}{m}}\label{vitair}
\end{align}
Il est donc possible de calculer la vitesse de translation moyenne des molécules d'air en connaissant leur masse. Or, comme l'air est composé essentiellement de molécules d'azote \(N_2\) (à 76\%) et d'oxygène \(O_2\) (à 23\%) dont les masse molaires sont respectivement de \SI{28}{\gram\per\mole} et \SI{32}{\gram\per\mole}, la masse de la molécule d'azote est de \SI{28}{\unitemasseatomique} et celle d'oxygène de \SI{32}{\unitemasseatomique}. L'unité de masse atomique ayant pour valeur~: \SI{1}{\unitemasseatomique}=\SI{1,66e-27}{\kilogram}, la masse d'une molécule d'azote est de \SI{4,648e-26}{\kilogram} et celle de la molécule d'oxygène de \SI{5,312e-26}{\kilogram}. On peut ainsi se faire une idée de la vitesse moyenne des deux types de molécules dans de l'air à \SI{20}{\celsius}, c'est à dire comme nous l'avons vu ci-dessus à \SI{293,15}{\kelvin}, à l'aide de l'équation \ref{vitair}~:
\begin{align*}
v_{N_2}&=\sqrt{\frac{3\cdot k\cdot 293,15}{4,648\cdot 10^{-26}}}=\SI{511}{\metre\per\second}=\SI{1840}{\kilo\metre\per\hour}\\
v_{O_2}&=\sqrt{\frac{3\cdot k\cdot 293,15}{5,312\cdot 10^{-26}}}=\SI{478}{\metre\per\second}=\SI{1721}{\kilo\metre\per\hour}
\end{align*}

\begin{figure}[t]
\centering
\subfigure[Fahrenheit\label{celsiusfahrenheit}]{\includegraphics[width=2cm]{Celsius_Fahrenheit.eps}}\qquad
\subfigure[Kelvin\label{celsiuskelvin}]{\includegraphics[width=3cm]{Celsius_Kelvin.eps}}
\caption[Thermomètres]{Les échelles de température\label{thermometres}\endnote{Voir le site de l'encyclopédie Wikipedia~: \url=http://commons.wikimedia.org/wiki/Image:Fahrenheit_Celsius_scales.jpg= pour l'image de gauche et \url=http://commons.wikimedia.org/wiki/Image:Celsius_kelvin_estandar_1954.png= pour l'image de droite, notamment pour le copyright.}}
\end{figure}

Ces vitesses sont considérables. L'image microscopique de notre environnement est donc plutôt celle d'un ensemble de molécules et d'atomes se déplaçant à grande vitesse sur de courtes distances que celle du monde immobile tel que nous le voyons à échelle macroscopique.

\section{Dilatation}
La mesure de la température est pratiquement liée à celle de la dilatation de liquides comme l'alcool ou le mercure. En effet, c'est par l'intermédiaire de thermomètres utilisant un liquide qui se dilate dans un tube qu'on réalise, encore de nos jours, cette mesure.

Pour une matière solide ou liquide de longueur initiale \(L_0\) soumise à une différence de température \(\Delta\theta\), sa dilatation linéaire \(\Delta L\) obéit à la loi phénoménologique suivante~:
\begin{equation}
\fbox{\(\displaystyle \Delta L=L_0\cdot \alpha\cdot \Delta\theta\)}
\end{equation}
où \(\alpha\) est le c\oe fficient de dilatation linéaire de la matière en question. Ses unités sont~:
\[[\alpha]=\frac{[\Delta L]}{[L_0]\cdot [\Delta\theta]}=\si{\per\celsius}=\si{\per\kelvin}\]
On trouve dans la table \ref{coefdilat} différents c\oe fficient de dilatation linéaires.

\begin{table}
\centering
\begin{tabular}{|l|c|}\hline
Matière & C\oe fficient\\
& \si{\per\kelvin}\\\hline\hline
Aluminium & \num{23,1e-6}\\
Béton & \num{10e-6}\\
Cuivre & \num{16,6e-6}\\
Fer & \num{12e-6}\\
Verre & \num{68e-6}\\
\hline
\end{tabular}
\caption{C\oe fficient de dilatation linéaire\label{coefdilat}}
\end{table}

\smallskip
Un exemple intéressant de dilatation linéaire est donnée par la détermination de l'espace entre chaque rail d'une voie de chemin de fer. La longueur d'un rail étant de \SI{5}{\metre} environ et sa matière du fer, on peut calculer cet espace pour une variation de température de \SI{50}{\celsius} par~:
\[\Delta L=5\cdot 12\cdot 10^{-6}\cdot 50=\SI{3}{\milli\metre}\]
Remarquons que le calcul donne la dilatation totale du rail. Il ne faut pas compter \SI{3}{\milli\metre} de chaque côté du rail. La dilatation est comptée d'un seul côté, l'autre étant considéré comme fixe.

\section{Chaleur}
La chaleur\index{chaleur} est une notion souvent associée à la température\index{temperature@température}. Il faut pourtant clairement les distinguer. Pour le comprendre, étudions les chaleurs spécifique et latente.

\subsection{Chaleur spécifique\index{chaleur!spécifique}}
La chaleur spécifique \(c\) d'une matière est l'énergie \(Q\) qu'il faut fournir à \SI{1}{\kilogram} de celle-ci pour en élever la température de \SI{1}{\celsius}. En d'autres termes, on a~:
\[c=\frac{Q}{m\cdot \Delta\theta}\]
Autrement dit, pour élever la température d'un corps d'une masse \(m\) d'une température \(\Delta\theta\), il faut lui fournir une quantité de chaleur \(Q\) donnée par~:
\begin{equation}\label{thermique}
\fbox{\(\displaystyle Q=m\cdot c\cdot \Delta\theta\)}
\end{equation}

\smallskip
Prenons comme exemple une masse de \SI{2}{\kilogram} de glace dont on veut élever la température de \SI{-10}{\celsius} à \SI{-5}{\celsius}. Quelle est la chaleur nécessaire ?

On trouve la chaleur spécifique de la glace dans la table \ref{tabchalspe}.

\tabchalmass{Chaleur spécifique}{tabchalspe}

Celle-ci nous permet de calculer la chaleur nécessaire par l'équation~:
\[Q=2\cdot 2,06\cdot 10^3\cdot (-5-(-10))=\SI{20600}{\joule}\]
On voit que la chaleur nécessaire est positive. Cela signifie qu'on doit en fournir à la glace.

\medskip
Comme autre exemple, considérons qu'on fournit \SI{10000}{\joule} à \SI{3}{\kilogram} d'eau à une température de \SI{20}{\celsius}. On peut alors calculer la température à laquelle elle parvient après chauffage par~:
\begin{align*}
Q&=m\cdot c_{eau}\cdot (T_f-T_i)\;\Rightarrow\\
T_f&=\frac{Q}{m\cdot c_{eau}}+T_i\\
&=\frac{10'000}{3\cdot 4,18\cdot 10^3}+20=\SI{20,8}{\celsius}
\end{align*}

\subsection{Chaleur latente\index{chaleur!latente}}
Il arrive parfois qu'en fournissant de la chaleur à un corps on n'élève pas sa température. C'est le cas pour la glace qui se trouve à \SI{0}{\celsius}. Alors, ce corps se transforme, il change d'état. Pour la glace, elle fond. L'eau passe de l'état solide à l'état liquide. De manière générale, lors de transition d'état, la température ne varie pas. Par exemple, quand de l'eau passe de l'état liquide à l'état gazeux, une transition d'état qu'on appelle \emph{vaporisation}, sa température ne change pas. Temporellement, quand on chauffe de la glace à une température inférieure à \SI{0}{\celsius}, celle-ci augmente progressivement jusqu'à \SI{0}{\celsius}, puis s'arrête le temps que toute la glace devienne de l'eau, puis la température de l'eau augmente progressivement jusqu'à \SI{100}{\celsius}, s'arrête le temps que toute l'eau devienne de la vapeur dont la température continue alors d'augmenter. L'élévation de température au cours du temps prend donc la forme présentés sur le graphe de la figure \ref{paliers}.

\begin{figure}[h]
\centering
\caption{Changements d'états\label{paliers}}
\psfrag{T}{T/\SI{}{\celsius}}
\psfrag{t}{t}
\psfrag{0}{0}
\psfrag{100}{100}
\psfrag{glace}{glace}
\psfrag{eau}{eau}
\psfrag{vapeur}{vapeur}
\psfrag{Palier}{Palier}
\psfrag{de fusion}{de fusion}
\psfrag{de vaporisation}{de vaporisation}
\psfrag{de liquéfaction}{de liquéfaction}
\psfrag{de solidification}{de solidification}
\includegraphics{Paliers.eps}
\end{figure}

On y distingue des paliers pendant lesquels la température reste constante. Il s'agit du \emph{palier de fusion}\index{palier!de fusion} de la glace ou du \emph{palier de solidification}\index{palier!de solidification} de l'eau et du \emph{palier de vaporisation}\index{palier!de vaporisation} de l'eau ou du \emph{palier de liquéfaction}\index{palier!de liquéfaction} de la vapeur.

\smallskip
Notons que ce sont ces paliers qui ont servi à définir l'échelle de température de Celsius.

\smallskip
La chaleur latente \(L\) nécessaire à faire passer un kilogramme d'une matière donnée d'un état à l'autre est donnée par la \emph{chaleur latente}\index{chaleur!latente}. Elle s'exprime par~:
\[L=\frac{Q}{m}\]
Autrement dit, pour changer l'état d'un corps d'une masse \(m\), il faut lui fournir une quantité de chaleur \(Q\) donnée par~:
\begin{equation}
\fbox{\(\displaystyle Q=m\cdot L\)}
\end{equation}
Évidemment, la chaleur latente est différente pour chaque transformation d'état. On trouve dans le tableau \ref{tabchaleurlat} les chaleur latentes de quelques matières pour chaque changement d'état. Les chaleurs latentes sont données positivement. Elles correspondent donc à un changement d'état qui nécessite un apport de chaleur au système pour se réaliser. Pour le changement d'état inverse, il suffit d'affecter à la chaleur latente un signe négatif. Pour l'eau, par exemple, la chaleur latente de fusion est de \SI{3,3e5}{\joule\per\kilogram}. La chaleur latente de solidification est donc de \SI{-3,3e5}{\joule\per\kilogram}.

\tabchallat{tabchaleurlat}

Par exemple, on peut calculer la chaleur nécessaire à transformer \SI{3}{\kilogram} de glace en eau ainsi~:
\[Q=m\cdot L_f=3\cdot 3,3\cdot 10^5=\SI{990000}{\joule}\]

\smallskip
De la même manière, en fournissant une chaleur de \SI{100000}{\joule} à de l'eau à \SI{100}{\celsius}, la quantité d'eau qui va passer de l'état liquide à l'état gazeux est donnée par~:
\begin{align*}
Q&=m\cdot L_v\;\Rightarrow\;m=\frac{Q}{L_v}\\
m&=\frac{100'000}{23\cdot 10^5}=\SI{0,0435}{\kilogram}=\SI{43,5}{\gram}
\end{align*}

\begin{figure*}[t]
\fbox{
\begin{minipage}{16cm}
\begin{minipage}[b]{9cm}
\textbf{Nicolas Léonard Sadi Carnot (1796-1832)\index{Carnot Nicolas Leonard@Carnot Nicolas Léonard}}

\medskip
Nicolas Léonard Sadi Carnot\index{Carnot Nicolas Leonard@Carnot Nicolas Léonard} ne publia qu'un seul livre, \oe uvre de sa vie~: \textit{``Réflexions sur la puissance motrice du feu et sur les machines propres à développer cette puissance''} (Paris, 1824).

Il est l'un des fondateurs de la thermodynamique\index{thermodynamique}, science des phénomènes thermiques. Il développa l'idée d'un moteur idéal\index{moteur ideal@moteur idéal} d'un rendement maximum\index{rendement!maximum} indépassable. Il est en effet impossible de transformer intégralement la chaleur provenant d'une source chaude fournie à un moteur thermique en travail mécanique. Nécessairement, un partie de cette chaleur est rejetée vers une source froide.

Il ébaucha la première loi de la thermodynamique\index{première loi!de la thermodynamique} en tentant de trouver un lien entre travail et chaleur.

Évidemment, une révolution motrice fut engagée avec Carnot et les machines à vapeur, révolution qui fut à l'origine de la société thermo-industrielle.

\bigskip 

\raggedleft{\footnotesize{Nicolas Léonard Sadi Carnot en uniforme de polytechnicien peint par Louis Léopold Boilly, tiré de Wikipedia\endnote{Voir \url=http://fr.wikipedia.org/wiki/Image:Sadi_Carnot.jpeg=}}}
\end{minipage}
\hfill
\parbox[b]{6cm}{\includegraphics[width=6cm]{Sadi_Carnot.eps}}
\end{minipage}
}
\end{figure*}

\section{Énergie thermique}
Il s'agit ici de présenter ici le plus simplement possible la relation qui existe entre les concepts de travail\index{travail}, d'énergie interne\index{energie@énergie!interne} et de chaleur\index{chaleur}. Nous avons déjà vu ce qu'est le travail. L'énergie interne est constituée, quant à elle, de la somme des énergie potentielles\index{energie@énergie!potentielle} et cinétiques\index{energie@énergie!cinétique} d'un corps constitué de plusieurs éléments. L'énergie potentielle représente ici l'énergie liée aux forces qui s'exercent entre les éléments du corps et l'énergie cinétique est celle qui est liée à l'agitation de ces éléments dont une bonne mesure est donnée par la température\index{temperature@température}. La chaleur quant à elle s'entend comme de l'énergie qui transite d'un corps à l'autre.

\medskip
Les gaz ont des propriétés étonnantes qui en permettent une utilisation particulièrement adaptée au moteurs thermiques. Sans donner ici la loi des gaz parfaits qui traduit ces propriétés, on peut présenter rapidement le \emph{premier principe}\index{premier principe} de la thermodynamique qui règle le fonctionnement de ces moteurs.

\subsection{Premier principe}
Lorsqu'on chauffe un gaz contenu dans un récipient muni d'un piston, la chaleur Q transférée à ce gaz peut produire deux effets~: une augmentation de son énergie interne \(\Delta U\), c'est-à-dire de sa température et/ou une variation de son volume impliquant un travail A du piston.

Le premier principe\index{premier principe} de la thermodynamique dit en effet que~:
\begin{equation}\label{premierprincipe}
\fbox{\(\displaystyle Q=\Delta U+A\)}
\end{equation}
Ce principe est à la base de la construction de moteurs thermiques auxquels on fournit de la chaleur \(Q\) pour la transformer en partie en travail mécanique \(A\). Mais cela dépasse le cadre de cette partie d'introduction à la thermodynamique.

%\section{Transfert de chaleur}
%Un phénomène intéressant concernant la chaleur concerne les modalités de son transfert à travers les matériaux. Il est important notamment pour l'isolation des batiments.

\begin{sidewaysfigure*}
%\begin{figure*}[!b]
\begin{shaded}
\begin{center}
\begin{minipage}{16cm}
	\begin{minipage}[b]{16cm}
		\begin{center}
		\textsc{Résumé des grandeurs et unités}
		\end{center}
		\smallskip
	\end{minipage}\\
	\begin{minipage}[t]{16cm}
		\begin{center}
		\begin{tabular}{lll}
		\textbf{Grandeur} & \textbf{Définition} & \textbf{Unité} \\
		Température & \(T\) & \(\SI{}{\celsius},\SI{}{\kelvin},\SI{}{\Fahrenheit}\) \\
		C\oe f. dilatation & \(\alpha\) & \(\si{\per\celsius}\) \\
		Chaleur & \(Q\) & \(\si{\joule}\) \\
		Chaleur spécifique & \(c\) & \(\si{\joule\per\kilogram\kelvin}\) \\
		Chaleur latente & \(L_o\) & \(\si{\joule\per\kilogram}\) \\
		Énergie interne & \(U\) & \(\si{\joule}\) \\
		Travail & \(A\) & \(\si{\joule}\)
		\end{tabular} 
		\end{center}
	\end{minipage}\\
	
	\bigskip
	
	\begin{minipage}[b]{16cm}
		\begin{center}
		\textsc{Résumé des relations concernant la thermodynamique}
		\end{center}
		\smallskip
	\end{minipage}\\
	\begin{minipage}[t]{8cm}
		\textbf{Définition des grandeurs}
		\begin{align}
		\alpha&=\frac{\Delta L}{L_o\cdot \Delta\theta}\\
		c&=\frac{Q}{m\cdot \Delta\theta}
		\end{align}
	\end{minipage}
	\begin{minipage}[c]{2.3cm}
		et
		% \[\stackrel{\displaystyle a_{o}=0}{\Longleftrightarrow}\]
	\end{minipage}
	\begin{minipage}[t]{6cm}
		\textbf{Relations particulières}
		
		\begin{equation}
		\frac{1}{2}\cdot m\cdot v^2=\frac{3}{2}\cdot k\cdot T
		\end{equation}
	\end{minipage}\\

	\bigskip

	\begin{minipage}[b]{16cm}
	\textbf{Lois}
		\begin{align}
		\Delta L&=L_0\cdot \alpha\cdot \Delta\theta\\
		Q&=m\cdot c\cdot \Delta\theta\\
		Q&=\Delta U+A
		\end{align}
	\end{minipage}
\end{minipage}
\end{center}
\end{shaded}
%\end{figure*}
\caption{Résumé de thermodynamique}
\end{sidewaysfigure*}