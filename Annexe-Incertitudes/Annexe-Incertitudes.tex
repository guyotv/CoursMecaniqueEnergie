\myclearpage

\chapter{Ordre de grandeur, erreur et incertitudes}

\section{Ordre de grandeur}
En physique la représentation des nombres est une chose complexe. Imaginez une mesure de la largeur d'une feuille A4 donnant le résultat de \SI{21,03544329}{\centi\metre}. Quelle en est sa précision ? On peut pour l'évaluer la représenter en millimètre, soit \SI{210,3544329}{\milli\metre}, en micromètre, soit \SI{210354,4329}{\micro\metre}, en nanomètre, soit \SI{210354432,9}{\nano\metre} ou même en Angstr\oe m, soit \SI{2103544329}{\angstrom}.

Comme elle semble être précise à l'ordre de grandeur de l'Angstr\oe m (\SI{1e-10}{\metre}) et qu'on est alors à l'échelle de l'atome, on peut se demander comment une telle mesure peut avoir été réalisée.

Par ailleurs, faut-il écrire \SI{21,0}{\centi\metre}, \SI{21}{\centi\metre}, voir \SI{2,1}{\deci\metre}, pour autant qu'il existe des avantages à utiliser l'une ou l'autre de ces notations ?

\medskip
Ce qu'il faut garder à l'esprit, c'est que la représentation choisie peut être porteuse d'une information intéressante quant à la précision de la mesure qui est à son origine. Par exemple, entre \SI{21,0}{\centi\metre} et \SI{21}{\centi\metre}, si il n'y a aucune différence numérique, on peut considérer que la présence du zéro de la première expression signifie que sa précision est de l'ordre du millimètre, alors qu'elle est de l'ordre du centimètre dans la seconde.

\subsection{Chiffres significatifs}
Si on réalise une mesure au centimètre près et qu'on l'écrit comme \SI{21,0}{\centi\metre}, on ne peut dire que le chiffre zéro signifie quelque chose. On dira donc que la mesure a été faite avec deux chiffres significatifs et on s'abstiendra d'écrire le zéro. Le nombre de chiffres significatifs dépend donc de l'estimation de la qualité de la mesure, de ce qu'on nomme incertitude et qu'on présentera plus loin. Ainsi, quand cette estimation s'exprime correctement dans l'expression d'un nombre, le nombre de chiffres significatifs \index{chiffres@chiffres!significatifs} est simplement le nombre de chiffres présents, à l'exception des zéros qui pourraient se trouver à gauche du nombre.

\smallskip
Ainsi, écrire \SI{21}{\centi\metre} ou \SI{2,1}{\deci\metre} ne change pas le nombre de chiffres significatifs qui est ici de deux.

\subsection{Ordre de grandeur}
Qu'en est-il exactement de la notion d'ordre de grandeur ?

\smallskip
L'ordre de grandeur du chiffre \SI{2045000}{\metre} est le million de mètres, qu'on le représente comme précédemment ou par \SI{2045e3}{\metre} ou encore par \SI{2,045e6}{\metre}. Il s'agit toujours du million de mètres ou du millier de kilomètres. On voit que l'expression de l'ordre de grandeur dépend de l'unité choisie (million de mètres ou millier de kilomètres) et se situe dans le premier chiffre du nombre. C'est pourquoi son expression sous une forme particulière de notation scientifique est intéressante pour déterminer l'ordre de grandeur d'un nombre.

\medskip
Généralement, la notation scientifique se définit par la présence d'une mantisse, d'une base et d'un exposant. Ainsi, \SI{2045e3}{\metre} ou encore \SI{2,045e6}{\metre} sont des nombre exprimés en notation scientifique. Mais la seconde expression est plus intéressante du point de vue de l'ordre de grandeur, car on y lit celui-ci directement dans l'exposant. Cette notation scientifique \index{notation@notation!scientifique} particulière ne comporte qu'un seul et unique chiffre avant la virgule et celui-ci est différent de zéro.

\medskip
Si on utilise la notation scientifique particulière présentée ci-dessus, l'ordre de grandeur \index{ordre@ordre!de grandeur} d'un chiffre exprimé dans une unité donnée est son exposant.

\medskip
Relevez enfin qu'il ne faut pas confondre notation scientifique et notation d'ingénieur \index{notation@notation!d'ingénieur}. Cette dernière est une notation scientifique dont les exposants sont des multiples de trois. La raison en est que les préfixes des unités changent généralement leurs ordres de grandeur par trois~: \SI{1e-3}{\milli\metre}, \SI{1e-6}{\micro\metre} ou \SI{1e-9}{\nano\metre}, etc.

\section{Écart et erreur}
On peut facilement déterminer l'écart\index{ecart@écart} entre deux valeurs a et b par leur différence a-b. On peut, par exemple, mesurer la longueur L des baguettes de pain vendues par un boulanger et déterminer les différents écarts entre elles. Par exemple, on pourrait avoir une série de mesures telles que celle données dans le tableau \ref{baguettepain}.

\begin{table}[ht]
\centering
\caption{La longueur des baguettes de pain}\label{baguettepain}
\begin{tabular}{|c|c|c|c|c|}
\hline 
\textbf{L} & \textbf{Écart} & \textbf{Écart} & \textbf{Erreur} & \textbf{Erreur}\\
\si{\centi\metre}	&\si{\centi\metre}	&\%	&\si{\centi\metre}	&\%\\
\hline
60	&-2	&-3.2	&0	&0.0\\
65	&3	&4.8	&5	&8.3\\
64	&2	&3.2	&4	&6.7\\
58	&-4	&-6.5	&-2	&-3.3\\
61	&-1	&-1.6	&1	&1.7\\
57	&-5	&-8.1	&-3	&-5.0\\
60	&-2	&-3.2	&0	&0.0\\
64	&2	&3.2	&4	&6.7\\
62	&0	&0.0	&2	&3.3\\
65	&3	&4.8	&5	&8.3\\
63	&1	&1.6	&3	&5.0\\
60	&-2	&-3.2	&0	&0.0\\
64	&2	&3.2	&4	&6.7\\
65	&3	&4.8	&5	&8.3\\
\hline
\multicolumn{5}{|c|}{Moyennes}\\
\hline
62	&0.0	&0.0	&2.0	&3.3\\
\hline 
\end{tabular}
\end{table}

On voit immédiatement que le calcul des écarts pose un problème~: il faut déterminer les écarts entre chaque baguettes deux par deux. On peut le faire. Mais quel sens cela a-t-il ?

Par contre, déterminer quel est l'écart à la moyenne des baguettes est plus instructif. La moyenne vaut \SI{62}{\centi\metre} et la seconde colonne du tableau \ref{baguettepain} présente les écarts. On voit alors facilement que l'écart ne dépasse pas \SI{5}{\centi\metre}. Ce qui peut avoir de l'importance si on a faim.

Par ailleurs, si on sait que le boulanger avait décidé de faire des baguettes de \SI{60}{\centi\metre}, on peut se poser une autre question~: quel est l'écart à cette valeur ? La quatrième colonne y apporte une réponse. Comme on utilise ce qu'on peut appeler une référence, on parlera d'\emph{erreur}\index{erreur@erreur}, plutôt que d'écart. Ce qui est alors intéressant, c'est qu'on voit que le boulanger à tendance à faire des baguettes trop grandes. Cela peut avoir une importance pour lui s'il a prévu un budget précis de matières premières pour des baguettes de \SI{60}{\centi\metre}. Cela permet aussi de s'interroger sur la règle utilisée par le boulanger pour estimer la longueur des baguettes. Comme ses baguettes sont trop longues, on peut penser que sa règle est aussi trop longue, ce qui peut avoir pour conséquence une mauvaise estimation de la longueur de la baguette par le boulanger. On parlera alors d'une \emph{erreur systématique}\index{erreur@erreur!systématique} induite par un matériel mal calibré. Ce type d'erreur se détecte par la présence d'une importante quantité de signes\index{signe@signe!des écarts} systématiquement positifs ou systématiquement négatifs dans les écarts. En effet, si la règle avait une longueur correcte, l'erreur faite par le boulanger devrait être aléatoirement répartie autour de la valeur de \SI{60}{\centi\metre} et les signes positifs et négatifs des écarts devraient être en nombres à peu près identiques.

Évidemment, la moyenne des écarts à la moyenne est nulle, cela par définition, alors que la moyenne des erreurs ne l'est pas en présence d'une erreur systématique\index{erreur@erreur!systématique}.

Figurent aussi dans le tableau \ref{baguettepain} les écarts et erreurs relatifs\index{ecart@écart!relatif}\index{erreur@erreur!relative} en pourcents. Il s'agit du rapport entre l'écart et la valeur de référence~: la moyenne pour l'écart et \SI{60}{\centi\metre} pour l'erreur. Autant pour l'écart que pour l'erreur, on a donc~:
\begin{equation}
e=\frac{val-val_{ref}}{val_{ref}}\cdot 100
\end{equation}

\begin{table}[ht]
\centering
\caption{La longueur d'autres baguettes de pain}\label{baguettepain2}
\begin{tabular}{|c|c|c|c|c|}
\hline 
\textbf{L} & \textbf{Écart} & \textbf{Écart} & \textbf{Erreur} & \textbf{Erreur}\\
\si{\centi\metre}	&\si{\centi\metre}	&\%	&\si{\centi\metre}	&\%\\
\hline
40	&-2	&-4.8	&0	&0.0\\
44	&2	&4.8	&4	&10.0\\
41	&-1	&-2.4	&1	&2.5\\
44	&2	&4.8	&4	&10.0\\
43	&1	&2.4	&3	&7.5\\
42	&0	&0.0	&2	&5.0\\
44	&2	&4.8	&4	&10.0\\
42	&0	&0.0	&2	&5.0\\
42	&0	&0.0	&2	&5.0\\
43	&1	&2.4	&3	&7.5\\
43	&1	&2.4	&3	&7.5\\
41	&-1	&-2.4	&1	&2.5\\
40	&-2	&-4.8	&0	&0.0\\
39	&-3	&-7.1	&-1	&-2.5\\
\hline
\multicolumn{5}{|c|}{Moyennes}\\
\hline
42	&0.0	&0.0	&2.0	&5.0\\
\hline 
\end{tabular}
\end{table}

Ces indications sont importantes si on désire comparer la production de deux boulangers dont la longueur de la baguette de référence n'est pas la même. Considérons le tableau \ref{baguettepain2} qui décrit la production d'un boulanger dont la baguette de référence est de \SI{40}{\centi\metre}. On voit que la moyenne des écarts est nulle comme pour le boulanger précédent. Ce qui est normal en raison du choix de la valeur moyenne comme référence. On voit aussi que la moyenne des erreurs est la même et qu'il y a une grande systématique dans celle-ci, puisqu'elles sont pratiquement toutes positives. Cela signifie probablement, comme précédemment, que l'appareil de  mesure à un biais, que la règle utilisée est trop longue. Par contre, on voit grâce à la dernière colonne donnant l'erreur relative que celle-ci est plus importante pour le second boulanger. Cela s'explique facilement. En effet, l'erreur moyenne est la même, mais la baguette de référence du second boulanger est plus courte (\SI{40}{\centi\metre}). Ainsi, malgré la différence de longueur de la baguette de référence, l'erreur relative permet de comparer les erreurs des deux boulangers.

\bigskip
Imaginons maintenant qu'on s'intéresse à la production annuelle de baguettes d'un boulanger, soit des centaines de baguettes. Il devient difficile de les représenter dans un tableau, surtout si on fait des mesures d'une précision supérieures au centimètre. On peut alors réaliser des classes de mesures (on parle d’enclassement\index{enclassement@enclassement}) en mettant par exemple, toutes les baguettes entre \SI{42,5}{\centi\metre} et \SI{43,4}{\centi\metre} dans la classe des baguettes de \SI{43}{\centi\metre}. En procédant de la même manière pour les autres valeurs, on peut alors obtenir des mesures comme celles présentées dans le tableau \ref{enclassementbaguettes} où L est la longueur des baguettes et n le nombre de baguettes dans la classe associée à cette longueur, soit la fréquence d'apparition de la longueur.

\begin{table}[ht]
\centering
\caption{Des centaines de baguettes de pain}\label{enclassementbaguettes}
\begin{tabular}{|c|c|}
\hline 
\textbf{L} & n\\
\si{\centi\metre}	& -\\
\hline
38	& 53\\
39	& 72\\
40	& 95\\
41	& 121\\
42	& 130\\
43	& 118\\
44	& 90\\
45	& 67\\
46	& 44\\
\hline
\multicolumn{2}{|c|}{Moyennes}\\
\hline
41,9	& -\\
\hline 
\end{tabular}
\end{table}
La moyenne \(\mu\) de la longueur des baguettes est alors calculée ainsi~:
\[\mu=\frac{\sum\limits_i L_i\cdot n_i}{\sum\limits_i N_i}=\frac{33'116}{790}=\SI{41,9}{\centi\metre}\]
La moyenne des fréquences n n'a que peu de sens.

\smallskip
On peut aussi représenter ces mesures graphiquement, comme le montre la figure \ref{baguettesgauss}.

\begin{figure}
\centering
\caption{Baguettes d'une année}\label{baguettesgauss}
\begin{gnuplot}[terminal=latex,terminaloptions=rotate,scale=0.6]
		# domaine de définition
        set xrange [36:48]
        set yrange [0:140]
        # flèches
        set arrow 1 from 39.7,120 to 39.7,0 head filled
        #size screen 0.5,30
        set label "Écart type" at 39.4,100 rotate by 90
        set arrow 2 from 44.1,120 to 44.1,0 head filled
        set label "Écart type" at 44.4,138 rotate by -90
        set arrow 3 from 41.9,120 to 41.9,0 head filled
        set label "Moyenne" at 42.2,80 rotate by -90
        #set grid
        #set title "Baguettes d'une année"
        # suppression de la légende
        set key off
        # légendes des axes
        set xlabel "Longueur des baguettes (en cm)"
        set ylabel "Nombre de baguettes" rotate by 90
        # largeur des colonnes (boxes)
        #set boxwidth 0.1
       
        # tracé du graphe
        plot "Annexe-Incertitudes/Images/baguettesgauss.dat" with boxes
\end{gnuplot}
\end{figure}
Sur un grand nombre de mesures, la notion d'écart devient difficile à gérer. On utilise alors celle d'écart type\footnote{Le -1 apparaissant au dénominateur de la fraction de l'écart type vient d'une estimation de l'écart type (ou variance) de la population des mesures basée sur un échantillon de celle-ci. Pour bien le comprendre, il faut évoquer le calcul des probabilités et la notion de variable aléatoire, ce qui dépasse le cadre de ce document. Vous trouverez des informations sur internet.}\index{ecart@écart!type} définie par~:
\[\sigma=\sqrt{\frac{\sum\limits_i n_i\cdot (L_i-\mu)^2}{\sum\limits_i n_i-1}}=\sqrt{\frac{3'777.1}{789}}=\SI{2,2}{\centi\metre}\]
Il s'agit de la moyenne des écarts quadratiques, soit de la racine de la moyenne des écarts au carrés. L'élévation au carré permet de ne pas tenir compte du signe des écarts.

On peut alors affirmer qu'une forte proportion de la longueur L des baguettes se trouve entre \(\mu-\sigma\) et \(\mu+\sigma\), soit entre \SI{39,7}{} et \SI{44,1}{\centi\metre}.

\bigskip
Pour aller plus loin, il faut voir l'ensemble complet des mesures qui pourraient être faites dans le cadre d'une loi physique donnée comme une population\index{population@population} qu'on ne pourrait étudier qu'à partir d'échantillons\index{echantillon@échantillon} partiels extraits de celle-ci. Imaginez la population de tous les éléphants existants sur la Terre. Pour l'étudier, il faut en prélever un ou plusieurs échantillons dont les propriétés peuvent être différentes de celles de la population. Par exemple, la masse moyenne de la population des éléphants peut être différente de celle d'un échantillon d'éléphants pris par hasard obèses. La science des probabilités et statistiques dispose d'outils pour caractériser les échantillons, mais aussi pour en déduire les caractéristiques des populations. Mais cette science, qui repose sur les probabilités, est malheureusement hors du cadre de cet ouvrage.

\section{Incertitude\index{incertitude@incertitude}}
Avec les écarts, on étudie la répartition de mesures réalisées. Du point de vue des probabilités et statistiques, on parlera des propriété d'un échantillon de mesures relatives à une loi donnée.

\medskip
Avec les incertitudes, on va s'intéresser à l'évaluation des capacités des instruments de mesures. Ces deux domaines n'ont rien à voir l'un avec l'autre, si ce n'est qu'ils sont les deux nécessaires à la réalisation des expériences de physique. Le problème est que l'évaluation de la qualité de mesure des instruments nécessite des méthodes de probabilités et statistiques. Cette évaluation peut donc devenir complexe si on entre réellement dans les détails.

Nous en resterons ici à un niveau aussi simple que possible en admettant qu'il soit possible d'évaluer la précision d'un instrument de mesure sans avoir recours aux statistique pour le calibrer ou que cette calibration ait été réalisée par ailleurs et soit disponible.

\medskip
Imaginons donc la mesure de la longueur d'une feuille A4 à l'aide d'une règle. La présentation de cette mesure est la suivante~:
\[L=L_m\pm\, I(L_m)\]
où, \(L\) est la grandeur, \(L_m\) sa valeur et \(I(L_m)\) son incertitude absolue. Par exemple, une mesure pourrait donner~:
\[L=\SI{29,0}\pm\, \SI{0,2}{\centi\metre}\]
L'origine de l'incertitude absolue\index{incertitude@incertitude!absolue} peut être de diverses nature~:
\begin{enumerate}
\item une estimation de la précision suite à la lecture visuelle sur la règle selon sa graduation, la distance à laquelle ou l'angle sous lequel on la regarde, \dots,
\item une information du fabriquant qui a réalisé des tests approfondis,
\item une étude statistique à travers un ou plusieurs échantillons sur une mesure identique, etc.
\end{enumerate}
Conformément à ce que nous avons dit plus haut, on ne considérera pas le dernier point.

\medskip
Admettons maintenant qu'on réalise aussi une mesure de la largeur de la feuille, mais avec une règle différente, et obtienne~:
\[l=\SI{21,1}\pm\, \SI{0,3}{\centi\metre}\]

\subsection{Addition/soustraction}
Que pouvons-nous dire alors du périmètre de la feuille ?

\smallskip
Évidemment, il est facile à calculer~:
\[P=2\cdot L_m+2\cdot l_m=2\cdot (29,0+21,1)=\SI{100,2}{\centi\metre}\]

\medskip
Mais qu'en est-il de son incertitude ? Pour la connaître, on va calculer l'incertitude absolue de la somme de deux grandeurs en déterminant les maximum et minimum de celle-ci. Formellement, si~:
\begin{equation*}
s=l_1+l_2\;;\;l_1=l_{1m}\pm\, I(l_{1m})\;;\;l_2=l_{2m}\pm\, I(l_{2m})
\end{equation*}
alors, on peut calculer les extremums~:
\begin{align*}
s_{max}&=l_{1\,max}+l_{2\,max}\\
&=l_{1m}+I(l_{1m})+l_{2m}+I(l_{2m})\\
&=l_{1m}+l_{2m}+I(l_{1m})+I(l_{2m})\\
&=s_m+I(s_m)\\
\text{et}\\
s_{min}&=l_{1\,min}+l_{2\,min}\\
&=l_{1m}-I(l_{1m})+l_{2m}-I(l_{2m})\\
&=l_{1m}+l_{2m}-I(l_{1m})-I(l_{2m})\\
&=s_m-I(s_m)
\end{align*}
et en déduire que l'incertitude d'une somme ou d'une soustraction\index{incertitude@incertitude!somme}\index{incertitude@incertitude!soustraction}~:

\begin{empheq}[box=\fbox]{equation*}
\text{Si }s=l_1\pm\, l_2\;\Rightarrow\;I(s_m)=I(l_{1m})+I(l_{2m})
\end{empheq}

On peut alors calculer l'incertitude sur le périmètre~:
\begin{align*}
I(P_m)&=2\cdot I(L_m)+2\cdot I(l_m)\\
&=2\cdot 0,2+2\cdot 0,3=\SI{1,0}{\centi\metre}
\end{align*}
et donner le résultat final sous la forme~:
\[C=\SI{100,2}\pm\, \SI{1,0}{\centi\metre}\]

\subsection{Multiplication par un entier\index{incertitude@incertitude!multiplication par un entier}}
De l'équation précédente, on tire facilement que~:
\[I(2\cdot b_m)=I(b_m)+I(b_m)=2\cdot I(b_m)\]
et en généralisant que~:

\begin{empheq}[box=\fbox]{equation*}
\text{Si }c=n\cdot b\;\Rightarrow\;I(c_m)=n\cdot I(b_m)
\end{empheq}

\subsection{Multiplication/division}
Que pouvons-nous dire de la surface de la feuille ?

\smallskip
Évidemment, elle est facile à calculer~:
\[S=l_1\cdot l_2=29,0\cdot 21,1=\SI{611,9}{\centi\metre\squared}\]

\medskip
Mais qu'en est-il de son incertitude ? Pour la connaître, on va calculer l'incertitude absolue de la multiplication\index{incertitude@incertitude!multiplication} de deux grandeurs en déterminant les maximum et minimum de celle-ci. Formellement, si~:
\begin{equation*}
S=l_1\cdot l_2\;;\;l_1=l_{1m}\pm\, I(l_{1m})\;;\;l_2=l_{2m}\pm\, I(l_{2m})
\end{equation*}
alors, on peut calculer le maximum~:
\begin{align*}
S_{max}&=l_{1\,max}\cdot l_{2\,max}\\
&=(l_{1m}+I(l_{1m}))\cdot (l_{2m}+I(l_{2m}))\\
&=l_{1m}\cdot l_{2m}+l_{1m}\cdot I(l_{2m})\\
&\;+l_{2m}\cdot I(l_{1m})+I(l_{1m})\cdot I(l_{2m})\\
&=S_m+I(S_m)
\end{align*}
L'expression de l'incertitude s'avère alors pour le moins complexe~:
\[I(S_m)=l_{1m}\cdot I(l_{2m})+l_{2m}\cdot I(l_{1m})+I(l_{1m})\cdot I(l_{2m})\]
Mais, comme \(S=l_1\cdot l_2\), on peut diviser cette équation par \(S\)~:
\begin{align*}
\frac{I(S_m)}{S_m}&=\frac{l_{1m}\cdot I(l_{2m})}{l_{1m}\cdot l_{2m}}+\frac{l_{2m}\cdot I(l_{1m})}{l_{1m}\cdot l_{2m}}+\frac{I(l_{1m})\cdot I(l_{2m})}{l_{1m}\cdot l_{2m}}\\
&=\frac{I(l_{1m})}{l_{1m}}+\frac{I(l_{2m})}{l_{2m}}+\frac{I(l_{1m})\cdot I(l_{2m})}{l_{1m}\cdot l_{2m}}
\end{align*}
Pour autant que l'incertitude soit nettement plus petite que la grandeur qui lui correspond, le terme \(I(l_{1m})\cdot I(l_{2m})\) est très petit devant \(l_{1m}\cdot l_{2m}\) et donc il est possible de négliger le dernier terme. Dans ces conditions~:
\[\frac{I(l_{1m})\cdot I(l_{2m})}{l_{1m}\cdot l_{2m}}<<1\]
et on peut écrire~:
\begin{align*}
\frac{I(S_m)}{S_m}&\cong\frac{I(l_{1m})}{l_{1m}}+\frac{I(l_{2m})}{l_{2m}}\;\Rightarrow\\
i(S_m)&=i(l_{1m})+i(l_{2m})
\end{align*}
en définissant l'incertitude relative\index{incertitude@incertitude!relative} par~:

\begin{empheq}[box=\fbox]{equation*}
i(G_m)=\frac{I(G_m)}{G_m}
\end{empheq}

\medskip
L'incertitude relative, comme expression de l'incertitude absolue rapportée à la valeur de sa grandeur, représente une incertitude sans unités qu'on peut exprimer en pourcents. En utilisant l'incertitude relative, l'expression de l'incertitude d'une multiplication de deux grandeurs devient alors bien plus simple.

Par ailleurs, on peut que le résultat est le même pour le minimum et pour la division\index{incertitude@incertitude!multiplication}\index{incertitude@incertitude!division} de deux grandeurs. Ainsi, finalement, on peut écrire~:

\begin{empheq}[box=\fbox]{equation*}
\text{Si }S=l_1\cdot\div l_2\;\Rightarrow\;i(S_m)=i(l_{1m})+i(l_{2m})
\end{empheq}

Pour revenir à l'exemple de la surface de la feuille A4, pour en calculer l'incertitude absolue, on écrit~:
\begin{align*}
I(S_m)&=S_m\cdot i(S_m)=L_m\cdot l_m\cdot i(L_m\cdot l_m)\\
&=L_m\cdot l_m\cdot (i(L_m)+i(l_m))\\
&=L_m\cdot l_m\cdot (\frac{I(L_m)}{L_m}+\frac{I(l_m)}{l_m})\\
&=29,0\cdot 21,1\cdot (\frac{0,2}{29,0}+\frac{0,3}{21,1})\\
&=611,9\cdot (0,007+0,014)\\
&=611,9\cdot (0,7\%+1,4\%)\\
&=611,9\cdot 2,1\%=\SI{12,8}{\centi\metre\squared}
\end{align*}

Ainsi, le résultat final est~:
\[S=\SI{611,9}\pm\, \SI{12,8}{\centi\metre\squared}\]

\smallskip
L'expression du calcul en pourcent permet de se rendre compte que c'est la mesure de la largeur de la feuille qui est la plus imprécise (1,4\% contre 0,7\% pour la longueur) et que l'incertitude relative est relativement faible (2,1\%).

Par ailleurs, un problème se pose quant à l'expression du résultat dont l'incertitude absolue est arrondie~: jusqu'où devons nous aller dans l'arrondi ? Il n'existe pas de réponse logique à ce problème. Mais avec l'incertitude absolue obtenue, on se rend compte que le plus grand chiffre affecté par celle-ci est celui des dizaines de centimètres. Comme l'incertitude absolue est supérieure à \SI{10}{\centi\metre\squared}, il est courant dans ce cas, d'utiliser le chiffre des dizaines arrondis à sa limite supérieure, soit \SI{20}{\centi\metre\squared}, pour écrire le résultat sous la forme~:
\[S=\SI{611,9}\pm\, \SI{20}{\centi\metre\squared}\]
ou, par changement du préfixe de l'unité~:
\[S=\SI{6,1}\pm\, \SI{0,2}{\centi\metre\squared}\]

\subsection{Puissance\index{incertitude@incertitude!puissance}}
Si on veut calculer l'incertitude sur une grandeur élevée au carré, il suffit d'écrire~:
\[i(a_m^2)=i(a_m\cdot a_m)=i(a_m)+i(a_m)=2\cdot i(a_m)\]
On peut généraliser cette expression à celle d'une puissance quelconque~:

\begin{empheq}[box=\fbox]{equation*}
\text{Si }c=a^n\;\Rightarrow\;i(c_m)=n\cdot i(a_m)
\end{empheq}

En particulier, pour des valeurs de n comme 1/2 ou 1/3, voire 3/5, cette équation s'applique.

\subsection{Résumé}
L'incertitude relative\index{incertitude@incertitude!relative} est le rapport entre l'incertitude absolue\index{incertitude@incertitude!absolue} et la valeur de la grandeur correspondante.

Pour l'addition\index{incertitude@incertitude!addition} et la soustraction\index{incertitude@incertitude!soustraction}, on somme donc les incertitudes absolues et pour la multiplication\index{incertitude@incertitude!multiplication} et la division\index{incertitude@incertitude!division}, on somme les incertitudes relatives.

Pour la multiplication par un entier\index{incertitude@incertitude!multiplication par un entier}, on multiplie l'incertitude absolue par celui-ci et pour l'élévation à une puissance\index{incertitude@incertitude!puissance}, on multiplie l'incertitude relative par celle-ci.

\begin{empheq}[box=\fbox]{align*}
&I(a_m)=a_m\cdot i(a_m)\;\text{ou}\;i(a_m)=I(a_m)/a_m\\
&\text{Si }s=l_1\pm\, l_2\;\Rightarrow\;I(s_m)=I(l_{1m})+I(l_{2m})\\
&\text{Si }c=n\cdot b\;\Rightarrow\;I(c_m)=n\cdot I(b_m)\\
&\text{Si }S=l_1\cdot\div l_2\;\Rightarrow\;i(S_m)=i(l_{1m})+i(l_{2m})\\
&\text{Si }c=a^n\;\Rightarrow\;i(c_m)=n\cdot i(a_m)
\end{empheq}

\subsection{Exemples}
Voici finalement quelques exemples simples de calcul de l'incertitude d'équations connues à partir des règles énoncées ci-dessus. Remarquez qu'on calcule ici l'incertitude absolue directement. Si on connaît bien le mécanisme de calcul, il est aussi possible de calculer d'abord les incertitudes relatives des grandeurs mesurées et de les utiliser ensuite pour calculer l'incertitude absolue.
\begin{itemize}
\item Pour obtenir l'incertitude sur l'énergie potentielle\index{incertitude@incertitude!énergie potentielle}~:
\begin{align*}
E_{pot}&=m\cdot g\cdot h\\
I(E_{pot})&=E_{pot}\cdot i(E_{pot})\\
&=m\cdot g\cdot h\cdot i(m\cdot g\cdot h)\\
&=m\cdot g\cdot h\cdot (i(m)+i(g)+i(h))\\
&=m\cdot g\cdot h\cdot (\frac{i(m)}{m}+\frac{i(g)}{g}+\frac{i(h)}{h})\\
\end{align*}
C'est l'exemple le plus simple mettant en jeu des incertitudes relatives.
\item Pour obtenir l'incertitude sur l'énergie cinétique\index{incertitude@incertitude!énergie cinétique}~:
\begin{align*}
E_{cin}&=\frac{1}{2}\cdot m\cdot v^2\\
I(E_{cin})&=E_{cin}\cdot i(E_{cin})\\
&=\frac{1}{2}\cdot m\cdot v^2\cdot i(\frac{1}{2}\cdot m\cdot v^2)\\
&=\frac{1}{2}\cdot m\cdot v^2\cdot (i(\frac{1}{2})+i(m)+i(v^2))\\
&=\frac{1}{2}\cdot m\cdot v^2\cdot (0+i(m)+2\cdot i(v))\\
&=\frac{1}{2}\cdot m\cdot v^2\cdot (\frac{I(m)}{m}+2\cdot \frac{I(v)}{v})
\end{align*}
en remarquant que \(I(2)=0\), puisqu'il n'y a aucune incertitude sur une valeur parfaitement connue.
\item Pour la vitesse finale de chute libre\index{incertitude@incertitude!vitesse chute libre} à vitesse initiale nulle~:
\begin{align*}
v&=\sqrt{2\cdot g\cdot h}\\
I(v)&=v\cdot i(v)\\
&=\sqrt{2\cdot g\cdot h}\cdot i(\sqrt{2\cdot g\cdot h})\\
&=\sqrt{2\cdot g\cdot h}\cdot \frac{1}{2}\cdot i(2\cdot g\cdot h)\\
&=\sqrt{2\cdot g\cdot h}\cdot \frac{1}{2}\cdot (i(2)+i(g)+i(h))\\
&=\sqrt{2\cdot g\cdot h}\cdot \frac{1}{2}\cdot (\frac{I(g)}{g}+\frac{I(h)}{h})
\end{align*}
avec la même remarque que précédemment pour l'incertitude sur la valeur 2.
\end{itemize}