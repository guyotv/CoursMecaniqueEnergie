\chapter{Physique théorique\index{physique@physique!théorique}}\label{physiquetheorique}

\section{Introduction}
Ce chapitre constitue une introduction à la physique théorique. Il présente la notion fondamentale d'action sous la forme la plus simple possible. Il s'inspire des idées de l'excellent ouvrage \emph{Le minimum théorique} de \cite{SU03}. Le formalisme développé dans ces ouvrage est clairement inaccessible ici. C'est pourquoi, un effort important a été fait pour s'en dégager et rester au niveau de mathématiques simples. Si le formalisme mathématique vous intéresse, reportez-vous à cet ouvrage.

Le tableau \ref{energie}, page \pageref{energie}, présente la notion d'énergie comme située au niveau de la vitesse, soit \og une intégration \fg{} au-dessus de l'accélération. La relation fondamentale de la dynamique, soit la seconde loi de Newton, se situant au niveau de l'accélération, déterminer la vitesse se fait mathématiquement par intégration. Par contre, avec l'énergie, une relation directe plus \og physique \fg{} à la vitesse est possible.

\medskip
Dans ce chapitre, on va montrer que la notion d'énergie se trouve au sein d'un principe fondamental qu'on peut voir comme générateur des équations du mouvement, le \emph{principe de moindre action}\index{principe@principe!de moindre action}.

\medskip
Pour le comprendre, il faut revenir sur les deux formes d'énergie potentielle et cinétique.  

\section{Énergie potentielle\index{energie@énergie!potentielle}}
Il s'agit d'un concept fondamental au même titre que l'énergie cinétique.
\begin{quotation}
Le principe de base - qu'on appelle le \emph{principe de l'énergie potentielle} - déclare que toutes les forces proviennent d'une fonction d'énergie potentielle notée V(x). \citep[p. 106]{SU03}
\end{quotation}
Les équations \ref{fderiveenpot} et \ref{fintenpot}, page \pageref{fintenpot} traduisent mathématiquement ce principe~:
\begin{equation}\label{thfderiveenpot}
F=-\frac{d}{dx}E_{pot}
\end{equation}

Et inversement~:
\begin{equation}\label{thfintenpot}
\fbox{\(\displaystyle\Delta E_{pot}=-A=-\int_{A}^{B} F\cdot dx\)}
\end{equation}

Évidemment, la relation est différentielle ou intégrale puisqu'on passe du niveau de l'accélération à celui de la vitesse.

\section{Énergie cinétique}
Rappelons aussi la notion d'énergie cinétique~:
\begin{equation}
E_{cin}=\frac{1}{2}\cdot m\cdot v^2
\end{equation}
qui va donner lieu au \emph{théorème de l'énergie cinétique}.

\section{Quantité de mouvement}
Enfin, rappelons aussi celle de quantité de mouvement et sa conservation exprimée par l'équation \ref{consqtitemvt}, page \pageref{consqtitemvt}~:
\begin{equation}\label{thconsqtitemvt}
\fbox{\(\overrightarrow p=constante\)}
\end{equation}

\section{Conservation de l'énergie}
Il est intéressant de considérer le théorème de conservation de l'énergie mécanique donné par l'équation \ref{consenmec0}, page \ref{consenmec0}~:
\begin{equation}\label{thconsenmec0}
\fbox{\(\displaystyle E_{mec}=const.\)}
\end{equation}
Avec la définition de l'énergie mécanique~:
\begin{equation}\label{thenmec}
E_{m\acute ec}=E_{cin}+E_{pot}=\frac{1}{2}\cdot m\cdot v^{2}+m\cdot g\cdot h
\end{equation}
on peut montrer que celui-ci implique la seconde loi de Newton. En effet, si conservation de l'énergie il y a, c'est que sa dérivée s'annule. On peut donc écrire~:
\begin{align*}
\frac{d}{dt}E_{mec}&=\frac{d}{dt}(E_{cin}+E_{pot})\\
&=\frac{d}{dt}(\frac{1}{2}\cdot m\cdot v^2+E_{pot})\\
&=\frac{1}{2}\cdot m\cdot \frac{d}{dt}v^2+\frac{d}{dt}E_{pot}\\
&=\frac{1}{2}\cdot m\cdot 2\cdot v\cdot \frac{d}{dt}v+\frac{d}{dx}\frac{dx}{dt}E_{pot}\\
&=m\cdot v\cdot \frac{dv}{dt}+\frac{dx}{dt}\frac{d}{dx}E_{pot}\\
&=m\cdot v\cdot a+v\cdot (-\frac{d}{dx}\int_{A}^{B} F\cdot dx)\\
&=v\cdot (m\cdot a-F)=0\;\Rightarrow\\
F&=m\cdot a
\end{align*}
Ce qu'il fallait démontrer.

\section{Principe de moindre action}
La physique newtonienne n'est pas la relativité. La question est de savoir s'il existe une ou des équations générales permettant de traiter ces domaines et d'autres encore de la même manière.

\smallskip
Une grandeur et un principe très général existent en effet. Il s'agit de l'\emph{action lagrangienne} et de sa minimalisation.

\smallskip
Trois éléments sont donc à considérer : Le lagrangien, son action et sa minimalisation.

\subsection{Lagrangien}
Le lagrangien est une grandeur à la fois évidente est étonnante. On part des grandeurs connues d'énergie cinétique et potentielle. Cela paraît normal que la mécanique nécessite ces grandeurs. Mais on construit le lagrangien L à l'inverse de l'énergie mécanique, soit par une différence de ces deux grandeurs~:
\begin{equation}
L=E_{cim}-E_{pot}
\end{equation}
La raison de cette différence tient dans l'obtention de la seconde loi de Newton, comme nous le verrons par la suite.

\subsection{Action}
Nous sommes ici au c\oe ur de cette introduction à la physique théorique. On va voir que l'action et sa minimalisation constituent un principe très général permettant d'obtenir les équations de Newton du mouvement. Pourquoi alors ajouter une couche d'abstraction à celles-ci, alors qu'elles suffisent à décrire le comportement des objets classiques ?

C'est que si la mécanique classique est bien décrite par les équations de Newton, la mécanique quantique et la relativité ne reposent pas sur celles-ci. Par contre, dans tous ces domaines, la minimalisation de l'action reste parfaitement valable et permet d'en obtenir les équations fondamentales.

\begin{quotation}
\og  \emph{L'action se présente comme la sommation, le long du parcours du système, de la différence entre l'énergie cinétique et l'énergie potentielle. La détermination du trajet se fait par une méthode variationnelle : à points extremum fixés, temps de trajet fixé, et trajet variable, on cherche le ou les trajets pour lesquels l'action est stationnaire par rapport aux variations possibles et infimes du trajet.}\fg{}\footnote{Voir Wikipedia : \url{https://fr.wikipedia.org/wiki/Principe_de_moindre_action_et_mécanique_classique}}
\end{quotation}
Pratiquement, on évalue le lagrangien pour chaque petit intervalle de temps et on en effectue la somme sur tout le trajet. Soit~:
\begin{equation}
\fbox{\(\displaystyle A= \int_{t_0}^{t_1}(\frac{1}{2} m\cdot v^2-V) dt\)}
\end{equation}
Comme le lagrangien est la différence entre les énergies cinétique et potentielle, cela revient à évaluer la somme des variation de ces énergies sur le parcourt.

\subsection{Minimalisation}
Chaque parcourt ne va pas transférer son énergie cinétique en énergie potentielle et vice-versa de la même manière. Certains trajets auront des variations instantanées plus petites que d'autres. Au final, ce sera le chemin dont les variations instantanées seront minimales qui deviendra réalité.

\begin{quotation}
\og \textit{En mécanique, le principe de moindre action affirme qu'un corps prend la direction qui lui permet de dépenser le moins d'énergie dans l'immédiat (ou d'acquérir le plus d'énergie dans l'immédiat), en tenant compte qu'il doit y avoir continuité du mouvement (positions et vitesses) s'il y a continuité des conditions physiques.}

\textit{En reliant deux points, la trajectoire prise par le corps n'est pas toujours celle qui lui fait dépenser globalement le moins d'énergie car c'est la dépense immédiate (ou plutôt instantanée) d'énergie qui est minimisée (comme si le corps ne percevait que les conditions de son environnement immédiat) et si le chemin parcouru est long, un chemin plus court avec une dépense d'énergie immédiate plus élevée peut permettre une dépense globale inférieure. Une analogie avec la consommation en carburant d'une voiture peut être faite.}

\textit{Dans ce \og résumé \fg, \og énergie\fg{} signifie énergie cinétique, et une \og dépense d'énergie\fg{} signifie que de l'énergie cinétique se transforme en énergie potentielle.}\fg\endnote{Voir Wikipedia : \url|https://fr.wikipedia.org/wiki/Principe_de_moindre_action|}
\end{quotation}

\begin{quotation}
\og \textit{On peut interpréter cela comme équivalent aux deux conditions suivantes~:}
\begin{itemize}
\item \textit{la trajectoire que suit un corps est celle qui permet la transformation instantanée de l'énergie cinétique en énergie potentielle la plus petite possible (donc aussi la plus lente sur la trajectoire), ou la transformation immédiate dans le sens inverse la plus grande possible (donc la plus rapide possible sur la trajectoire) ;}
\item \textit{la transformation (et donc la trajectoire) est déterminée par les conditions initiales (position et vitesse) et les conditions de l'environnement physique : il doit y avoir continuité de la trajectoire s'il y a continuité du milieu physique.}
\end{itemize}
\textit{Il y a parfois un échange cyclique entre ces deux énergies (balancier sans frottement, satellite à orbite elliptique...) ou une stabilisation provisoire (bille immobile ou posée au fond d'un trou, satellite à orbite circulaire...).}

\textit{La chute libre d'un corps est l'exemple type de la transformation de l'énergie potentielle (gravitationnelle) en énergie cinétique. Le ralentissement et l'arrêt (avant sa chute) d'un corps lancé verticalement est un exemple de la transformation inverse.}

\textit{Les frottements imposent une transformation plus compliquée car ils engendrent de la chaleur, qui est l'énergie cinétique des molécules des matériaux, mais en négligeant cette forme d'énergie, on peut utiliser le Principe de moindre action en considérant que de l'énergie cinétique se perd (sort du système étudié).} \fg{}\endnote{Voir Wikipedia : \url{https://fr.wikipedia.org/wiki/Principe_de_moindre_action_et_mécanique_classique}}
\end{quotation}

Ainsi le principe de moindre action se réalise-t-il et il s'écrit pratiquement~:
\begin{equation}
\partial A = 0
\end{equation}
Mathématiquement cela signifie que toutes les variations sont minimalisées sur l'ensemble de la trajectoire. Il s'agit d'une opération mathématique généralement complexe que nous n'allons envisager ici qu'en une seule dimension pour éviter des différenciations partielles.

\section{Euler-Lagrange}
\begin{quotation}
\og \emph{Cette méthode aboutit aux équations d'Euler-Lagrange qui donnent des trajets sur lesquels l'action n'est pas toujours minimale, mais parfois maximale, voire ni l'un ni l'autre mais seulement stationnaire. Dans tous les cas ces trajets respectent les conditions physiques et sont donc réalistes. Mais le long de chacun de ces trajets, si deux points sont assez proches (mesure faite par la longueur du trajet les séparant) alors on peut démontrer qu'entre eux ce trajet minimise l'action dans la méthode variationnelle, ce qui justifie le nom du principe.} \fg{}\endnote{Voir Wikipedia : \url{https://fr.wikipedia.org/wiki/Principe_de_moindre_action_et_mécanique_classique}}
\end{quotation}

Pour obtenir les équations d'Euler-Lagrange, il faut partir du principe de moindre action~:
\begin{align*}
\partial A&=\partial \left(\int_{t_0}^{t_1}(\frac{1}{2} m\cdot v^2-V)\cdot dt\right)\\
&=\partial \int_{t_0}^{t_1} L\cdot dt=\partial \int_{t_0}^{t_1} L(t,x,v(t))\cdot dt
\end{align*}
Comme le lagrangien contient non seulement la variable t, mais x(t) et la vitesse v dérivée de x(t), minimiser celui-ci n'est pas simple. Le lagrangien est ce qu'on appelle une fonctionnelle et on peut montrer que sa minimisation produit les équations d'Euler-Lagrange qui s'expriment en une dimension par~:
\begin{equation}
\fbox{\(\displaystyle \frac{d}{dt}\frac{\partial L}{\partial v}-\frac{\partial L}{\partial x}=0 \)}
\end{equation}
Cette équation est fondamentale, mais elle n'est pas évidente. Pour tenter de la comprendre, nous allons l'appliquer dans des cas très simples.