\myclearpage

\chapter{Énergies}
\section{Introduction}
Cette annexe est destinée à aller un peu plus loin que ce qui figure dans le cours concernant les différentes énergies. Elle est faite de quelques exemples particuliers qui peuvent fixer les idées, les ordres de grandeur, les problèmes, etc. Ce domaine étant très vaste, il ne s'agit ici que de donner quelques exemples, sans plus.

\section{Énergie hydraulique\index{energie@énergie!hydraulique}}\label{barrageduchatelot}
On s'intéresse ici au barrage\index{barrage} du Châtelot, entré en service en novembre 1953, sur le Doubs dans le Jura suisse et français. Ce barrage a une hauteur de \unit{74}{\metre}. Par ailleurs, les turbines\index{turbine} ne se trouvent pas à son pied, mais plus bas au bord de l'eau. Celle-ci est amenée par une conduite forcée\index{conduite forcee@conduite forcée} de trois kilomètres selon une pente de 4 \textperthousand, ce qui représente un hauteur supplémentaire de \unit{12}{\metre}. Au total, la hauteur est donc de \unit{86}{\metre}. Le Doubs a un débit moyen de \unit{25}{\metre\cubed\per\second}. Mais le débit aménagé\index{debit@débit!aménagé}, c'est-à-dire le débit maximal produisant de l'énergie, est de \unit{44}{\metre\cubed\per\second}.

\begin{figure}[t]
\centering
\caption[Le barrage du Châtelot]{Le barrage du Châtelot\label{chatelot@châtelot} \par \scriptsize{Sur le Doubs dans le jura.}}
\includegraphics[width=7cm]{Chatelot.eps}
\end{figure}

Le débit de restitution\index{debit@débit!de restitution} étant fixé par la loi française à environ 10\% du débit moyen de la rivière, un turbinage minimum permanent de \unit{2}{\metre\cubed\per\second} est réalisé (pendant les phases de remplissage du barrage). A partir de ces données on peut estimer la puissance moyenne \(\overline{P}\) de chute :
\begin{align*}
\overline{P}&=\eta\cdot Q\cdot g\cdot h\\
&=0,8\cdot 25\cdot 9,81\cdot 86=\unit{16'873}{\kilo\watt}
\end{align*}
Cela représente une énergie totale par année de :
\begin{align}
E&=16'873\cdot 24\cdot 365\notag\\
&=147,807\,\text{millions\,de\,\kilowatthour}\notag\\
&=\unit{147'807}{\mega\watt\hour}\label{prodchatelot}
\end{align}
Évidemment, la puissance installé doit être plus importante pour faire face à un débit plus important. On a :
\begin{align*}
P_{inst}&=\eta\cdot Q\cdot g\cdot h\\
&=0,8\cdot 44\cdot 9,81\cdot 86\cong\unit{30'000}{\kilo\watt}
\end{align*}
La puissance installé est de \unit{15'000}{\kilo\watt} pour chacune des deux turbines. La puissance totale installée\index{puissance!installée} est donc de \unit{30'000}{\kilo\watt}.

On peut alors évaluer approximativement le nombre de ménages que peut alimenter le barrage du Châtelot. En effet, sachant que ce barrage fournit la moitié de son énergie à la France et l'autre moitié à la Suisse\footnote{Selon la Convention franco-suisse du 19 novembre 1930.} et que la consommation électrique moyenne par ménage suisse est d'environ \unit{2000}{\kilowatthour\per an}, on couvre les besoins de :
\[n=\frac{147,807\cdot 10^6}{2\cdot 2000}=36'951\,\text{ménages}\]
Très grossièrement, on peut dire que le barrage couvre trois fois les besoins des ménages (sans les entreprises, commerces et administrations) de la ville de la Chaux-de-Fonds (\(\sim\) 30'000 personnes c'est-à-dire, à trois personnes par ménages, approximativement 10'000 ménages).

L'énergie électrique est distribuée vers la Suisse par quatre lignes haute tension de \unit{60'000}{\volt} chacune et par deux lignes sous la même tension vers la France.

\section{Énergie éolienne}

\subsection{Règle de Betz\index{regle de Betz@règle de Betz}}

Démonstration de la règle de Betz :\label{reglebetz}

On imagine une éolienne\index{eolienne@éolienne} brassant à l'aide de ses pales\index{pale} une surface \(S\). Si la vitesse du vent en amont est \(v_{amont}\), en un temps donné \(t\) toutes les particules se trouvant jusqu'à une distance \(d=v_{amont}\cdot t\) d'elle la traverseront. Le volume d'air qu'elle brassera sera donc de :
\[V=S\cdot d=S\cdot v_{amont}\cdot t\]
En considérant la masse volumique de l'air \(\rho\), on calcule la masse d'air que cela représente par :
\[\rho=\frac{m}{V}\;\Rightarrow\;m=\rho\cdot V=\rho\cdot S\cdot v_{amont}\cdot t\]
On peut alors calculer la puissance correspondant au déplacement de cette masse d'air :
\begin{align*}
P&=\frac{E_{cin}}{t}=\frac{\frac{1}{2}\cdot m\cdot v_{amont}^2}{t}\\
&=\frac{1}{2}\cdot \frac{m}{t}\cdot v_{amont}^2\\
&=\frac{1}{2}\cdot \rho\cdot S\cdot v_{amont}^3
\end{align*}
Si maintenant on comprend qu'une partie de cette énergie est prise au vent par l'éolienne, on se rend compte que cela implique une diminution de sa vitesse en aval. Soit \(v_{aval}\) cette vitesse. On peut considérer que la même masse d'air traverse l'éolienne, mais à une vitesse moyenne plus faible. On peut donc admettre (cela se justifie) que cette vitesse est la moyenne des vitesses en amont et en aval :
\[v_{moy}=\frac{v_{amont}+v_{aval}}{2}\]
Ainsi, le volume traversant l'éolienne est :
\[V=S\cdot v_{moy}\cdot t=S\cdot \frac{v_{amont}+v_{aval}}{2}\cdot t\]
Et la masse est alors :
\[m=\rho\cdot S\cdot \frac{v_{amont}+v_{aval}}{2}\cdot t\]
La diminution de la puissance du flux d'air qui correspond à la puissance reçue par l'éolienne est ainsi :
\begin{align*}
\Delta P&=\frac{\Delta E_{cin}}{t}=\frac{E_{cin}^{amont}-E_{cin}^{aval}}{t}\\
&=\frac{\frac{1}{2}\cdot m\cdot v_{amont}^2-\frac{1}{2}\cdot m\cdot v_{aval}^2}{t}\\
&=\frac{1}{2}\cdot \frac{m}{t}\cdot (v_{amont}^2-v_{aval}^2)\\
&=\frac{1}{2}\cdot \rho\cdot S\cdot \frac{v_{amont}+v_{aval}}{2}\cdot (v_{amont}^2-v_{aval}^2)\\
&=\frac{1}{4}\cdot \rho\cdot S\cdot (v_{amont}^2-v_{aval}^2)\cdot (v_{amont}+v_{aval})
\end{align*}
Si on cherche quelle est la vitesse en aval \(v_{av}\) qui donne un maximum de puissance, il faut dériver la fonction \(\Delta P\) par rapport à \(v_{av}\) et l'annuler. Pour écourter la notation, on pose encore : \(v_{amont}=v_{am}\).
\begin{gather*}
\frac{d\Delta P}{dv_{av}}=\frac{d}{dv_{av}}(\frac{1}{4}\cdot \rho\cdot S\cdot (v_{am}^2-v_{av}^2)\cdot (v_{am}+v_{av}))\\
=\frac{1}{4}\cdot \rho\cdot S\cdot \frac{d}{dv_{av}} ((v_{am}^2-v_{av}^2)\cdot (v_{am}+v_{av}))=0
\end{gather*}
L'équation à résoudre alors est :
\begin{align*}
\frac{d}{dv_{av}} ((v_{am}^2-v_{av}^2)\cdot (v_{am}+v_{av}))&=0\;\Rightarrow\\
\frac{d}{dv_{av}} (v_{am}^3+v_{am}^2\cdot v_{av}-v_{av}^2\cdot v_{am}-v_{av}^3)&=0\;\Rightarrow\\
v_{am}^2-2\cdot v_{am}\cdot v_{av}-3\cdot v_{av}^2&=0
\end{align*}
Il s'agit d'une équation du second degré en \(v_{av}\) dont la solution est :
\begin{align*}
v_{av}&=\frac{2\cdot v_{am}\pm \sqrt{(2\cdot v_{am})^2-4\cdot (-3)\cdot v_{am}^2}}{2\cdot (-3)}\\
&=\frac{2\cdot v_{am}\pm \sqrt{16\cdot v_{am}^2}}{-6}\\
&=\frac{2\cdot v_{am}\pm 4\cdot v_{am}}{-6}\\
&=\frac{v_{am}\pm 2\cdot v_{am}}{-3}=\begin{cases}v_{am}/3\\-v_{am}\end{cases}
\end{align*}
Évidemment \(v_{aval}>0\) et donc seule la solution positive à un sens. On a donc finalement :
\begin{equation}
v_{aval}=\frac{v_{amont}}{3}
\end{equation}
Et la conséquence pour la variation de puissance est que :
\begin{align*}
\Delta P&=\frac{1}{4}\cdot \rho\cdot S\cdot (v_{am}^2-v_{av}^2)\cdot (v_{am}+v_{av})\\
&=\frac{1}{4}\cdot \rho\cdot S\cdot (v_{am}^2-(v_{am}/3)^2)\cdot (v_{am}+v_{am}/3)\\
&=\frac{1}{4}\cdot \rho\cdot S\cdot (\frac{9\cdot v_{am}^2-v_{am}^2}{9})\cdot (\frac{3\cdot v_{am}+v_{am}}{3})\\
&=\frac{1}{4}\cdot \rho\cdot S\cdot (\frac{8\cdot v_{am}^2}{9})\cdot (\frac{4\cdot v_{am}}{3})\\
&=\frac{16}{27}\cdot \frac{1}{2}\cdot \rho\cdot S\cdot v_{am}^3\\
&=\frac{16}{27}\cdot P=0,57\cdot  P
\end{align*}
Ce qui signifie que la puissance maximale que l'éolienne peut développer représente les 16/27 de la puissance du vent, soit 57\% de celle-ci :
\begin{equation}
P_{\acute eolienne}=\frac{16}{27}\cdot P_{vent}
\end{equation}
Cela constitue la limite de Betz\index{limite de Betz}.

\subsection{Éoliennes\index{eolienne@éolienne}}

\subsubsection{Éolienne de Collonges-Dorénaz}
Il ne s'agit pas ici de se substituer aux multiples informations qui se trouvent sur internet\endnote{Voir le site de RhônEole : \url=http://www.rhoneole.ch/=}. Il s'agit simplement d'illustrer la théorie à travers l'exemple concret de la plus grande éolienne de Suisse pour permettre une comparaison avec un barrage comme celui du Châtelot (voir paragraphe \ref{barrageduchatelot}).

Le mât\index{mat@mât} fait pratiquement \unit{100}{\metre} de haut et la longueur des pales\index{pale} est de \unit{33}{\metre}. Le rendement est très proche de la limite de Betz\index{limite de Betz} puisqu'il vaut 56\%. La puissance maximale est de \unit{2000}{\kilo\watt}, mais la production annuelle est de \unit{3,5}{millions\thickspace de\thickspace\kilowatthour}. Si la consommation électrique annuelle moyenne d'un ménage est d'environ \unit{2000}{\kilowatthour}, le nombre \(n\) de ménages qui peuvent être alimentés par cette éolienne vaut :
\[n=\frac{3,5\cdot 10^6}{2000}=1750\,\text{ménages}\]
Comparé au 74'000 ménages alimentés par le barrage\index{barrage} du Châtelot (parties suisse et française ensemble), cela peut paraître bien peu. Encore faut-il comparer le coût du barrage aux quatre millions d'investissement pour cette éolienne. Et aussi comparer les deux impacts écologiques, les possibilités et le coût de démontage, les risques d'accidents~\dots\ Une juste comparaison nécessite de prendre en compte un nombre de paramètres assez grand pour qu'il ne soit pas possible ici de poursuivre plus avant la comparaison.

\subsubsection{Éoliennes du Mont Soleil (Jura suisse)}
A nouveau l'information se trouve sur internet\endnote{Voir le site de Juvent : \url=http://www.juvent.ch/=}.

Il faut savoir que cette centrale est constituée de huit éoliennes\index{eolienne@éolienne} d'une puissance allant de \unit{600}{} à \unit{1750}{\kilo\watt}. Elles ont une hauteur de mât\index{mat@mât} de \unit{45}{} à \unit{67}{\metre} et des pales\index{pale} de \unit{22}{} à \unit{33}{\metre}. L'ensemble a produit en 2006 une énergie de \unit{9,176}{millions\thickspace de\thickspace\kilowatthour}. Cela représente 4588 ménages (à \unit{2000}{\kilowatthour\per an}). Une fois encore c'est bien peu comparé au barrage\index{barrage} du Châtelot. Mais les remarques faites précédemment restent valables.

Au total, la centrale de Collonges-Dorénaz et celle du Mont Soleil alimentent ensemble environ 6300 ménages, soit très approximativement la moitié d'une ville comme La Chaux-de-Fonds en Suisse.

%\section{Énergie solaire}
%\subsection{Solaire thermique}
%Le solaire thermique est mal connu. Pourtant, il constitue une excellente manière de produire de la chaleur pour les habitations privées.

%\subsection{Solaire électrique}
%Si on évoque souvent le faible rendement d'environ $15\%$ des cellules solaires électriques, on ignore aussi trop souvent les paramètres nécessaires pour envisager son utilisation pour des habitations privées.

\section{Géothermie\index{geothermie@géothermie}}\label{riehen}
IL existe en Suisse une centrale géothermique qui permet un chauffage urbain, à l'instar de Cridor à La Chaux-de-Fonds. Il s'agit de la centrale de Riehen près de Bâle. Elle est constituée de deux pompes à chaleur qui exploitent l'eau provenant d'un forage\index{forage} à $1547\,m$ amenant une eau à $65^{\circ}$. Un second forage, distant de $1\,km$ du premier, réinjecte de l'eau froide à $1247\,m$. Le débit est de $18\,l/s$. Elle permet d'approvisionner 180 immeubles à Riehen et de nouvelles constructions en Allemagne. L'énergie annuellement produite est de $22,8\,GWh$\endnote{Voir les sites de Géothermie : \url=http://www.geothermal-energy.ch/= et \url=http://www.ader.ch/energieaufutur/energies/geothermie/index2.php=}. À raison d'environ $20\,MWh/an$ pour une famille de trois personnes, on a une couverture en terme de chauffage à distance (chauffage et eau chaude sanitaire) de :
\[n=\frac{22'800}{20}=1'140\,\text{ménages}\]
Ce qui représente environ $3'500\,\text{personnes}$.

\section{Énergie de combustion des déchets\index{energie@énergie!de combustion!des déchets}}
Nous allons ici prendre pour exemple de ce type de production énergétique la centrale de chauffage à distance de Cridor à La Chaux-de-Fonds dans le jura suisse. L'objectif est d'avoir un exemple concret de ce qui se fait déjà dans un domaine concernant les énergies renouvelables qui passe souvent trop inaperçu.

La production\endnote{voir dépliant ``Nos déchets = notre énergie'' sur le site : \url=http://www.cridor.ch/content/doc/brochures.php=} totale d'énergie par cette usine d'incinération est de $85'000\,MWh/an$. Par comparaison, rappelons que la production du barrage\index{barrage} du Chatelot est de $150'000\,MWh/an$ (voir équation \ref{prodchatelot}), soit environ le double.

Cette énergie se partage en $60'000\,MWh/an$ pour le chauffage à distance (chauffage des habitations) et $25'000\,MWh/an$ produit par une turbine sous forme électrique, dont $19'000\,MWh/an$ sont vendus. Cela représente $55'000\,t/an$ de déchets incinérés. Le nombres de ménages fournis en énergie électrique est donc de :
\[n=\frac{19'000\cdot 10^6}{2000\cdot 10^3}=9500\,\text{ménages}\]
Pour l'énergie thermique, en comptant très approximativement $20\,MWh/an$ pour une famille de trois personnes, on a une couverture en terme de chauffage à distance (chauffage et eau chaude sanitaire) de :
\[n=\frac{60'000}{20}=3000\,\text{ménages}\]
Ce qui représente environ $10'000\,\text{personnes}$.

%\section{Énergie du gaz}

%\section{Analyse des besoins énergétiques d'une maison}