set terminal latex rotate
set output 'CoursMecaniqueOSDF-gnuplottex-fig1.tex'
# domaine de définition
        #set xrange [0:1E11]
        #set yrange [-1E40:1E40]
        set xrange [0:5]
        set yrange [-1E46:1E45]
        # flèches
        #set arrow 1 from 39.7,120 to 39.7,0 head filled
        #size screen 0.5,30
        #set label "Écart type" at 39.4,100 rotate by 90
        #set arrow 2 from 44.1,120 to 44.1,0 head filled
        #set label "Écart type" at 44.4,138 rotate by -90
        #set arrow 3 from 41.9,120 to 41.9,0 head filled
        #set label "Moyenne" at 42.2,80 rotate by -90
        #set grid
        #set title "Baguettes d'une année"
        # suppression de la légende
        #set key off
        set key right bottom
        # légendes des axes
        set xlabel "Distance (m)"
        #set ylabel "Gravitation" rotate by 90
        # largeur des colonnes (boxes)
        #set boxwidth 0.1
        #set arrow from graph 1,0 to graph 1.05,0 size screen 0.025,15,60 filled ls 11
#set arrow from graph 0,1 to graph 0,1.05 size screen 0.025,15,60 filled ls 11
set xzeroaxis
G=6.67E-11
M=1.99E30
m=5.97E24
F(x)=-G*M*m/x**2
E(x)=-G*M*m/x
        # tracé du graphe
        plot E(x) title "Énergie potentielle (J)", F(x) title "Force de gravitation (N)"
