\begin{Solution OS}{1}
	La masse m constitue évidemment le système. On choisit un système d'axes vertical et horizontal dirigé vers le haut et dans le sens de l'accélération.

	Les forces extérieures exercées sur la masse sont au nombre de trois~: son poids P, la réaction du plan R et la force F.
	\begin{figure}[ht]
	\caption[Force inclinée]{Une force inclinée}\label{forceinclinee}
	\smallskip
	\begin{center}
	\def\svgwidth{6cm}
	\input{Annexe-Exercices/Images/forceinclinee.eps_tex}
	\end{center}
	\end{figure}
	La figure \ref{forceinclinee} présente la situation. On peut y voir les trois forces extérieures et, en rouge , la décomposition de la force F en ses composantes sur les axes (remarquez qu'on a représenté les composantes par des vecteurs, ce qui est courant en physique lors de la décomposition de forces parce qu'on peut alors comprendre celle-ci comme un remplacement de la force F par deux forces qui en forment la somme).

	L'équation de Newton sous forme vectorielle donne alors les équations du mouvement en composantes~:
	\begin{align*}
	\sum \overrightarrow{F}^{ext}=\overrightarrow{R}+\overrightarrow{P}+\overrightarrow{F}&=m\cdot \overrightarrow{a}\;\Rightarrow\\[0.8em]
	F_x&=m\cdot a\\
	R-P-F_y&=0
	\end{align*}
	car l'accélération selon l'axe y est nulle puisque la masse ne se déplace par sur cet axe.

	En considérant le triangle formé par la force F, sa composante sur l'axe x et l'angle \(\alpha\), on peut écrire~:
	\[F_x=F\cdot \cos(\alpha)\;\;\text{et}\;\;F_y=F\cdot \sin(\alpha)\]
	et à l'aide de la définition du poids (\(P=m\cdot g\)), préciser les équations du mouvement~:
	\begin{align*}
	F\cdot \cos(\alpha)&=m\cdot a\\
	R-m\cdot g-F\cdot \sin(\alpha)&=0
	\end{align*}
	Cela permet finalement les résultats~:
	\begin{align*}
	a&=\frac{F\cdot \cos(\alpha)}{m}\\
	&=\frac{5\cdot \cos(30)}{15}=\SI{0,29}{\metre\per\second\squared}\\
	R&=m\cdot g+F\cdot \sin(\alpha)\\
	&=15\cdot 9.81+5\cdot \sin(30)=\SI{149,65}{\newton}
	\end{align*}
	
\end{Solution OS}
\begin{Solution OS}{2}
	Commençons par trouver les forces exercées par les câbles sur la lampe. Considérons donc la lampe comme système. Pour des câbles souples la force est exercée le long du câble. La situation est donc celle de la figure \ref{lampe}.
	\begin{figure}[ht]
	\caption[Lampe suspendue]{Une lampe suspendue}\label{lampe}
	\smallskip
	\begin{center}
	\def\svgwidth{6cm}
	\input{Annexe-Exercices/Images/lampe.eps_tex}
	\end{center}
	\end{figure}
	Où R et F sont les tension dans les câbles. Dans le système d'axes présenté, on peut écrire l'équation du mouvement~:
	\begin{align*}
	\sum \overrightarrow{F}^{ext}=\overrightarrow{R}+\overrightarrow{P}+\overrightarrow{F}&=0\;\Rightarrow\\[0.8em]
	F_x-R_x&=0\\
	F_y+R_y-P&=0
	\end{align*}
	car le système est statique et l'accélération nulle.

	En considérant que le poids \(P=m\cdot g\) et les triangles d'angles au sommet \(\alpha\) et \(\beta\), grâce à la trigonométrie, on peut écrire~:
	\[F_x=F\cdot \cos(\beta)\;\;\textbf{et}\;\;R_x=R\cdot \cos(\alpha)\]
	et préciser les équations du mouvement~:
	\begin{align*}
	F\cdot \cos(\beta)-R\cdot \cos(\alpha)&=0\\
	F\cdot \sin(\beta)+R\cdot \sin(\alpha)-m\cdot g&=0
	\end{align*}
	Il s'agit de deux équations à deux inconnues F et R. Pour les résoudre, on tire F de la première et on la remplace dans la seconde~:
	\begin{align*}
	F&=R\cdot\frac{\cos(\alpha)}{\cos(\beta)}\;\;\Rightarrow\\
	R&\cdot\frac{\cos(\alpha)}{\cos(\beta)}\cdot \sin(\beta)+R\cdot \sin(\alpha)-m\cdot g=0\;\Rightarrow\\
	R&\cdot(\cos(\alpha)\cdot\tan(\beta)+\sin(\alpha))=m\cdot g\;\;\Rightarrow\\
	R&=\frac{m\cdot g}{\cos(\alpha)\cdot\tan(\beta)+\sin(\alpha)}\\
	&=\frac{7\cdot 9,81}{\cos(20)\cdot\tan(30)+\sin(20)}=\SI{77,63}{\newton}
	\end{align*}
	et donc pour F~:
	\begin{align*}
	F&=R\cdot\frac{\cos(\alpha)}{\cos(\beta)}\\
	&=77,63\cdot \frac{\cos(20)}{\cos(30)}=\SI{84,24}{\newton}
	\end{align*}
	
\end{Solution OS}
\begin{Solution OS}{3}
	On commence par choisir le système. Pour éviter de devoir calculer la tension dans la corde, on le choisit comme constitué de la corde et des deux masses M et m.

	Les forces extérieures sont alors au nombre de trois. Le plan horizontal exerce sur la masse M une force de soutient verticale égale et opposée à son poids (mais qui ne sont pas l'action et la réaction l'une de l'autre), puisque la masse se déplace horizontalement. Ces deux forces s'annulent donc. Reste le poids de la masse m, seule force extérieure à agir pour accélérer le système.

	On peut donc écrire, selon la seconde loi de Newton~:
	\[\sum F^{ext}=m\cdot g=(M+m)\cdot a\]
	où M+m est la masse du système. Ainsi, finalement, on peut calculer la valeur de l'accélération~:
	\begin{align*}
	&m\cdot g=(M+m)\cdot a\;\Rightarrow\\
	&a=\frac{m}{M+m}\cdot g=\frac{2}{5}\cdot 9,81=\SI{3,924}{\metre\per\second\squared}
	\end{align*}
	\medskip
	À partir de l'accélération, on peut ensuite calculer la vitesse au bout d'un mètre, grâce à l'équation du MRUA~:
	\begin{align*}
	v^2&=v_0^2+2\cdot a\cdot d\;\Rightarrow\\
	v^2&=0+2\cdot 3,924\cdot 1\;\Rightarrow\\
	v&=\sqrt{2\cdot 3,924}=\SI{2,8}{\metre\per\second}
	\end{align*}
	
\end{Solution OS}
\begin{Solution OS}{4}
	Deux forces agissent ici~: la réaction du plan, qui lui est normale (c'est-à-dire perpendiculaire), et le poids de la masse. Comme la réaction du plan n'a aucune composante parallèlement au plan, elle ne peut être responsable de l'accélération de la masse le long de celui-ci.

	Il faut donc trouver la composante du poids qui est parallèle au plan incliné. L'angle entre le poids et un plan horizontal est de \SI{90}{\degree}. Quand le plan est incliné, cet angle diminue de la valeur de l'inclinaison. L'angle \(\beta\) entre le plan incliné et le poids et donc \(\beta=90-\alpha\).

	Comme la projection du poids selon l'angle \(\beta\) correspond à sa composante parallèle au plan, dans le triangle rectangle composé du poids comme hypoténuse et de ses composantes parallèle et perpendiculaire au plan, la composante parallèle au plan correspond au côté adjacent. Ainsi, on peut écrire~:
	\begin{align*}
	P_{//}&=P\cdot\cos(\beta)=P\cdot\cos(90-\alpha)\\
	&=P\cdot\sin(\alpha)
	\end{align*}
	La seconde loi de Newton s'écrit donc le long du plan incliné~:
	\begin{align*}
	\sum F^{ext}=P\cdot\sin(\alpha)&=m\cdot a\;\Rightarrow\\
	m\cdot g\cdot\sin(\alpha)&=m\cdot a\;\Rightarrow\\
	a&=g\cdot\sin(\alpha)\;\Rightarrow\\
	a&=9,81\cdot\sin(20)=\SI{3,36}{\metre\per\second\squared}
	\end{align*}
	Avec une vitesse initiale nulle, pour un MRUA d'accélération calculée ci-dessus, la vitesse au bout d'un temps t=\SI{2}{\second} s'obtient par~:
	\[v=a\cdot t+v_0=3,36\cdot 2=\SI{9,72}{\metre\per\second}\]

	\medskip
	Une autre manière de résoudre le problème est de procéder avec méthode. Le système qu'on doit choisir est bien évidemment la masse m, puisque c'est de celle-ci qu'on cherche l'accélération pour en trouver la vitesse au bout de \SI{2}{\second}.
	\begin{figure}[ht]
	\centering
	\caption[Plan incliné]{Le plan incliné}\label{incline}
	\medskip
	\def\svgwidth{5cm}
	\input{Annexe-Exercices/Images/incline.eps_tex}
	\end{figure}

	La figure \ref{incline} présente ensuite le dessin des forces extérieures et le choix du système d'axes. Remarquez que ce dernier l'a été selon l'inclinaison du plan. Il aurait pu ne pas en être ainsi, mais ce choix simplifie les calculs, car la masse étant contrainte à se déplacer le long du plan, son accélération perpendiculairement est nulle. Les équations de la seconde loi de Newton, obtenues par projection des forces extérieures et de l'accélération selon les axes, peuvent alors s'écrire~:
	\begin{align*}
	\sum F^{ext}_x&=P_x=m\cdot a_x &\text{sur l'axe x}\\
	\sum F^{ext}_y&=R-P_y=m\cdot a_y=0 &\text{sur l'axe y}
	\end{align*}
	car l'accélération perpendiculairement au plan est nulle, comme déjà mentionné.

	\smallskip
	Si on considère l'angle \(\alpha\), en s'imaginant le plan incliné horizontal, on comprends qu'il se reporte entre le vecteur poids \(\overleftarrow{P}\) et sa composante selon y \(P_y\).

	Avec le triangle rectangle formé par le poids et ses composantes et un peu de trigonométrie, on peut en déduire~:
	\begin{align*}
	P_x &= P\cdot \sin(\alpha)\\
	P_y &= P\cdot \cos(\alpha)
	\end{align*}
	Comme par ailleurs on sait que \(P=m\cdot g\), on peut réécrire les équations de Newton sur les axes comme~:
	\begin{align*}
	\sum F^{ext}_x&=m\cdot g\cdot \sin(\alpha)=m\cdot a_x\\
	\sum F^{ext}_y&=R-m\cdot g\cdot \cos(\alpha)=m\cdot a_y=0
	\end{align*}
	La première de ces équations permet de trouver l'accélération du bloc selon le plan incliné~:
	\begin{align*}
	a=a_x&=g\cdot \sin(\alpha)\\
	&=9,81\cdot\sin(20)=\SI{3,36}{\metre\per\second\squared}
	\end{align*}

	\smallskip
	Mais une information supplémentaire nous est donnée par la seconde équation, c'est la valeur de la réaction R au plan~:
	\begin{align*}
	R&=m\cdot g\cdot \cos(\alpha)\\
	&=3\cdot 9,81\cdot \cos(20)=\SI{27,67}{\newton}
	\end{align*}

	\smallskip
	Ensuite, le problème se résout de la même manière que précédemment.
	
\end{Solution OS}
\begin{Solution OS}{5}
	Le problème peut paraître complexe du fait de la présence de deux objets distincts se déplaçant selon deux axes différents. Pourtant, le fait que la corde soit inextensible fait de l'ensemble des deux masse et de la corde un système se déplaçant avec la même accélération. De plus, pour autant qu'on considère correctement l'action des forces sur chaque masse, on peut s'imaginer ce système se déplaçant d'un bloc horizontalement.

	\smallskip
	Comme on ne connaît pas la tension dans la corde (on ne peut s'imaginer à priori qu'elle correspond au poids de la masse m), le choix du système comprenant les deux masses et la corde s'impose, car ainsi la tension dans la corde, en tant que force intérieure, n'apparaîtra pas dans les équations de Newton.

	\smallskip
	Comme déjà dit, on peut considérer le système d'un seul tenant. On va donc imaginer un axe suivant la corde et orienté vers le bas du plan incliné, car la masse M étant plus grande que m, il est évident que le mouvement se fera dans ce sens. Ainsi, le signe de l'accélération sera positif.

	\smallskip
	Reste à considérer les forces extérieures. Elles sont au nombre de quatre~:
	\begin{enumerate}
	\item le poids P de la masse M,
	\item la réaction R du plan incliné,
	\item celui p de la masse et
	\item la force exercée sur la corde par la poulie.
	\end{enumerate}

	La dernière est toujours perpendiculaire à la corde et ne participe donc pas au mouvement des masses. La troisième est toujours parallèle à la corde. La seconde est toujours perpendiculaire au plan incliné et ne participe elle aussi pas au mouvement. La première à une composante perpendiculaire au plan incliné et ne participe pas au mouvement, mais aussi une composante parallèle à ce plan et doit être considérée.
	Avec l'angle \(\alpha\) défini, on peut reprendre le raisonnement évoqué au problème \ref{planinclinesimple}, évoquant le triangle rectangle formé par le poids de la masse M et se composantes et affirmant que l'angle \(\alpha\) est celui entre le poids et sa composante perpendiculaire au plan, pour écrire que la composante parallèle au plan vaut~:
	\[P_{//}=M\cdot g\cdot \sin(\alpha)\]

	\smallskip
	À partir de là, on peut écrire l'équation du mouvement du système (des deux masse et de la corde)~:
	\[\sum F^{ext}=M\cdot g\cdot \sin(\alpha)-m\cdot g=(M+m)\cdot a\]
	et calculer l'accélération~:
	\begin{align*}
	a&=\frac{M\cdot g\cdot \sin(\alpha)-m\cdot g}{M+m}\\
	&=\frac{5\cdot 9,81\cdot \sin(30)-3\cdot 9,81}{5+3}\\
	&=\SI{-0,6}{\metre\per\second\squared}
	\end{align*}
	Le signe négatif signifie que la masse M monte vers le haut du plan incliné.

	\medskip
	Pour calculer la tension dans la corde, il est indispensable de changer de système pour la faire apparaître en tant que force extérieure dans l'équation de Newton.

	Trois éléments peuvent prétendre servir de système.

	La corde en premier lieu. Si on la considère seule, à l'une de ses extrémités la masse m exerce sur elle une force \(T_m\) et à l'autre la masse M exerce une tension à priori différente \(T_M\). La force exercée par la poulie reste perpendiculaire et ne contribue pas au mouvement. On peut donc écrire~:
	\[T_M-T_m=\mu\cdot a\]
	où \(\mu\) est la masse de la corde. Or, si cette masse est nulle, indépendamment de l'accélération, le deux tensions sont égales. Cela est évidemment valable pour tous les éléments de la corde dont on dira donc qu'elle exerce une force \(T\) à déterminer.

	Le système corde ne permet donc pas de la trouver.

	Restent les deux masses. Pour la masse M interviendra dans l'équation du mouvement un sinus qu'on va éviter en considérant m.

	Sur m, avec un axe pointant toujours vers le haut, l'équation de Newton devient très simple~:
	\begin{align*}
	\sum F{ext}&=T-m\cdot g=m\cdot a\;\Rightarrow\\
	T&=m\cdot (a+g)=3\cdot (-0,6+9,81)\\
	&=\SI{27,6}{\newton}
	\end{align*}

	\medskip
	Pour vérifier ce résultat, choisissons l'autre masse (M) pour système. On écrira alors~:
	\begin{align*}
	\sum F^{ext}&=-T+M\cdot g\cdot \sin(\alpha)=M\cdot a\;\Rightarrow\\
	T&=M\cdot g\cdot \sin(\alpha)-M\cdot a\\
	&=5\cdot 9,81\cdot \sin(30)-5\cdot (-0,6)\\
	&=\SI{27,6}{\newton}
	\end{align*}
	
\end{Solution OS}
\begin{Solution OS}{6}
	Pour calculer la vitesse de chute, il suffit de trouver la vitesse d'un objet en chute libre au bout de \SI{2}{\metre} de déplacement. Avec les équations du MRUA, on a~:
	\begin{align*}
	v&=\sqrt{2\cdot g\cdot h}=\sqrt{2\cdot 9.81\cdot 2}\\
	&=\SI{6,26}{\metre\per\second}
	\end{align*}

	Ensuite, si on considère que la descente en parachute se fait à vitesse constante, c'est-à-dire avec une accélération nulle, on peut écrire~:
	\begin{align*}
	\sum F^{ext}&=-T+m\cdot g=m\cdot a=0\;\Rightarrow\\
	T&=m\cdot g=70\cdot 9,81=\SI{686,7}{\newton}
	\end{align*}
	En d'autre termes, le parachute n'a à supporter que le poids du parachutiste.
	
\end{Solution OS}
\begin{Solution OS}{7}
	\begin{enumerate}
	\item Avec pour système la nacelle, la corde et la masse, les deux seules forces extérieures sont le poids P et la force exercée par le ballon F. Tout se déroulant sur un axe vertical, qu'on choisira vers le haut, on peut écrire la seconde loi sur cet axe~:
	\[\sum F^{ext}=F-(M+m)\cdot g=(M+m)\cdot a=0\]
	puisque l'accélération est nulle. Ainsi, la force F exercée par le ballon sur la nacelle est simplement égale au poids du système~:
	\[F=(M+m)\cdot g=180\cdot 9,81=\SI{1765,8}{\newton}\]

	\smallskip
	Le même raisonnement vaut pour la masse suspendue dont l'accélération est nulle. Ainsi la tension T dans la corde vaut exactement le poids de la masse pendante~:
	\[T=m\cdot g=80\cdot 9,81=\SI{784,8}{\newton}\]
	\item À vitesse constante, la force permettant au ballon de s'élever est donc égale au poids de ce qu'il soulève. La force d'ascension vaut donc \(F=\SI{1765,8}{\newton}\).

	Si on lâche \SI{20}{\kilo\gram} de lest, la masse de la nacelle devient égale à \SI{80}{\kilo\gram}. La seconde loi de Newton permet alors de calculer l’accélération~:
	\begin{align*}
	\sum F^{ext}&=F-(M+m)\cdot g=(M+m)\cdot a\;\Rightarrow\\
	a&=\frac{F}{M+m}-g=\frac{1765,8}{80+80}-9,81\\
	&=\SI{1,23}{\metre\per\second\squared}
	\end{align*}
	Évidemment, la force exercée par le ballon sur la nacelle vaut \SI{1765,8}{\newton}.

	La force exercée par la corde sur la masse se calcule finalement par application de la seconde loi au système constitué uniquement de m~:
	\begin{align*}
	\sum F^{ext}&=T-m\cdot g=m\cdot a\;\Rightarrow\\
	T&=m\cdot a + m\cdot g=80\cdot (1,23+9,81)\\
	&=\SI{882,9}{\newton}
	\end{align*}
	\end{enumerate}
	
\end{Solution OS}
\begin{Solution OS}{8}
	On verra ci-dessous que l'utilisation de la seconde loi de Newton permet d'obtenir une relation entre l'accélération de chaque masse, constituant une équation à deux inconnues. Pour déterminer la valeur des deux accélérations, il est donc nécessaire de trouver une seconde équation entre ces deux équations.

	\medskip
	Imaginons une poulie suspendue à une corde qui dépasse de celle-ci des deux côtés d'une longueur L, comme présenté sur la figure \ref{cordepoulie}. La demi-circonférence de la poulie vaut aussi L.
	\begin{figure}[ht]
	\centering
	\caption[Corde poulie]{Corde et poulie}\label{cordepoulie}
	\medskip
	\def\svgwidth{2cm}
	\input{Annexe-Exercices/Images/cordepoulie.eps_tex}
	\end{figure}
	On tire sur la corde à gauche pour la faire monter d'une hauteur L.

	\smallskip
	La question est~: \emph{de quelle hauteur monte la poulie ?}

	\smallskip
	Pour le déterminer, il faut considérer qu'en tirant sur la corde pour la faire monter d'une hauteur L, on amène le point A qui est à l'origine au contact de la poulie	à la place du point B du haut de la corde (voir figure \ref{cordepoulie}). Si la poulie restait à sa place, on aurait la situation de la figure \ref{cordepoulietiree}). Mais, elle est en réalité libre de monter. Ce qui reste fixe est le point C de la figure \ref{cordepoulie}.
	\begin{figure}[ht]
	\centering
	\caption[Corde tirée poulie]{Corde tirée et poulie}\label{cordepoulietiree}
	\medskip
	\def\svgwidth{2cm}
	\input{Annexe-Exercices/Images/cordepoulietiree.eps_tex}
	\end{figure}
	On a donc a répartir une longueur 2L de corde entre le point A de la figure \ref{cordepoulietiree} et le point C de la figure \ref{cordepoulie}. Comme la demi-circonférence de la poulie vaut L, il reste une longueur L à répartir des deux côtés de la poulie, soit L/2 de chaque côté, comme le montre la figure \ref{cordepoulietireejuste}.
	\begin{figure}[ht]
	\centering
	\caption[Corde tirée poulie juste]{Corde tirée et poulie montée}\label{cordepoulietireejuste}
	\medskip
	\def\svgwidth{3.1cm}
	\input{Annexe-Exercices/Images/cordepoulietireejuste.eps_tex}
	\end{figure}
	Ainsi, quand on tire la corde d'une longueur L, la poulie monte d'une longueur L/2.

	\bigskip
	Pour revenir au système des deux poulies du problème, la remarque précédente se traduit par le fait que quand la masse m descend d'une longueur L, la masse M monte d'une longueur L/2.

	L'accélération étant une distance divisée par un temps au carré, on comprends facilement que cela signifie que l'accélération de la masse m vaut simplement le double de celle de la masse M, soit~:
	\[a_m=2\cdot a_M\]
	Cette équation constitue une première relation entre les deux inconnues que sont les accélérations de chaque masse.

	\bigskip
	Les masses étant égales, on pourrait aussi croire que le système est en équilibre. Ce n'est pas le cas. Pour le comprendre, considérons la poulie qui n'est pas accrochée au plafond.

	\smallskip
	À l'instar d'une poulie suspendue au plafond à laquelle on accroche deux masses identiques pendantes par l'intermédiaire d'une corde, la force totale qu'exerce sur elle le plafond vaut évidemment le poids total des deux masses, puisque la poulie ne bouge pas. Or, pour que chaque masse individuellement ne bouge pas, il faut que la corde qui passe dans la poulie exerce sur chacune d'elle une force égale à son poids. De chaque côté de la poulie, la corde exerce donc une même force et la poulie est tirée vers le bas par l'ensemble de ces deux forces.

	\smallskip
	Ainsi, sur la poulie qui n'est pas accrochée au plafond, la force totale exercée vers le haut vaut deux fois la tension dans la corde. Vers le bas, seule le poids de la masse M qui lui est suspendue est présent.

	\smallskip
	De l'autre côté, la masse m retenue par la corde qui passe sur la poulie suspendue au plafond est soumise à un poids identique vers le bas et à une seule force vers le haut exercée par la corde. Comme la masse de la corde est nulle, la tension dans la corde est la même partout.

	\smallskip
	Finalement, la masse M est tirée vers le haut par deux fois la tension dans la corde et la masse m est retenue par une fois la tension dans la corde. Clairement donc, la première monte et la seconde descend.

	\medskip
	Le choix du système d'axe est donc clair~: vers le haut pour la masse M qui monte et vers le bas pour m qui descend, le mouvement se faisant dans cette direction avec une accélération a identique pour les deux masses.

	\smallskip
	On peut alors écrire les équations du mouvement de chaque masse ainsi~:
	\begin{align*}
	m\cdot g-T&=m\cdot a_m\\
	2\cdot T-M\cdot g&=M\cdot a_M
	\end{align*}
	En multipliant par deux la première équation, on peut les additionner pour en tirer l'accélération.
	\begin{align*}
	2\cdot m\cdot g-2\cdot T&=2\cdot m\cdot a_m\\
	+\;\;\;\;\;\;\;\;\;\;2\cdot T-M\cdot g&=M\cdot a_M\\
	\hline\\
	2\cdot m\cdot g-M\cdot g&=2\cdot m\cdot a_m+M\cdot a_M
	\end{align*}
	Cela constitue la seconde relation entre les deux inconnues que sont les accélérations de chaque masse.

	\medskip
	Avec la première relation établie ci-dessus entre les accélérations des deux masses (\(a_m=2\cdot a_M\)), on a alors~:
	\begin{align*}
	2\cdot m\cdot g-M\cdot g&=2\cdot m\cdot a_m+M\cdot a_M\;\Rightarrow\\
	2\cdot m\cdot g-M\cdot g&=2\cdot m\cdot 2\cdot a_M+M\cdot a_M\;\Rightarrow\\
	2\cdot m\cdot g-M\cdot g&=4\cdot m\cdot a_M+M\cdot a_M\;\Rightarrow\\
	a_M&=\frac{2\cdot m-M}{4\cdot m+M}\cdot g
	\end{align*}
	Dans le cas où les deux masse sont identiques (M=m), on a alors~:
	\[a_M=\frac{2\cdot m-m}{4\cdot m+m}\cdot g=\frac{m}{5\cdot m}\cdot g=\frac{1}{5}\cdot g\]
	et pour l'accélération de m~:\[a_m=2\cdot a_M=\frac{2}{5}\cdot g\]
	
\end{Solution OS}
\begin{Solution OS}{9}
	Dans ce problème, on n'utilise pas le système d'axes de la figure \ref{incline} qui est parallèle et normal au plan incliné, mais un système d'axes horizontal et vertical. Les équations du mouvement selon ce système s'écrivent avec des notations évidentes~:
	\begin{align*}
	\sum \overrightarrow{F}^{ext}=\overrightarrow{R}+\overrightarrow{P}&=m\cdot \overrightarrow{a}\;\;\Rightarrow\\[0.8em]
	R_x&=m\cdot a_x\\
	R_y-P&=m\cdot a_y
	\end{align*}
	En considérant que l'angle \(\alpha\) du plan incliné se retrouve entre la réaction R et la verticale, on peut écrire~:
	\[R_x=R\cdot \sin(\alpha)\;\;\text{et}\;\;R_y=R\cdot \cos(\alpha)\]
	et finalement en tirer les accélérations cherchées~:
	\begin{align*}
	a_x&=\frac{R\cdot \sin(\alpha)}{m}\\
	a_y&=\frac{R\cdot \cos(\alpha)-m\cdot g}{m}
	\end{align*}
	Visiblement, on a deux équations pour trois inconnues, les deux accélérations et la réaction du plan.

	Pour le calcul de cette dernière, il est donc nécessaire de disposer de l'équation supplémentaire donnée par la contrainte imposée à la masse de rester sur le plan incliné, soit la relation spécifiant que la réaction R est égale à la composante perpendiculaire au plan incliné du poids~:
	\[R=P\cdot \cos(\alpha)=m\cdot g\cdot \cos(\alpha)\]
	Ainsi, en remplaçant dans les équations du mouvement, on a~:
	\begin{align*}
	a_x&=\frac{m\cdot g\cdot \cos(\alpha)\cdot \sin(\alpha)}{m}\\
	a_y&=\frac{m\cdot g\cdot \cos^2(\alpha)-m\cdot g}{m}\\[0.8em]
	a_x&=g\cdot\cos(\alpha)\sin(\alpha)\\
	a_y&=g\cdot (\cos^2(\alpha)-1)=-g\cdot\sin^2(\alpha)
	\end{align*}
	Pour vérifier que ces relations sont correctes, on peut calculer la norme du vecteur accélération qui devrait selon le problème \ref{planinclinesimple} valoir \(a=g\sin(\alpha)\). Le calcul est le suivant~:
	\begin{align*}
	a&=\sqrt{a_x^2+a_y^2}\\
	&=\sqrt{(g\cdot\cos(\alpha)\sin(\alpha))^2+(-g\cdot\sin^2(\alpha)^2}\\
	&=\sqrt{g^2\cdot\cos^2(\alpha)\sin^2(\alpha)+g^2\cdot\sin^4(\alpha)}\\
	&=\sqrt{g^2\cdot\sin^2(\alpha)\cdot (\cos^2(\alpha)+\sin^2(\alpha))}\\
	&=\sqrt{g^2\cdot\sin^2(\alpha)}\\
	&=g\cdot\sin(\alpha)
	\end{align*}
	Ce qu'il fallait démontrer.
	
\end{Solution OS}
\begin{Solution OS}{10}
	Rappelons tout d'abord que l'exercice \ref{massesuspendue} a permis de calculer l'accélération d'un système de deux masses, l'une sur un plan horizontal, la masse M', et l'autre pendant dans le vide, la masse m, accrochée à la première par une ficelle sans masse. Avec un système constitué des deux masses, on a pu montrer que~:
	\[m\cdot g=(M'+m)\cdot a\;\;\Rightarrow\;\;a=\frac{m}{M'+m}\cdot g\]

	C'est à partir de là que l'on peut résoudre le présent problème.

	\smallskip
	La solution est toute simple. À un instant donné on groupe toute la masse qui glisse sur le plan pour en faire la masse M et toute la masse pendante pour en faire la masse m. Pour cela, définissons une masse par unité de longueur de corde~:
	\[\rho=\frac{M}{L}\]
	Ainsi on peut écrire~:
	\begin{align*}
	M&=(L-y)\cdot \rho=(L-y)\cdot\frac{M}{L}\\
	m&=y\cdot \rho=y\cdot\frac{M}{L}
	\end{align*}
	Ainsi, on peut simplement écrire l'équation de l’accélération~:
	\begin{align*}
	a&=\frac{m}{M+m}\cdot g=\frac{y\cdot M/L}{(L-y)\cdot M/L+y\cdot M/L}\cdot g\\
	&=\frac{y}{(L-y)+y}\cdot g=\frac{y}{L}\cdot g
	\end{align*}
	
\end{Solution OS}
\begin{Solution OS}{11}
	On commence par calculer l'accélération à la surface de Sedna grâce à la loi de la gravitation universelle~:
	\begin{align*}
	g_{Sedna}&=G\cdot \frac{M}{R^2}=6,67\cdot 10^{-11}\cdot \frac{10^{21}}{(995\cdot 10^3/2)^2}\\
	&=\SI{0,27}{\metre\per\second\squared}
	\end{align*}
	On peut alors calculer les deux poids~:
	\begin{align*}
	P_{Terre}&=m\cdot g=13\cdot 9,81=\SI{127,5}{\newton}\\
	P_{Sedna}&=m\cdot g=13\cdot 0,27=\SI{3,5}{\newton}\\
	\end{align*}
	Le rapport est alors de 127,5/3,5=36,4.
	
\end{Solution OS}
\begin{Solution OS}{12}
	La seconde loi de Newton pour la trajectoire circulaire d'un corps m soumis à la gravitation autour d'une masse centrale M s'écrit~:
	\[G\cdot \frac{M\cdot m}{r^2}=m\cdot a=m\cdot \frac{v^2}{r}\]
	Sur une trajectoire circulaire la vitesse vaut~:
	\[v=\frac{2\cdot \pi\cdot r}{T}\]
	où T est la période de rotation de la masse m. Ainsi, on peut écrire~:
	\begin{align*}
	G\cdot \frac{M\cdot m}{r^2}&=m\cdot \frac{(2\cdot \pi r)^2}{T^2\cdot r}\\
	G\cdot \frac{M}{r^2}&=\frac{4\cdot \pi^2\cdot r}{T^2}\;\Rightarrow\\
	M&=\frac{4\cdot \pi^2\cdot r^3}{G\cdot T^2}
	\end{align*}
	Avec les données suivantes~:
	\begin{align*}
	r&=5,91\cdot 11,19\cdot 6,371\cdot 10^6=\SI{421,33e6}{\metre}\\
	T&=1,769\cdot 24\cdot 3'600=\SI{1,53e5}{\second}
	\end{align*}
	on peut alors calculer la masse~:
	\begin{align*}
	M&=\frac{4\cdot \pi^2\cdot r^3}{G\cdot T^2}\\
	&=\frac{4\cdot \pi^2\cdot (421,33\cdot 10^6)^3}{6,67\cdot 10^{-11}\cdot (1,53\cdot 10^5)^2}\\
	&=\SI{1,89e27}{\kilo\gram}
	\end{align*}
	
\end{Solution OS}
\begin{Solution OS}{13}
	La situation est celle de la figure \ref{inclinefrottement}.
	\begin{figure}[ht]
	\centering
	\caption[Frottements]{Frottements sur plan incliné}\label{inclinefrottement}
	\medskip
	\def\svgwidth{5cm}
	\input{Annexe-Exercices/Images/inclinefrottement.eps_tex}
	\end{figure}
	Dans le système d'axe de cette figure, les équations du mouvement sont~:
	\begin{align*}
	P_x-F_f=m\cdot a\\
	R-P_y=0
	\end{align*}
	Si le bloc est statique, l'accélération est nulle, la force de frottement vaut~:
	\[F_f=\mu_0\cdot R\]
	et les composantes du poids sont~:
	\[P_x=P\cdot \sin(\alpha)\;\;\textbf{et}\;\;P_y=P\cdot \cos(\alpha)\]
	Ainsi, les équations du mouvement deviennent~:
	\begin{align*}
	P\cdot \sin(\alpha)-\mu_0\cdot R=0\\
	R-P\cdot \cos(\alpha)=0
	\end{align*}
	En remplaçant la réaction R de la seconde équation dans la première, on obtient~:
	\begin{align*}
	P\cdot \sin(\alpha)&-\mu_0\cdot P\cdot \cos(\alpha)=0\;\;\Rightarrow\\
	P\cdot \sin(\alpha)&=\mu_0\cdot P\cdot \cos(\alpha)\;\;\Rightarrow\\
	\mu_0=\frac{\sin(\alpha)}{\cos(\alpha)}&=\tan(\alpha)=\tan(32)=\SI{0,62}{}
	\end{align*}
	\smallskip
	Si le bloc glisse, c'est le c\oe fficient cinétique qui intervient dans la force de frottement. Celui-ci vaut~:
	\[\mu=(1-0,05)\cdot \mu_0=0,95\cdot 0,62=\SI{0,59}{}\]
	Alors, sur l'axe x, avec la composante du poids parallèle au plan incliné et la force de frottement proportionnelle à la réaction R égale à la composante du poids perpendiculaire au plan, on peut écrire~:
	\begin{align*}
	m&\cdot g\cdot \sin(\alpha)-\mu\cdot m\cdot g\cdot \cos(\alpha)=m\cdot a\;\;\Rightarrow\\
	a&=g\cdot (\sin(\alpha)-\mu\cdot \cos(\alpha))\\
	&=9,81\cdot (\sin(32)-0,59\cdot \cos(32))=\SI{0,29}{\metre\per\second\squared}
	\end{align*}
	
\end{Solution OS}
\begin{Solution OS}{14}
	Tant que les deux blocs ne glissent pas, la force de frottement est statique. Si on considère comme système le bloc supérieur, celui-ci étant immobile sur le bloc inférieur, la forces F et celle de frottement s'annulent. Elles sont donc égales et si la force F exercée sur le bloc supérieur augmente, la force de frottement statique augmente également dans une exacte mesure. Cela jusqu'à la valeur de frottement statique maximum exercée par m sur M, donnée par l'équation~:
	\begin{align*}
	F&=F_{fr\:stat\:max}=\mu\cdot N=\mu\cdot m\cdot g\\
	&=0,6\cdot 2\cdot 9,81=\SI{11,77}{\newton}
	\end{align*}
	On est alors à l'imminence de glissement.

	Pour calculer l'accélération des deux masses, qui n'en forment alors plus qu'une, il faut choisir le système qui les constitue et écrire~:
	\begin{align*}
	F&=(M+m)\cdot a\;\Rightarrow\;a=\frac{F}{M+m}\\
	&=\frac{11,77}{2+5}=\SI{1,68}{\metre\per\second\squared}
	\end{align*}

	\medskip
	À partir du moment où le glissement commence, la force de frottement statique maximum devient une force de frottement cinétique. Si les c\oe fficients de frottements statique et cinétique sont égaux, la force de frottement cinétique est égale à celle statique maximale et ne varie plus. Ainsi, sur le bloc du bas ne s'exerce qu'une seule force extérieure, c'est la réaction à la force de frottement cinétique exercée par le bloc du bas sur celui du haut. Sa valeur est donnée par le calcul ci-dessus. Ainsi, on peut écrire pour le système du bas~:
	\begin{align*}
	F_{fr\:stat\:max}&=M\cdot a_M\;\Rightarrow\\
	a_M&=\frac{11,77}{5}=\SI{2,35}{\metre\per\second\squared}
	\end{align*}
	Quant au bloc du haut, deux forces extérieures s'exercent sur lui~: la force F et la force de frottement dynamique, égale à la force de frottement statique maximale. Pour ce système, on peut écrire~:
	\begin{align*}
	F-F_{fr\:stat\:max}&=m\cdot a_m\;\Rightarrow\\
	a_m&=\frac{23,54-11,77}{2}=\SI{5,89}{\metre\per\second\squared}
	\end{align*}
	
\end{Solution OS}
\begin{Solution OS}{15}
	Les deux forces de frottement statique et cinétique valent~:
	\begin{align*}
	F_{fr\:stat\:max}&=\mu_{stat}\cdot m\cdot g\\
	&=0,6\cdot 20'000\cdot 9,81=\SI{117720}{\newton}\\
	F_{cin}&=\mu_{cin}\cdot m\cdot g\\
	&=0,4\cdot 20'000\cdot 9,81=\SI{78480}{\newton}
	\end{align*}
	Les deux décélérations sont alors~:
	\begin{align*}
	-F_{fr\:stat\:max}&=m\cdot a_{stat}\;\Rightarrow\\
	a_{stat}&=\frac{-117'720}{20'000}=\SI{-5,886}{\metre\per\second\squared}\\
	-F_{cin}&=m\cdot a_{cin}\;\Rightarrow\\
	a_{cin}&=\frac{-78'480}{20'000}=\SI{-3,924}{\metre\per\second\squared}
	\end{align*}
	On peut alors calculer la distance de freinage dans chaque cas~:
	\begin{align*}
	v^2&=v_0^2+2\cdot a\cdot d\;\Rightarrow\;d=\frac{v^2-v_0^2}{2\cdot a}\\
	d_{stat}&=\frac{0-13,89^2}{2\cdot (-5,886)}=\SI{16,39}{\metre}\\
	d_{cin}&=\frac{0-13,89^2}{2\cdot (-3,924)}=\SI{24,58}{\metre}
	\end{align*}
	La différence est donc de \(24,58-16,39=\SI{8,19}{\metre}\).
	
\end{Solution OS}
\begin{Solution OS}{16}
	Le poids du bloc de masse m vaut~:
	\[P_m=m\cdot g=3\cdot 9,81=\SI{29,43}{\newton}\]
	Pour que ce bloc ne tombe pas, il faut qu'une force verticale s'exerce vers le haut et compense le poids calculé ci-dessus, soit \(F_{fr}=P\). L'origine de cette force est évidemment le frottement entre les deux blocs. Or, celle-ci dépend de la force exercée par la masse M sur m, notée \(\overrightarrow{N}\), qui joue le rôle de la réaction R du sol pour un objet glissant horizontalement. La figure \ref{blocsuspenduforces} présente la situation qui permet d'exprimer la force de frottement et de déterminer la valeur N de la réaction.
	\begin{figure}[ht]
	\centering
	\caption[Forces sur bloc suspendu]{Forces sur le bloc suspendu}\label{blocsuspenduforces}
	\medskip
	\def\svgwidth{4cm}
	\input{Annexe-Exercices/Images/blocsuspenduforces.eps_tex}
	\end{figure}
	\begin{align*}
	F_{fr}&=\mu_{stat}\cdot N\;\Rightarrow\\
	N&=\frac{F_{fr}}{\mu_{stat}}=\frac{29,43}{0,5}=\SI{58,86}{\newton}
	\end{align*}
	On peut dire en français que pour avoir une force de frottement donnée il faut une réaction normale N de valeur double, en raison du c\oe fficient de frottement particulier ici.

	\medskip
	C'est la force normale \(\overrightarrow{N}\) qui est la seule force qui pousse la masse m horizontalement. Elle est donc responsable de l'accélération qu'on peut ainsi calculer simplement à l'aide de la seconde loi~:
	\[a=\frac{N}{m}=\frac{58,56}{3}=\SI{19,62}{\metre\per\second\squared}\]
	Cette accélération est celle de la masse m, mais aussi de M. La valeur F de la force permettant de l'obtenir pour le système constitué par les deux masses est donc finalement~:
	\[F=(M+m)\cdot a=7\cdot 19,62=\SI{137,34}{\newton}\]
	
\end{Solution OS}
\begin{Solution OS}{17}
	Un autre corrigé de test.
	
\end{Solution OS}
\begin{Solution OS}{18}
	Un autre corrigé de test.
	
\end{Solution OS}
\begin{Solution OS}{19}
	Un autre corrigé de test.
	
\end{Solution OS}
\begin{Solution OS}{20}
	Le travail de l'équipe pour remonter le bobsleigh du bas de la piste à son point de départ sert à augmenter l'énergie potentielle du bob. On néglige les frottement exercés sur le véhicule de transport. Ainsi, on a~:
	\begin{align*}
	E_{pot}&=m\cdot g\cdot h=250\cdot 9,81\cdot (1000-800)\\
	&=\SI{490500}{\joule}
	\end{align*}
	pour une seule montée. Pour 5 montées, on a donc \SI{2452500}{\joule}.

	\medskip
	C'est évidemment la même énergie qui est perdue pendant la descente (à l'exception de la poussée initiale). Elle vient du travail du poids sur le bob et les quatre personnes qui se trouvent à l'intérieur.

	\medskip
	L'énergie potentielle perdue par le bob sert à augmenter sa vitesse et à lutter contre le frottement. Le travail nécessaire à parvenir à une vitesse de \SI{80}{\kilo\metre\per\hour} est égal à l'énergie cinétique acquise. Pour un bob de \SI{600}{\kilo\gram} et une vitesse moyenne de 80/3,6=\SI{22,2}{\metre\per\second}, on a un travail de~:
	\[A=\frac{1}{2}\cdot m\cdot v^2=\frac{1}{2}\cdot 600\cdot 22,2^2=\SI{148148}{\joule}\]
	par descente. Soit au total, pour 5 descentes \SI{740740}{\joule}.

	Cette valeur est supérieure à l'énergie fournie au bob pendant la montée. C'est normal, car on a pas tenu compte alors de l'énergie nécessaire à monter les personnes. Pour cela, il faut calculer l'énergie potentielle acquise par la masse de \SI{600}{\kilo\gram}~:
	\[E_{pot}=600\cdot 9,81\cdot 200=\SI{1177200}{\joule}\]
	qui est bien supérieure à celle fournie pour augmenter la vitesse.

	\medskip
	La différence d'énergie potentielle et cinétique, soit 1'117'200-740'740 = \SI{436460}{\joule}, est perdue dans le frottement.

	Comme la distance sur laquelle il s'exerce est de \SI{1800}{\metre}, on peut calculer la force de frottement moyenne par~:
	\begin{align*}
	A_{fr}&=F_fr\cdot d\;\Rightarrow\\
	F_{fr}&=\frac{A_{fr}}{d}=\frac{436'460}{1'800}=\SI{242,5}{\newton}
	\end{align*}
	C'est évidemment une estimation.
	
\end{Solution OS}
