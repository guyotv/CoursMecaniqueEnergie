set terminal latex rotate
set output 'CoursMecaniqueOSDF-gnuplottex-fig2.tex'
# domaine de définition
        set xrange [36:48]
        set yrange [0:140]
        # flèches
        set arrow 1 from 39.7,120 to 39.7,0 head filled
        #size screen 0.5,30
        set label "Écart type" at 39.4,100 rotate by 90
        set arrow 2 from 44.1,120 to 44.1,0 head filled
        set label "Écart type" at 44.4,138 rotate by -90
        set arrow 3 from 41.9,120 to 41.9,0 head filled
        set label "Moyenne" at 42.2,80 rotate by -90
        #set grid
        #set title "Baguettes d'une année"
        # suppression de la légende
        set key off
        # légendes des axes
        set xlabel "Longueur des baguettes (en cm)"
        set ylabel "Nombre de baguettes" rotate by 90
        # largeur des colonnes (boxes)
        #set boxwidth 0.1

        # tracé du graphe
        plot "Annexe-Incertitudes/Images/baguettesgauss.dat" with boxes
