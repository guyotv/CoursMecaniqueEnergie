\chapter{Impulsion et quantité de mouvement\index{impulsion}}
%\minitoc

\section{Introduction}

\lettrine{I}{l existe} une classe de problèmes qui font intervenir des forces variant très rapidement et d'une grande intensité. Il s'agit des problèmes de chocs. Il existe une grandeur, la \textit{quantité de mouvement}\index{quantité de mouvement}, dont l'évolution permet d'étudier simplement ces problèmes. De plus, elle nous permet d'obtenir des renseignements sur la dynamique du choc et notamment sur la force moyenne qui agit pendant celui-ci (voir paragraphe \ref{apprmolecul}).

\section{Quantité de mouvement}
Le problème est le suivant~: deux billes de masse différentes $m_1$ et $m_2$ entrent en collision frontale avec des vitesse respectives $v_1$ et $v_2$. On peut donc écrire, à l'aide de la troisième loi de Newton~:
\begin{equation}\label{3eloichoc}
F_{1\rightarrow 2}=F_{2\rightarrow 1}
\end{equation}
où les forces $F_{1\rightarrow 2}$ et $F_{2\rightarrow 1}$ sont celles qui s'exercent au moment du choc.
\medskip

D'autre part, si on prend pour système celui composé de chaque bille individuellement, On peut écrire pour $m_2$, au moment du choc~:
\begin{equation}\label{2sur1}
\sum F^{ext}=m_2\cdot a_2\;\;soit\;\;F_{1\rightarrow 2}=m_2\cdot a_2
\end{equation}
et pour $m_1$~:
\begin{equation}\label{1sur2}
\sum F^{ext}=m_1\cdot a_1\;\;soit\;\;F_{2\rightarrow 1}=m_1\cdot a_1
\end{equation}
En combinant les équations \ref{3eloichoc}, \ref{2sur1} et \ref{1sur2}, on a alors~:
\begin{equation}\label{achoc}
m_2\cdot a_2=m_1\cdot a_1
\end{equation}
Or, pour ne pas devoir parler de dérivée\footnote{On verra au paragraphe \ref{cinemadiff}, que l'accélération n'est autre que la dérivée de la vitesse~: $a=\frac{dv}{dt}$}, on peut comprendre l'équation \ref{achoc} comme comprenant des accélérations moyennes et écrire~:
\begin{align}\label{varqtitedemvt}
m_2\cdot \frac{\Delta v_2}{\Delta t}=m_1\cdot \frac{\Delta v_1}{\Delta t}\;\;&\Rightarrow\nonumber \\
m_2\cdot \Delta v_2-m_1\cdot \Delta v_1&=0\nonumber \\
p_2-p_1=\Delta p=0
\end{align}
en définissant la grandeur $p$, appelée \textit{quantité de mouvement}\index{quantité de mouvement} par~:
\begin{equation}\label{qtitedemvt}
p:=m\cdot v
\end{equation}
L'équation \ref{varqtitedemvt} signifie que la variation de la quantité de mouvement est nulle pendant le choc. La quantité de mouvement est donc une constante ou en d'autres termes elle est conservée pendant les chocs~:
\begin{equation}
p=constante
\end{equation}

La conservation de la quantité de mouvement est une loi de conservation très générale. Elle ne s'applique pas seulement aux chocs.

\subsection{Masse d'inertie}
Il faut remarquer le rôle joué par la quantité de mouvement dans la définition de la masse d'inertie. ... voir matière Balibar

\section{Énergie cinétique}
Si on lâche une boule de ``pâte à modeler'' à une certaine hauteur du sol, on constate qu'en le percutant elle se colle à lui. Une boule de ``pâte à modeler'' ne rebondit pas. On qualifie le choc avec le sol de ``\emph{parfaitement mou}''. Si le sol ne bouge pas, c'est-à-dire ne recule pas, toute l'énergie cinétique juste avant le choc, qui vient de l'énergie potentielle initiale, est aborbée par la déformation de la ``pâte à modeler''. Par contre, si l'objet heurté par la ``pâte à modeler'' acquiert avec elle une certaine vitesse, une partie de l'énergie cinétique initiale se retrouve dans l'énergie cinétique de l'ensemble après le choc et une autre partie se retrouve dans la déformation de la ``pâte à modeler''.

Par contre, une balle superélastique va rebondir, voir même remonter jusqu'à la même hauteur qu'au départ. Si c'est le cas, on qualifira le choc de ``\emph{parfaitement élastique}''. Si non, il sera dit ``\emph{partiellement élastique}''. Deux corps se heurtant de manière parfaitement élastique ont donc une énergie cinétique totale qui est conservée au contraire du cas d'un choc partiellement élastique entre les deux corps.

\section{Choc parfaitement élastique}
Dans le cas d'un choc parfaitement élastique unidimentionnel entre deux particules la quantité de mouvement et l'énergie cinétique sont conservées. Les équations se présentent donc de la manière suivante~:
\begin{equation}
m_1\cdot v_1+m_2\cdot v_2=m_1\cdot v_1'+m_2\cdot v_2' \label{qtitemvt1dim}
\end{equation}
\begin{multline}
\frac{1}{2}\cdot m_1\cdot v_1^2+\frac{1}{2}\cdot m_2\cdot v_2^2=\\
\frac{1}{2}\cdot m_1\cdot v_1'^2+\frac{1}{2}\cdot m_2\cdot v_2'^2 \label{encin1dim}
\end{multline}

où les exposants $'$ signifient ``après le choc''. L'équation \ref{encin1dim} présente un degré de complexité plus important que l'équation \ref{qtitemvt1dim} en raison des carrés des vitesses. On peut résoudre ce système d'équations pour trouver les vitesses après le choc en passant par la résolution d'un équation du second degré. Mais, on peut aussi trouver de manière générale un équation plus simple que celle de la conservation de l'énergie cinétique. Pour cela, réécrivons les équations \ref{qtitemvt1dim} et \ref{encin1dim} avec les masses $m_1$ et $m_2$ de chaque côté des égalités~:
\begin{align}
m_1\cdot (v_1-v_1')&=m_2\cdot (v_2'-v_2)\label{qtitemvtsuppr}\\
m_1\cdot (v_1^2-v_1'^2)&=m_2\cdot (v_2'^2-v_2^2)\label{encincarre}
\end{align}

Puis, a l'aide de l'identité remarquable~:
\[a^2-b^2=(a-b)\cdot (a+b)\]

réécrivons l'équation \ref{encincarre} sous la forme~:
\begin{multline}
m_1\cdot (v_1-v_1')\cdot (v_1+v_1')=\\
m_2\cdot (v_2'-v_2)\cdot (v_2'+v_2)\label{encinremarquable}
\end{multline}

L'équation \ref{qtitemvtsuppr} permet alors de supprimer la partie gauche de chaque membre de l'équation \ref{encinremarquable} pour obtenir~:
\[v_1+v_1'=v_2'+v_2\]

Soit en réarrangeant les termes~:
\begin{equation}\label{qtitemvtvitrelative}
v_1-v_2=-(v_1'-v_2')
\end{equation}

Cette équation signifie que lors d'un choc frontal, unidimentionnel et élastique de deux particules, les vitesses changent de signe et leur grandeurs sont échangées.
\medskip

Ainsi, plutôt que d'utiliser les équations \ref{qtitemvt1dim} et \ref{encin1dim}, dont la seconde présente des vitesses quadratiques, on peut utiliser à la place les équations \ref{qtitemvt1dim} et \ref{qtitemvtvitrelative}. Ce dernier système d'équations étant plus simple que le premier.

\subsection{Exemple}
Considérons un exemple simple. Tout se déroule sur un seul axe. Deux particules de même masse se percutent frontalement, l'une ayant une vitesse de $7\,m/s$ et l'autre une vitesse double de la première. Quelles sont les vitesses des particules après le choc ?
\medskip

Posons $v_1=7\,m/s$ pour la vitesse de la particule de masse $m_1$ et $v_2=-14\,m/s$ pour la vitesse de la particule de masse $m_2$. Le signe négatif de la masse $m_2$ vient du choix de l'axe qui est orienté dans le sens de $v_1$. On a aussi que $m_1=m_2=m$. Ainsi, on peut écrire la conservation de la quantité de mouvement~:
\begin{align}
m_1\cdot v_1+m_2\cdot v_2&=m_1\cdot v_1'+m_2\cdot v_2'\nonumber\\
m\cdot 7+m\cdot (-14)&=m\cdot v_1'+m\cdot v_2'\nonumber\\
7-14&=v_1'+v_2'\nonumber\\
v_1'+v_2'&=-7\label{chocsimplemvt}
\end{align}

On peut aussi écrire l'équation de la conservation de l'énergie cinétique~:
\begin{align}
\frac{1}{2}\cdot m\cdot 7^2+\frac{1}{2}\cdot m\cdot (-14)^2&=\nonumber\\
\frac{1}{2}\cdot m\cdot v_1'^2&+&\frac{1}{2}\cdot m\cdot v_2'^2\nonumber\\
49+196&=v_1'^2+v_2'^2\nonumber\\
v_1'^2+v_2'^2&=245\label{chocsimpleen}
\end{align}

On tire alors de l'équation \ref{chocsimplemvt}~:
\begin{equation}\label{solv1'}
v_1'=-7-v_2'
\end{equation}

qu'on remplace dans l'équation \ref{chocsimpleen}~:
\begin{align*}
(-7-v_2')^2+v_2'^2&=245\\
49+2\cdot 7\cdot v_2'+v_2'^2+v_2'^2&=245\\
2\cdot v_2'^2+14\cdot v_2'-196&=0\\
v_2'^2+7\cdot v_2'-98&=0
\end{align*}

Soit une équation du second degré dont la solution est~:
\begin{align*}
v_2'&=\frac{-7\pm \sqrt{7^2-4\cdot (-98)}}{2}\\
&=\frac{-7\pm 21}{2}=\begin{cases}7\,m/s\\-14\,m/s\end{cases}
\end{align*}

Évidemment, la seconde solution $v_2'=-14\,m/s$ n'est pas possible puisque après le choc la particule poursuivrait son mouvement à la même vitesse qu'avant. Donc~:
\[v_2'=7\,m/s\]

et, selon l'équation \ref{solv1'}, on a~:
\[v_1'=-7-7=-14\,m/s\]

On constate donc que pour les grandeurs il y a échange des vitesses et que les signes des vitesses après le choc sont opposé à ceux des vitesses avant.
\medskip

Cet exemple est relativement compliqué car on a utilisé l'équation de la conservation de l'énergie cinétique qui mène à des vitesses quadratiques.

On peut cependant trouver la solution plus simplement en utilisant l'équation \ref{qtitemvtvitrelative} démontrée précédemment. On a alors~:
\begin{align*}
v_1-v_2&=-(v_1'-v_2')\;\Rightarrow\\
7-(-14)&=-(v_1'-v_2')\\
v_1'-v_2'&=-21
\end{align*}

Avec l'équation \ref{solv1'} obtenue de la conservation de la quantité de mouvement, on a alors~:
\begin{align*}
v_1'-v_2'&=-21\;\Rightarrow\\
(-7-v_2')-v_2'&=-21\\
-7-2\cdot v_2'&=-21\\
2\cdot v_2'&=14\\
v_2'&=7\,m/s
\end{align*}

Et, comme précédemment, $v_1'=-14\,m/s$. On retrouve donc bien plus simplement la solution en se passant de l'équation de conservation de l'énergie cinétique. Évidemment, celle-ci a déjà été utilisée pour obtenir l'équation \ref{qtitemvtvitrelative}.

\section{Choc parfaitement mou}
Pour un choc parfaitement mou, la vitesse finale des deux masses est la même~: elles restent ``collées'' ensemble. Dans ce cas, l'énergie cinétique initiale des deux corps se retrouve après le choc pour une partie dans l'énergie cinétique des deux corps attachée et pour l'autre dans le travail de déformation des corps.

\subsection{Exemple}
Comme exemple de choc parfaitement mou, considérons encore deux particules de même masse qui se percutent frontalement, l'une ayant une vitesse $v_1=7\,m/s$ et l'autre une vitesse double de la première. Mais cette fois-ci les deux particules restent collées ensemble après le choc. Quelle est la fraction d'énergie cinétique ``perdue'' dans la collision ?
\medskip

La conservation de la quantité de mouvement nous permet d'écrire~:
\begin{align*}
m\cdot v_1+m\cdot v_2&=(m+m)\cdot v'\\
v_1+v_2&=2\cdot v'\\
v'&=\frac{7-14}{2}=-3,5\,m/s
\end{align*}

Évidemment pour un tel choc mou, il n'y a pas conservation de l'énergie cinétique. On a, avec le résultat précédent~:
\begin{align*}
E_{cin\,avant}&=\frac{1}{2}\cdot m\cdot v_1^2+\frac{1}{2}\cdot m\cdot v_2^2\\
&=\frac{1}{2}\cdot m\cdot 7^2+\frac{1}{2}\cdot m\cdot (-14)^2\\
&=24,5\cdot m+98\cdot m=122,5\cdot m\\
E_{cin\,apr\grave es}&=\frac{1}{2}\cdot 2\cdot m\cdot (-3,5)^2\\
&=12,25\cdot m
\end{align*}

Ainsi, l'énergie cinétique perdue est~:
\[\Delta E_{cin}=122,5\cdot m-12,25\cdot m=110,25\cdot m\]

et la fraction de l'énergie cinétique initiale perdue dans le choc est~:
\[p=\frac{110,25\cdot m}{122,5\cdot m}=0,90=90\%\]

\section{choc bidimentionnel}
L'équation de conservation de la quantité de mouvement qui dérive de la seconde loi de Newton est en réalité vectorielle. Tout le développement qui mène à elle en partant de la seconde loi de Newton utilise des grandeurs vectorielles comme l'accélération et la vitesse. Ainsi, la conservation de la quantité de mouvement s'écrit~:
\begin{equation}
\fbox{$\overrightarrow p=constante$}
\end{equation}

\subsection{Exemple}
Considérons le cas d'un choc mou de deux particules 




\section{Impulsion}\label{impuls}
Nous considérerons par la suite l'action d'une force $\overrightarrow F$ sur une distance donnée $\overrightarrow{dr}$. La grandeur associée à cette action sera le travail A~:
\[A=\int{\overrightarrow F\cdot \overrightarrow{dr}}\]

De la même manière, on peut définir l'action d'une force $\overrightarrow F$ pendant un temps donné $dt$. La grandeur associée à cette action est l'impulsion $\overrightarrow I$~:
\[\overrightarrow I=\int{\overrightarrow F\cdot dt}\]

Il s'agit d'une grandeur vectorielle.

\section{Impulsion et quantité de mouvement}\label{qtitemvtimpuls}