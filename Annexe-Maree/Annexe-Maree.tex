\myclearpage

\chapter{Marées\index{maree@marée}}\label{chapmarees}
\section{Introduction}
\lettrine{L}{'origine des marées} est un phénomène à la fois simple et complexe. Simple, car il suit d'assez près le mouvement de la Lune pour qu'on puisse naturellement le lui attribuer. Complexe, car il se produit aussi sur la Terre à l'opposé du zénith\index{zenith@zénith} de la Lune et il est souvent en retard sur son passage.

Nous allons ici rappeler la simplicité du phénomène et expliquer pourquoi il se produit à la fois du côté de la Terre où se trouve la Lune et à l'opposé. L'explication des différents retards des marées sur le passage de la Lune ne sera pas abordée ici, car elle est très complexe. Une description du phénomène ayant été faite précédemment (voir paragraphe \ref{paramarees}), nous nous intéresserons ici plus particulièrement à l'aspect mathématique du phénomène, tout en expliquant le plus simplement les choses.

\begin{figure}[t]
\centering
\caption[Marée]{Marée\label{maree}\endnote{Voir le site de l'encyclopédie Wikipedia~: \url=http://commons.wikimedia.org/wiki/Image:Bay_of_Fundy.jpg= notamment pour le copyright de l'image. Remerciements à son auteur Samuel Wantman.}}
\includegraphics[width=6cm]{Maree.eps}
\end{figure}

\medskip

L'analyse se fait en trois étapes. Tout d'abord, la détermination du centre de gravité\index{centre de gravité} du système Terre-Lune. Puis, le calcul de la force d'inertie\index{force@force!d'inertie} sur un élément de masse d'eau que l'application de la seconde loi de Newton modifiée pour des référentiels en rotation\index{referentiel@référentiel!tournant} (voir annexe \ref{relativite}) nécessite ou, d'un autre point de vue, le calcul de la pseudo-force centrifuge\index{pseudo-force!centrifuge} exercée sur cet élément et enfin le calcul de son poids relatif\index{poids!relatif}. L'analyse des termes constituant le poids relatif permettra alors de préciser l'influence de la Lune et d'expliquer la présence de deux marées par jour.

\section{Centre de gravité\index{centre de gravité}}
Une partie du problème des marées se trouve dans le fait que le système Terre-Lune a un centre de gravité qui ne coïncide pas avec le centre de la Terre, comme le montre la figure \ref{systterrelune}.

La notion de centre de gravité est ici relativement aisée à saisir. Imaginons deux masses \(m\) identiques situées à une distance \(d\) l'une de l'autre. On comprend bien que le centre de gravité de ce système se trouve exactement au milieu entre les masses. Maintenant, si l'une des masses double (\(M=2\cdot m\)), le centre de gravité va se rapprocher d'elle. La masse totale du système valant \(3\cdot m\), on comprend bien que la plus grande des masses compte pour deux tiers et la plus petite pour un tiers. Le centre de gravité se trouve donc à un tiers de la distance entre les corps, du côté de la plus grande masse. Depuis le centre de la plus grande masse, la distance \(r\) au centre de gravité se calcule donc ainsi~:
\begin{equation}\label{distgrav}
r=\frac{m}{M+m}\cdot d
\end{equation}
(depuis le centre de la plus petite masse, la distance au centre de gravité se calcule de la même manière en remplaçant le \(m\) du numérateur de la fraction par \(M\)).

\section{Force d'inertie}

\begin{figure}[t]
\centering
\caption{Système Terre-Lune\label{systterrelune}}
\input{Annexe-Maree/Images/Systterrelune.pst}
\end{figure}

En substance, le problème des marées est posé par la rotation d'une masse \(m\) d'eau non pas autour du centre de la Terre, mais autour du centre de gravité du système Terre-Lune. Il s'agit d'un mouvent circulaire uniforme\index{mouvement!circulaire uniforme} et l'une des composantes de la seconde loi de Newton est l'accélération centripète\index{acceleration@accélération!centripète} \(a_c\) de cette masse. On peut l'exprimer sous différentes formes présentées dans l'équation \ref{acccentr}~:
\begin{equation}\label{acccentr}
a_c=\frac{v^2}{R}=\omega^2\cdot R
\end{equation}
où \(v\) est la vitesse linéaire\index{vitesse!linéaire} de la masse \(m\) d'eau, \(R\) la distance du centre de gravité à la masse \(m\) et \(\omega\) sa vitesse angulaire\index{vitesse!angulaire}. En effet, l'équation \ref{vomegar}, page \pageref{vomegar} montre que la vitesse linéaire d'un corps tournant en mouvement circulaire uniforme vaut \(v=\omega\cdot R\) avec pour unité \([\omega]=\si{\radian\per\second}\).

\subsection{Vitesse angulaire\index{vitesse!angulaire}}

Or, la vitesse linéaire de la masse d'eau n'est pas celle qui résulte de sa rotation diurne autour du centre de la Terre, mais celle correspondant au déplacement de \(m\) sous l'influence de la Lune, c'est-à-dire celle correspondant à sa rotation autour du centre de gravité du système Terre-Lune. Avec les grandeurs présentées à la figure \ref{systterrelune} et l'équation \ref{distgrav}, le calcul de la vitesse angulaire \(\omega\) de ce dernier est le suivant~:
\begin{align}
F&=M_L\cdot \omega^2\cdot R\nonumber\\
G\cdot \frac{M_T\cdot M_L}{d_{T-L}^2}&=M_L\cdot \omega^2\cdot (d_{T-L}-r)\nonumber\\
G\cdot \frac{M_T\cdot M_L}{d_{T-L}^2}&=M_L\cdot \omega^2\cdot (d_{T-L}-\frac{M_L\cdot d_{T-L}}{M_L+M_T})\nonumber\\
G\cdot \frac{M_T}{d_{T-L}^2}&=\omega^2\cdot d_{T-L}\cdot (1-\frac{M_L}{M_L+M_T})\nonumber\\
G\cdot \frac{M_T}{d_{T-L}^3}&=\omega^2\cdot \frac{M_L+M_T-M_L}{M_L+M_T}\nonumber\\
G\cdot \frac{M_T}{d_{T-L}^3}&=\omega^2\cdot \frac{M_T}{M_L+M_T}\nonumber\\
\omega^2&=G\cdot \frac{M_L+M_T}{d_{T-L}^3}\label{omega}
\end{align}

\subsection{Force d'inertie\index{force@force!d'inertie}}

\begin{figure}[t]
\centering
\caption{Système Terre-Lune-eau\label{systterreluneeau}}
\input{Annexe-Maree/Images/Systterreluneeau.pst}
\end{figure}

On peut alors calculer la force d'inertie à ajouter à la seconde loi de Newton dans le cas de référentiels en rotation, puisque le référentiel lié au centre de la Terre est en rotation autour du centre de masse du système Terre-Lune. En considérant la distance du centre de gravité au centre de la Terre~:
\[r=\frac{M_L}{M_T+M_L}\cdot d_{T-L}\]
et l'équation \ref{omega}, on peut écrire cette force sous la forme~:
\begin{align}
F_{in}&=m\cdot \omega^2\cdot r\nonumber\\
&=m\cdot G\frac{M_L+M_T}{d_{T-L}^3}\cdot \frac{M_L}{M_L+M_T}\cdot d_{T-L}\nonumber\\
&=m\cdot G\frac{M_L}{d_{T-L}^2}\label{forceinertie}
\end{align}

\section{Poids relatif\index{poids!relatif}}
On peut maintenant calculer le poids relatif à l'aide de la seconde loi de Newton. La masse d'eau \(m\) est en équilibre statique dans le référentiel lié au centre de la Terre et son accélération est nulle. On peut s'imaginer cette masse comme posée sur une balance. Les forces qui s'exercent sur elle sont son poids \(P\), correspondant à l'attraction de la Terre, l'attraction \(F_L\) de la Lune, la force d'inertie\index{force@force!d'inertie} \(F_{in}\) et la réaction \(R\) de la balance\index{balance} qui constitue le poids apparent de la masse d'eau. Si on choisit un axe dirigé vers le centre de la Terre, on peut ainsi écrire~:
\begin{equation}\label{Newacc}
\sum F=P-R-F_L+F_{in}=0(=m\cdot a_m)
\end{equation}
Soit, en utilisant la loi de la gravitation universelle et l'équation \ref{forceinertie} dans l'équation \ref{Newacc}~:
\begin{align}
P-R-G\frac{M_L\cdot m}{(d_{T-L}-R_T)^2}+G\frac{M_L}{d_{T-L}^2}&=0\nonumber\\
P-R-\nonumber\\
G\frac{M_L\cdot m}{d_{T-L}^2}\cdot (1+\frac{2\cdot R_T}{d_{T-L}})+G\frac{M_L}{d_{T-L}^2}&=0\label{DLmaree}\\
P-R-G\frac{M_L\cdot m}{d_{T-L}^2}-\nonumber\\
2\dot G\frac{M_L\cdot m}{d_{T-L}^3}\cdot R_T+G\frac{M_L\cdot m}{d_{T-L}^2}&=0\nonumber\\
P-R-2\cdot G\frac{M_L\cdot m}{d_{T-L}^3}\cdot R_T&=0\label{secondeloimaree}
\end{align}
Pour établir l'équation \ref{DLmaree}, on a utilisé le développement limité~: \((1+x)^p\simeq1+p\cdot x\).
\begin{align*}
\frac{1}{(d_{T-L}-R_T)^2}&=\frac{1}{d_{T-L}^2}\cdot \frac{1}{(1-R_T/d_{T-L})^2}\\
&=\frac{1}{d_{T-L}^2}\cdot (1-R_T/d_{T-L})^{-2}\\
&\simeq \frac{1}{d_{T-L}^2}\cdot (1+(-2)\cdot (-\frac{R_T}{d_{T-L}}))\\
&=\frac{1}{d_{T-L}^2}\cdot (1+\frac{2\cdot R_T}{d_{T-L}})
\end{align*}

En réorganisant les termes de l'équation \ref{secondeloimaree}, on obtient finalement la force de réaction d'une balance qu'on placerait sous la masse \(m\) d'eau~:
\begin{equation}\label{forcedemareebalance}
R=P-2\cdot G\frac{M_L\cdot m}{d_{T-L}^3}\cdot R_T
\end{equation}
Cette équation se comprend par l'action du poids \(P\) sur la masse d'eau \(m\) et par une force, dite force de marée\index{force@force!de marée}, opposée au poids et qui soulève la masse.

\bigskip
La force de marée de l'équation \ref{forcedemareebalance} correspond à la force donnée par l'équation \ref{eqmaree}, page \pageref{eqmaree}, et le développement ci-dessus en constitue une démonstration.

L'analyse du rapport des forces de marées lunaire et solaire qui suit l'équation \ref{eqmaree}, page \pageref{eqmaree}, et qui est issue de la forme de l'équation \ref{forcedemareebalance}, constitue aussi une suite logique au calcul présenté ci-dessus que nous ne reprendrons pas ici.

\section{Analyse différentielle}
Le cadre de l'analyse présentée ci-dessus est celui de la seconde loi de Newton. Insistons sur le fait que l'ensemble des forces considérées s'exercent sur le même système, soit la masse m d'eau. Cette analyse est donc cohérente avec la formulation de la seconde loi qui s'applique sur un système de masse \(m\).

\medskip
On va maintenant présenter une autre analyse du problème qui est moins consistante puisque les forces qu'elle utilise ne s'appliquent pas sur un système unique. Cependant, non seulement elle mène au même résultat, mais encore elle permet de mettre en évidence le caractère différentiel des forces de marées, caractère qui apparaîtra alors plus clair dans l'étude des forces de marées exercées sur des satellites comme Io autour de Jupiter. La notion de limite de Roche pourra donc être mieux abordée au paragraphe \ref{limroche}.

\section{Autres rythmes}
L'explication de la présence de deux marées par jour (\emph{dites de pleines\index{pleine mer} et basses mers\index{basse mer}}) ne constitue qu'une partie de la compréhension du phénomène de marée. Même dans le cadre de la théorie de Newton, et sans entrer dans la théorie ondulatoire, d'autres périodes sont associées aux marées. Dans ce paragraphe on va entrer dans ce qu'on peut appeler la dimension astronomique du phénomène. Évidemment, c'est la gravitation qui reste le moteur des influences. Mais celles-ci varient en fonction de la position des astres.

\subsection{Décalages}
Une première conséquence du mouvement relatif de la Lune sur les marées se présente sous la forme d'un déplacement perpétuel des heure de pleine et basse mer. Si la Lune ne tournait pas autour de la Terre, les pleine\index{pleine mer} et basse\index{basse mer} mer auraient lieu toujours au même moment dans la journée. Ce n'est pas le cas en raison du déplacement de la Lune autour de la Terre. En effet, comme le montre la figure \ref{decalage}, pendant que la Terre fait un tour sur elle-même en vingt quatre heures, la lune se déplace. Comme cette dernière fait un tour autour de la Terre en trente jours, en vingt quatre heures, c'est-à-dire en une journée, la Lune parcours un angle de \(360/30=12^{\circ}\). Au bout de vingt quatre heures, une personne située au point \(P\) ne verrait plus la Lune au zénith\index{zenith@zénith} (au-dessus d'elle). Il lui faudrait parcourir encore un angle de \(12^{\circ}\) pour cela. Comme la terre parcours \SI{360}{\degree} en vingt quatre heures, il lui faut encore attendre \SI{48}{minutes} pour se retrouver en situation de pleine mer. Chaque jour, les pleines et basses mers se produisent avec environ cinquante minutes de retard sur le jour précédent.

\begin{figure}[t]
\centering
\caption[Décalage horaire]{Décalage horaire de la marée\label{decalage} \par \scriptsize{Une cinquantaine de minutes chaque jour.}}
\includegraphics[width=6cm]{Decalages.eps}
\end{figure}

\subsection{Marées de vives et mortes eaux\index{maree@marée!de vives et mortes eaux}}
Le marnage\index{marnage}, la différence de hauteur d'eau entre une pleine et une basse mer, varie dans le temps. Chaque mois, on constate un marnage maximum (marée de vive eau\index{vive eau}) et un minimum (marée de morte eau\index{morte eau}). On remarque aussi que ces marées particulières correspondent à certaines phases de la lune (voir la figure \ref{phasesdelalune}, page \pageref{phasesdelalune})~: lors des nouvelle\index{nouvelle lune} et pleine lune\index{pleine lune}, la marée est de vive eau et lors des quartiers, elle est de morte eau.

Comme les phases de la Lune correspondent à des positions particulières de notre satellite par rapport à l'axe Terre-Soleil, on peut en trouver une explication dans le rapport des actions gravifiques de la Lune et du Soleil. Comme on l'a vu (voir \ref{eqmaree}, page \pageref{eqmaree}), ce rapport est défavorable au Soleil, dont la masse est plus grande que celle de la Lune, mais qui se trouve plus loin que la Lune, en raison de la dépendance en \(1/d^3\) de la force de marée. Mais, même si elle est moins importante que celle de la Lune, l'action du Soleil existe.

\begin{figure}[t]
\centering
\caption[Vives et mortes eaux]{Marées de vives et mortes eaux\label{vivemorteeau} \par \scriptsize{Représentation dans le plan écliptique.}}
\includegraphics[width=6cm]{ViveMorteEau.eps}
\end{figure}

L'explication est simple. Comme le montre la figure \ref{vivemorteeau}, quand le Soleil, la Terre et la Lune sont alignés, les forces lunaires et solaires s'additionnent. On a des marées de vives eaux à la nouvelle et à la pleine lune. Par contre, aux premiers et derniers quartiers, les actions de la Lune et du Soleil ne se produisent pas dans le même axe et les marées sont de basses eaux.

\subsection{Marées d'équinoxes\index{maree@marée!d'équinoxe}}
On entend souvent parler des ``grandes marées d'équinoxes''. On se limitera ici à évoquer le phénomène, car il ne trouve une explication convaincante que dans la théorie ondulatoire des marées. Tout se passe comme si la force de marée exercée par le Soleil était maximale quand l'axe de rotation de la Terre est perpendiculaire à la direction Terre-Soleil. Si on considère que la direction de cet axe reste fixe dans l'espace, la figure \ref{saisonsperso}, page \pageref{saisonsperso}, présente la situation~: aux solstices\index{solstice}, l'angle \(\alpha\simeq \SI{66,5}{\degree}\), alors qu'aux équinoxes\index{equinoxe@équinoxe}, il vaut \SI{90}{\degree}. Pour bien s'en rendre compte, il faut considérer le plan constitué par l'axe de rotation de la Terre et le Soleil aux solstices. Ce plan est perpendiculaire au plan de l'écliptique\index{ecliptique@écliptique}. Mais si aux solstices l'axe de rotation de la Terre fait un angle non droit avec la direction Terre-Soleil, aux équinoxes cet angle est droit car ce plan est perpendiculaire à l'écliptique.

\subsection{Marées de périgée et périhélie\index{maree@marée!de périgée}\index{maree@marée!de périhélie}}
On sait grâce à Kepler (voir le paragraphe \ref{loiskepler}, page \pageref{loiskepler}) que l'orbite de la Lune est une ellipse\index{ellipse}. Cela signifie que la distance Terre-Lune varie. On nomme \emph{périgée}\index{perigee@périgée} le point de l'orbite de la Lune où cette distance est minimale et \emph{apogée}\index{apogee@apogée} celui où elle est maximale.

Au périgée, la distance Terre-Lune vaut~: \SI{3,654e8}{\metre} et à l'apogée \SI{4,067e8}{\metre}. Comme la force de marée est inversement proportionnelle au cube de la distance (ce qui donne une puissance \(^{-3}\)), la différence de force de marée lunaire entre le maximum et le minimum par rapport au minimum vaut~:
\begin{align*}
\Delta F_{mar\acute ee}^{lunaire}&=\frac{d_{p\acute erig\acute ee}^{-3}-d_{apog\acute ee}^{-3}}{d_{apo\acute ee}^{-3}}\\
&=\frac{(3,654\cdot 10^8)^{-3}-(4,067\cdot 10^8)^{-3}}{(4,067\cdot 10^8)^{-3}}\\
&\simeq0,38\equiv 38\%
\end{align*}
Ce rapport n'est de loin pas négligeable et il faut en tenir compte.

\medskip
L'orbite de la Terre autour du Soleil est aussi une ellipse\index{ellipse} et la distance Terre-Soleil varie aussi. On nome \emph{périhélie}\index{perihelie@périhélie} le point de l'orbite de la terre  où cette distance est minimale et \emph{aphélie}\index{aphelie@aphélie} le point le plus éloigné.

Au périhélie (le 3 janvier) la distance Terre-Soleil vaut~: \SI{1,471e11}{\metre} et à l'aphélie (le 3 juillet) \SI{1,521e11}{\metre}. Comme la force de marée est inversement proportionnelle au cube de la distance (ce qui donne une puissance \(^{-3}\)), la différence de force de marée solaire entre le maximum et le minimum par rapport au minimum vaut~:
\begin{align*}
\Delta F_{mar\acute ee}^{solaire}&=\frac{d_{p\acute erih\acute elie}^{-3}-d_{aph\acute elie}^{-3}}{d_{aph\acute elie}^{-3}}\\
&=\frac{(1,471\cdot 10^{11})^{-3}-(1,521\cdot 10^{11})^{-3}}{(1,521\cdot 10^{11})^{-3}}\\
&\simeq0,1\equiv 10\%
\end{align*}
Bien que plus faible que pour la Lune, ce rapport n'est pas non plus négligeable et il faut en tenir compte.

\subsection{Marées de déclinaison\index{maree@marée!de déclinaison}}
Jusque là, on a considéré que la lune tournait dans le plan de l'écliptique\index{ecliptique@écliptique}. Ce n'est en réalité pas le cas. Elle tourne dans un plan dont l'inclinaison par rapport à l'écliptique varie de \(5^{\circ}\) à \(5^{\circ}17'\) en oscillant sur une période de 173 jours. La symétrie axiale du champ de force de marée présentée à la figure \ref{mareechampforce}, page \pageref{mareechampforce}, évolue donc de part et d'autre de l'Équateur au cours du temps.

C'est un rythme de plus \dots

\subsection{Retards et marées côtières\index{maree@marée!côtière}}
La théorie newtonienne des marées, analysée à la lumière de l'astronomie de position, permet donc une première compréhension du phénomène de marée. Elle démontre clairement que le principal acteur qui l'explique est la Lune. Mais, elle n'explique de loin pas tout. En effet, on peut noter des décalages systématiques de plusieurs dizaines d'heures entre les marées de vive (ou morte) eau et les phases de la Lune. Ce retard ne s'explique qu'en terme d'ondes.

De plus, si les prédictions de la théorie newtonienne sont bons en haute mer, ils sont largement insuffisant près des côtes où des effets ondulatoires sont à prendre en compte.

\medskip
La théorie ondulatoire de la marée repose sur de complexes équations dites de Laplace et inaccessible dans le cadre de ce cours. Mais on peut trouver une bonne description mathématique de cette théorie dans \cite{BS07}.

\section{Limite de Roche\index{limite de Roche}}\label{limroche}
Les effets de marée présentés ci-dessus sont bien connus de tous puisqu'ils se manifestent clairement à nous sur les plages. Il faut aussi dire qu'il existe des marées terrestres~: simultanément à la surface océanique, la Lune déforme la croûte terrestre\index{croûte terrestre}. Et il faut savoir que ces effets sont réciproques. Si la Lune exerce son influence sur la Terre, la Terre exerce aussi des forces de marée sur la Lune et celle-ci se déforme sous leurs effets. De la même manière, le satellite Io\index{Io} a un volcanisme\index{volcanisme} très actif dû aux effets de marées produits sur lui par la planète Jupiter.

Tant qu'un satellite est assez éloigné de la planète qui l'attire, les effets de marée se limitent à des frictions et à des déformations. Par contre, si le satellite s'en approche trop, il peut être détruit par les tensions internes qu'il va subir. C'est ce qui explique en partie pourquoi les anneaux d’astéroïdes\index{anneau d'astéroïdes}, comme ceux de Saturne\index{Saturne}, ne donnent pas naissance à des satellites sous l'effet de leur attraction mutuelle. La distance à partir de laquelle cela se produit est complexe à déterminer. Cependant, à l'aide de quelques approximations judicieuses, il est possible d'évaluer cette distance, qui porte le nom de \emph{limite de Roche} en hommage au physicien qui la découvrit.

\subsection{Modèle simplifié}
L'idée est de considérer un satellite situé à une distance \(d\) d'une planète comme composé de deux corps distincts maintenus ensemble par leur action gravifique mutuelle. La figure \ref{roche} présente la situation.

\begin{figure}[t]
\centering
\caption[Limite de Roche]{Limite de Roche\label{roche}}
\includegraphics[width=6cm]{Roche.eps}
\end{figure}

En raison de sa plus grande proximité, la force de gravitation exercée par la planète sur la partie du satellite la plus proche d'elle est plus importante que celle exercée sur la partie la plus éloignée. En supposant que les centres de masse des deux parties du satellite sont séparés par une distance \(2\cdot r\), on peut calculer la différence de force qui s'exerce~:
\[\Delta F=G\cdot \frac{M\cdot m}{(d-r)^2}-G\cdot \frac{M\cdot m}{(d+r)^2}\]
Pour simplifier cette expression, on peut écrire~:
\begin{align*}
\Delta F&=G\cdot \frac{m\cdot m}{d^2\cdot (1-r/d)^2}-G\cdot \frac{M\cdot m}{d^2\cdot (1+r/d)^2} \\
&=G\cdot \frac{M\cdot m}{d^2}\cdot ((1-r/d)^{-2})-(1+r/d)^{-2})
\end{align*}
En raison de la relation (de développement limité) suivante~:
\[(1+x)^p\simeq 1+p\cdot x\]
et du fait que \(r\ll d\), on peut réécrire la différence de force~:
\begin{align*}
\Delta F&=G\cdot \frac{M\cdot m}{d^2}\cdot ((1+2\cdot r/d)-(1-2\cdot r/d)) \\
&=G\cdot \frac{M\cdot m}{d^2}\cdot 4\cdot r/d \\
&=4\cdot G\cdot \frac{M\cdot m}{d^3}\cdot r
\end{align*}

\medskip
D'autre part, la force de cohésion gravitationnelle entre les deux parties du satellite est donnée par~:
\[F_c=G\cdot \frac{m\cdot m}{(2\cdot r)^2}\]

\medskip
Mais, le satellite est détruit quand la différence de force gravitationnelle (force de marée) produite par la planète sur le satellite est plus grande que la force de cohésion, c'est-à-dire~:
\[4\cdot G\cdot \frac{M\cdot m}{d^3}\cdot r > G\cdot \frac{m\cdot m}{(2\cdot r)^2}\]

\medskip
En utilisant les masses volumiques des deux corps, \(\rho_p\) pour la planète et \(\rho_s\) pour le satellite, on a~:
\begin{align*}
M&=\frac{4}{3}\cdot \pi\cdot R^3\cdot \rho_p \\
m&=\frac{4}{3}\cdot \pi\cdot r^3\cdot \rho_s
\end{align*}
où \(R\) est le rayon de la planète. Cela permet d'écrire~:
\begin{align*}
4\cdot \frac{R^3\cdot \rho_p\cdot r^3\cdot \rho_s}{d^3}\cdot r &> \frac{(r^3\cdot \rho_s)^2}{4\cdot r^2} \\
4\cdot \frac{R^3\cdot \rho_p}{d^3} &> \frac{\rho_s}{4} \\
16\cdot R^3\cdot \frac{\rho_p}{\rho_s} &> d^3 \\
d &< R\cdot \sqrt[3]{16\cdot \frac{\rho_p}{\rho_s}}
\end{align*}
avec finalement l'expression de la limite de Roche, c'est-à-dire la distance minimale, par rapport à la planète, à laquelle le satellite est détruit~:
\begin{equation}
d_{Roche} = R\cdot \sqrt[3]{16\cdot \frac{\rho_p}{\rho_s}}
\end{equation}

\begin{figure}[t]
\centering
\caption[Limite de Roche]{Limite de Roche\label{limitederoche}\endnote{Voir le site de l'encyclopédie Wikipedia~: \url{http://fr.wikipedia.org/wiki/Limite_de_Roche} notamment pour le copyright de l'image.}}
\includegraphics[width=6cm]{LimiteRoche1.eps}
\includegraphics[width=6cm]{LimiteRoche2.eps}
\includegraphics[width=6cm]{LimiteRoche3.eps}
\includegraphics[width=6cm]{LimiteRoche4.eps}
\includegraphics[width=6cm]{LimiteRoche5.eps}
\end{figure}

\subsection{Exemples}
L'une des planètes les plus intéressantes du point de vue de la limite de Roche est certainement Saturne en raison des ses satellites et de ses anneaux. Pour se faire une idée numérique du calcul de la limite de Roche, considérons par exemple Épiméthée, l'un de ses satellites dont la masse volumique vaut~:
\[\rho_s=\SI{0,61e3}{\kilo\gram\per\cubic\metre}\]
Comme la masse volumique de Saturne vaut~:
\[\rho_p=\SI{0,6873e3}{\kilo\gram\per\cubic\metre}\]
et que son rayon équatorial est \(R=\SI{60268}{\kilo\metre}\), on peut calculer la limite de Roche~:
\begin{align*}
d_{Roche} &= R\cdot \sqrt[3]{16\cdot \frac{\rho_p}{\rho_s}}\\
&= 60'268\cdot \sqrt[3]{16\cdot \frac{0,6873\cdot 10^3}{0,61\cdot 10^3}}=\SI{158027}{\kilo\metre}
\end{align*}
Sachant que le rayon de l'orbite d'Épiméthée est de 2,51 fois le rayon de Saturne, soit environ \SI{151400}{\kilo\metre}, on peut calculer le rapport \(r\) de la distance d'Épiméthée-Saturne à la distance de Roche~:
\[r=\frac{151'400}{158'027}=0,96\]
Épiméthée est donc à une distance de Saturne correspondant à 96\% de la limite de Roche. Cela peut s'expliquer par le fait que d'autres forces de cohésion que la gravité s'exercent dans le satellite.

Il est aussi intéressant de constater que les anneaux G et E de Saturne, situés respectivement de \SI{170000}{} à \SI{175000}{\kilo\metre} et de \SI{181000}{} à \SI{483000}{\kilo\metre} de Saturne, sont bien au-delà de la limite de Roche. Pour l'anneau E, il pourrait s'agir de matière volcanique éjectée par Encelade, l'un des satellites de Saturne.

En réalité le calcul de la limite de Roche réelle est plus compliqué que celui présenté ci-dessus et il mène à la distance suivante~:
\begin{equation}
d_{roche} = 2,422649865\cdot R\cdot \sqrt[3]{\frac{\rho_p}{\rho_s}}
\end{equation}
soit pour Épiméthée la valeur de \SI{151932}{\kilo\metre}.
