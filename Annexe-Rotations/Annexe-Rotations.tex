\myclearpage

\chapter{Rotations}

\lettrine{L}{'objectif} de cette annexe est de prendre conscience des mouvements auxquels nous participons sans même le savoir. Il est aussi de se rendre compte que les vitesses qui leur correspondent sont difficilement imaginables et plus, elles sont relatives et il est parfois difficile de savoir par rapport à quoi les décrire.

Ces mouvements sont cependant assez bien connus pour qu'on puisse prévoir le futur proche de notre position dans l'espace.

\section{Rotation de la Terre\index{rotation!de la terre} sur elle-même}

On se propose de calculer la vitesse de rotation d'un point à l'arrêt sur l'équateur terrestre\index{equateur@équateur!terrestre}.

Le calcul est simple. Il se base sur les données suivantes~:

\begin{itemize}
\item le rayon de la Terre à l'équateur vaut~: \(R_T=\SI{6,378e6}{\metre}\) et
\item la période sidérale de rotation\index{periode@période!sidérale} de la Terre sur elle-même vaut~: \(T_s=23\,h\,56\,mn\,4,1\,s=\SI{86164,1}{\second}\)
\end{itemize}

Le calcul est celui de la vitesse d'un corps en rotation sur un cercle de rayon et de période donnés. On a~:
\begin{align*}
v&=\frac{d}{t}=\frac{2\cdot \pi\cdot R_T}{T_s}\\
&=\frac{2\cdot \pi\cdot 6,378\cdot 10^6}{86'164,1}\\
&=\SI{465,1}{\metre\per\second}=\SI{1674}{\kilo\metre\per\hour}
\end{align*}
A elle seule, cette vitesse est déjà fantastique. Nous ne la ressentons pas ou peu à cause de l'inertie\index{inertie}.

\section{Rotation de la Terre\index{rotation!de la Terre} autour du Soleil\index{Soleil}}

On se propose de calculer la vitesse de rotation de la Terre autour du Soleil.

Le calcul est simple. Il se base sur les données suivantes~:

\begin{itemize}
\item la distance de la Terre au Soleil vaut environ~: \(R_{T\rightarrow S}=\SI{1,496e11}{\metre}\) et
\item la période sidérale de rotation de la Terre autour du Soleil vaut~: \(T_s=\SI{365,263}{\day}=\SI{31,559e6}{\second}\)
\end{itemize}

Le calcul est celui de la vitesse d'un corps en rotation sur un cercle de rayon et de période donnés. On a~:
\begin{align*}
v&=\frac{d}{t}=\frac{2\cdot \pi\cdot R_{T\rightarrow S}}{T_s}\\
&=\frac{2\cdot \pi\cdot 1,496\cdot 10^{11}}{31,559\cdot 10^6}\\
&=\SI{29784,4}{\metre\per\second}=\SI{107224}{\kilo\metre\per\hour}
\end{align*}
Cette vitesse est encore plus fantastique. Nous ne la ressentons à nouveau pas ou peu à cause de l'inertie\index{inertie}.

\bigskip

Ce qui vient d'être dit montre qu'il est important de préciser \emph{par rapport à quoi} on observe le mouvement. Historiquement cette question a pris beaucoup d'importance lors de la mise en question de l'immobilité de la Terre. En effet, si pendant longtemps l'idée que la Terre tourne autour du Soleil a paru absurde, aujourd'hui elle est évidente. Et au contraire, c'est l'idée qu'on puisse considérer que le Soleil tourne autour de la Terre qui est devenue absurde. A tel point qu'on envisage même plus qu'on puisse l'affirmer effectivement. Or, la relativité\index{relativité} des mouvements permet de dire à juste titre que le Soleil tourne autour de la Terre pour autant qu'on observe le mouvement depuis celle-ci. La Terre tourne autour du Soleil par rapport au Soleil et le Soleil tourne autour de la Terre par rapport à la Terre. Ce que Galilée lui-même, dans son ``Dialogue sur les deux grands systèmes du monde'', a exprimé par~:

\begin{quotation}
``\textit{Les choses reviennent au même si la Terre seule se meut alors que tout le reste de l'Univers est immobile ou si, alors que la Terre seule est immobile, tout l'Univers se meut d'un même mouvement}''\footnote{On trouve dans \cite[p. 274]{GG92} le texte suivant~: \begin{quotation}``\textit{si la Terre est immobile, il faut que le Soleil et les étoiles fixes se meuvent, mais il se peut aussi que le Soleil et les fixes soient immobiles si c'est la Terre qui se meut.}''\end{quotation}}

\footnotesize{\cite[p. 60, sans référence à la page originale.]{JR07}}
\end{quotation}

Qu'en est-il alors de la fameuse question historique de l'immobilité de la Terre\index{immobilite de la terre@immobilité de la terre} ? Celle-ci révèle, en réalité, deux problèmes qui ne remettent pas en cause la possibilité d'une description relative des mouvements du Soleil et de la Terre.

Le premier problème se traduit dans l'immobilité de la Terre \emph{dans l'univers}. C'est un problème autant cosmologique que physique. Cosmologique, car il met en cause une vision de l'univers historique et religieuse qui dépasse clairement la simple relativité des mouvements. Physique, car il met en jeu la question de l'inertie\index{inertie} qui elle aussi dépasse le cadre de la relativité des mouvements. Nous n'en discuterons pas ici en raison de sa complexité, mais on peut relever qu'elle n'empêche nullement d'affirmer que le Soleil tourne autour de la Terre.

Le second problème tient dans la cinématique des planètes du système solaire. Il est aujourd'hui incontestable que les planètes tournent autour du Soleil selon des orbites elliptiques\index{orbite!elliptique} et non, comme l'envisageait Ptolémée\index{Ptolemee@Ptolémée}, autour de la Terre. Car le modèle utilisé à l'époque, qui faisait tourner les planètes autour de la Terre et sur des épicycles\index{epicycle@épicycle}\footnote{Les épicycles sont de petits cercles dont le centre tourne sur un plus grand cercle, le déférent, centré sur la Terre. Les planètes tournent sur les épicycles qui eux-mêmes tournent autour de la Terre.} pour expliquer leur rétrogradation\index{retrogradation@rétrogradation}\footnote{La rétrogradation est le fait que les planètes plus éloignées du Soleil que la Terre, semblent parfois, vu depuis la Terre, revenir en arrière dans le ciel par rapport aux étoiles fixes.} dans le ciel, est clairement faux. Les observations, notamment celle de l'orbite de Mars\index{orbite!de mars}, l'invalident. Mais, il n'est pas faux en raison de l'affirmation de la rotation du Soleil autour de la Terre, mais en raison de celle de la rotation des planètes autour de la Terre sur des cercles avec des épicycles.

\begin{figure}[t]
\centering
\caption[Le système géocentrique de Tycho Brahé]{Le système géocentrique de Tycho Brahé\label{tychosystem} \par \scriptsize{Un modèle encore actuel\endnote{Voir le site de l'encyclopédie Wikipedia~: \url=http://en.wikipedia.org/wiki/Image:Tychonian.gif= notamment pour le copyright de l'image.}.}}
\includegraphics[width=6cm]{TychoSystem.eps}
\end{figure}

Cette erreur, qui est celle du modèle de Ptolémée, si elle est bien une erreur, n'empêche pas qu'on puisse considérer que le Soleil tourne autour de la Terre, \emph{vu depuis la Terre}. Elle fut d'ailleurs reconnue et corrigée par Tycho Brahé\index{Tycho Brahe@Tycho Brahé} qui proposa un modèle (voir figure \ref{tychosystem}) où la Terre restait fixe dans l'univers, où le Soleil tournait autour d'elle et les autres planètes tournaient autour\dots\ du Soleil. Ce modèle, excepté la fixité de la Terre dans l'univers qui relève du premier problème énoncé ci-dessus, est parfaitement valide. Il s'agit tout simplement de la vision du système solaire \emph{relativement à la Terre}. Et la description actuelle des mouvements célestes vu depuis la Terre adopte un point de vue très proche de celui de Tycho Brahé, exception faite des orbites circulaires\index{orbite!circulaire} qui sont devenues des ellipses.

\section{Rotation du Soleil\index{rotation!du Soleil} dans la Voie Lactée\index{Voie Lactée}}

On se propose de calculer la vitesse de rotation du Soleil dans notre galaxie, la Voie Lactée.

La figure \ref{milky_way_2005} présente une vue d'artiste de la Voie Lactée telle qu'on se la représente. La position du Soleil y figure sous la forme d'un point jaune.

\begin{figure}[th]
\centering
\caption[Le Soleil dans la Voie Lactée]{Le soleil dans la Voie Lactée\label{milky_way_2005} \par \scriptsize{Représentation artistique\endnote{Voir le site de l'encyclopédie Wikipedia~: \url=http://commons.wikimedia.org/wiki/Image:Milky_Way_2005.jpg=. Image dans le domaine public. Remerciements à la NASA.}}}
\includegraphics[width=6cm]{Milky_Way_2005_soleil.eps}
\end{figure}

Le calcul est simple. Il se base sur les données suivantes~:

\begin{itemize}
\item la distance du Soleil au centre de la galaxie vaut environ~: \(R_{S\rightarrow G}=\SI{26000}{\lightyear}\) et
\item la période sidérale\index{periode@période!sidérale} de rotation du Soleil autour du centre de la galaxie vaut environ~: \(T_s=\SI{220e6}{\year}\)
\end{itemize}

On calcule le nombre de secondes que représentent 220 millions d'années~:
\begin{align*}
220\cdot 10^6\,ans &= 220\cdot 10^6\cdot 365\cdot 24\cdot 3600\\
&= \SI{6,937e15}{\second}
\end{align*}

Pendant ce temps, le Soleil fait un cercle de rayon \SI{26000}{\lightyear}. La distance qu'il parcourt vaut donc~:
\[d =2\cdot \pi\cdot 26'000\cdot 9,4608\cdot 10^{15} = \SI{1,546e21}{\metre}\]

car, comme la vitesse de la lumière vaut \SI{300000}{\kilo\metre\per\second}, on a~:
\[1\,AL = 300'000'000\cdot 3600\cdot 24\cdot 365 = \SI{9,4608e15}{\metre}\]

Le calcul est alors celui de la vitesse d'un corps en rotation sur un cercle de rayon et de période donnés. On a~:
\begin{align*}
v&=\frac{d}{t}=\frac{2\cdot \pi\cdot R_{S\rightarrow G}}{T_s}\\
&=\frac{1,546\cdot 10^{21}}{6,937\cdot 10^{15}}\\
&=\SI{222863}{\metre\per\second}=\SI{222,863}{\kilo\metre\per\second}=222,863\cdot 3600\\
&=\SI{802306}{\kilo\metre\per\hour}
\end{align*}
Cette vitesse est incroyable. Nous ne la ressentons à nouveau pas ou peu toujours à cause de l'inertie\index{inertie}.

Notons que cette vitesse est la même pour toutes les étoiles proches du Soleil qui participent au mouvement de rotation autour du centre de la galaxie. Mais le Soleil a aussi un mouvement propre, c'est-à-dire qu'une partie de sa vitesse ne correspond pas à sa vitesse de rotation autour du centre de la galaxie. Cette composante vaut environ \SI{20}{\kilo\metre\per\second}.\endnote{Voir le site \url=http://www.dil.univ-mrs.fr/~gispert/enseignement/astronomie/5eme_partie/voieLactee.php=}

Relevons enfin une règle bien pratique pour la transformation d'unité entre les \si{\metre\per\second} et les \si{\kilo\metre\per\hour}. On a en effet~:
\[\SI{1}{\kilo\metre\per\hour}=\frac{1\,km}{1\,h}=\frac{1000\,m}{3600\,s}=\SI{1 / 3,6}{\metre\per\second}\]
Ainsi, pour transformer des \si{\kilo\metre\per\hour} en \si{\metre\per\second}, il faut diviser le nombre correspondant aux \si{\kilo\metre\per\hour} par le facteur 3,6. Inversement, pour passer de \si{\metre\per\second} en \si{\kilo\metre\per\hour}, il faut multiplier les \si{\metre\per\second} par 3,6.

\section{Vitesse et référentiel}

La question de savoir par rapport à quoi on calcule la vitesse se pose donc puisque les différentes vitesses calculées précédemment constituent chacune une partie de la vitesse d'un point fixe à l'équateur terrestre\index{equateur@équateur}. Toutes ces vitesses sont donc relatives à des référentiels\index{referentiel@référentiel} différents.

Par exemple, la vitesse de rotation de la Terre sur elle-même est calculée dans un référentiel lié à la Terre, au centre de celle-ci, mais ne tournant pas par rapport à elle. Il peut être fixé sur des étoiles proches qui, à l'échelle de la période de rotation de la Terre, ne se déplacent pratiquement pas.

Pour évaluer la vitesse caractéristique du mouvement propre du Soleil dans la galaxie, mouvement auquel on a soustrait celui de rotation du Soleil autour du centre de la galaxie, il faut utiliser un référentiel dit LSR pour Local Standard of Rest\index{Local Standard of Rest}, c'est-à-dire un référentiel standard local de repos. Celui-ci a été construit à l'aide des vitesses et positions moyennes des étoiles proches du Soleil. Celles-ci sont donc statistiquement au repos dans le référentiel LSR. C'est ainsi qu'on peut étudier le mouvement propre du Soleil.

En conclusion, on voit qu'il est indispensable de toujours rapporter un mouvement à un référentiel\index{referentiel@référentiel} pour que la vitesse associée ait du sens.

Cependant, on sait aujourd'hui qu'il n'existe pas de référentiel absolu\index{referentiel@référentiel!absolu} auquel tout mouvement pourrait être rapporté. Newton a cru en l'existence d'un tel référentiel, Einstein, dans la relativité restreinte\index{relativité!restreinte} et à l'aide des expériences de Michelson et Morley\index{Michelson et Morley}, a démontré qu'il n'en existait pas. Un mouvement est donc nécessairement toujours rapporté à un autre corps qui tient lieu de référentiel.