\chapter{L'énergie\index{energie@énergie}}
\section{Introduction}
\lettrine{A}{vec la physique} de Newton, tout problème de mécanique peut être résolu. Mais le problème fondamental de cette dynamique est que toutes les grandeurs utilisées sont en constante évolution au cours du temps. L'idée d'une mécanique se situant au niveau de grandeurs conservée\index{grandeur@grandeur!conservée} au cours du temps est donc apparue. Nous allons voir que cette "nouvelle" mécanique utilise des grandeurs comme la vitesse\index{vitesse@vitesse}. Cela situe cette théorie à un niveau différent de la mécanique de Newton puisque celle-ci, à travers la seconde loi, lie la cause du mouvement\index{cause@cause!du mouvement} à l'accélération, alors que la conservation\index{conservation@conservation} de l'énergie\index{energie@énergie} est liée à la vitesse. On peut résumer cela comme dans la figure \ref{energie}.
\begin{figure}[H]
\centering
\caption{Énergie et vitesse\label{energie}}
\includegraphics{Energie.eps}
\end{figure}

La conséquence mathématique de cette nouvelle situation du problème est que l'intégration nécessaire pour obtenir la vitesse\index{vitesse@vitesse} à partir de l'accélération\index{acceleration@accélération} est supprimée. Si la grandeur recherchée est la vitesse (ou la position) le problème est donc considérablement simplifié.

\section{Le travail\index{travail@travail}}
\subsection{Historiquement\label{historiquement}}
En physique, le travail est une notion bien précise. Elle a pour origine l'expérience simple décrite sur la figure \ref{balance}~:
\begin{figure}[H]
\centering
\caption{Balance à fléau\label{balance}}
\includegraphics[width=6cm]{Equilibre.eps}
\end{figure}

L'idée est la suivante~:
on considère une balance équilibrée par deux masses. La condition d'équilibre veut que~:
\[m_{gauche}\cdot d_{gauche}=m_{droite}\cdot d_{droite}\]
ce qui est le cas sur la figure \ref{balance}.

Maintenant, si on descend la masse \(m_{gauche}\) de 10 cm, la masse \(m_{droite}\) monte de 20 cm. En effet~:
\[\frac{0,1}{1}=\sin(\alpha_{support})=\frac{0,2}{2}\]

Ainsi, on remarque que le produit \(A\) du poids de la masse par la hauteur déplacée est le même pour les deux masses~:
\[A_{gauche}=2\cdot9,81\cdot0,1=1\cdot9,81\cdot0,2=A_{droite}\]

La grandeur \(A=F\cdot d\) est donc identique. On peut traduire cette remarque en disant que le travail\index{travail@travail} (\(A\) pour Arbeit\index{Arbeit@Arbeit}) pour monter une masse de 1 kg sur une hauteur de 20 cm est le même que celui pour monter une masse de 2 kg sur 10 cm.

Attention, il ne faut pas voir là déjà une conservation. Bien entendu, il y a derrière cette expérience la conservation de l'énergie. Mais le concept de travail utilisé ici, s'il est intimement lié à celui d'énergie potentielle, comme nous le verrons par la suite, reste lié à un déplacement et non à un équilibre, à une situation spatiale des corps utilisés. C'est pourquoi il traduit la naissance de la notion de travail. Cependant cette liaison avec la conservation de l'énergie est assez typique pour que cet exemple ait sa place ici, même si il peut porter à confusion.

\subsection{Définition}
\subsubsection{Travail simple\index{travail@travail!simple}}
La définition la plus simple que l'on puisse envisager est donc~:
\[A_{F,d}=F\cdot d\]

Cette définition correspond au travail \(A\) d'une force \(F\) s'exerçant sur une masse m que l'on déplace sur une distance \(d\) (voir figure \ref{travailsimple}).
\begin{figure}[H]
\centering
\caption{Travail simple\label{travailsimple}}
\includegraphics{Travailsimple.eps}
\end{figure}

Remarquons qu'il s'agit toujours du travail d'une force sur une distance donnée. Parler du travail sans aucune autre précision n'a pas de sens.

\subsubsection{Travail et produit scalaire\index{produit@produit!scalaire}}
Une force qui ne s'exercerait pas parallèlement (et dans le même sens) que le déplacement, ne pourrait pas produire un travail simple. On
peut comprendre intuitivement qu'une force s'exerçant perpendiculairement au déplacement ne travaille pas. On peut donc définir le travail d'une manière plus générale~:
\[A_{F,d}=\overrightarrow{F\cdot}\overrightarrow{d}=\left\Vert \overrightarrow{F}\right\Vert \cdot\left\Vert \overrightarrow{d}\right\Vert \cos(\alpha)=F\cdot d\cdot\cos(\alpha)\]

Cette définition correspond à la situation de la figure \ref{travailvecteur}.
\begin{figure}[H]
\centering
\caption{Travail et produit scalaire\label{travailvecteur}}
\includegraphics{Travailvecteur.eps}
\end{figure}

Attention, cette définition est valable pour un déplacement rectiligne et une force constante vectoriellement.

Remarquons les cas particuliers de cette définition~:
\begin{enumerate}
\item Si \(\overrightarrow{F}\) et \(\overrightarrow{d}\) sont parallèles et de même sens (\(\overrightarrow{F}\left\uparrow \right\uparrow \overrightarrow{d}\)), alors le travail est simple.
\item Si \(\overrightarrow{F}\) et \(\overrightarrow{d}\) sont perpendiculaires (\(\overrightarrow{F}\bot\overrightarrow{d}\)), alors \(\cos(\alpha)=0\) et le travaille est nul. On dit que la force ne travaille pas.
\item Si \(\overrightarrow{F}\) et \(\overrightarrow{d}\) sont parallèles, mais de sens opposés (\(\overrightarrow{F}\left\uparrow \right\downarrow \overrightarrow{d}\)), alors le travail est simple, mais négatif.
\end{enumerate}

\subsubsection{Travail\index{travail@travail} cas général}
Dans ce cas, le déplacement n'est pas forcément rectiligne et la force pas forcément constante vectoriellement. La situation générale correspond donc à la figure \ref{travailgeneral}:
\begin{figure}[H]
\centering
\caption{Travail en général\label{travailgeneral}}
\includegraphics{Travailgeneral.eps}
\end{figure}

Ainsi, pour déterminer le travail total effectué par la force sur le chemin A-B, il faut décomposer ce dernier en petits bouts de déplacement rectilignes \(\overrightarrow{\Delta l}\), sur lesquels la force peut être considérée comme vectoriellement constante (c'est-à-dire qu'elle ne change ni en direction, ni en sens, ni en grandeurs).On est ainsi ramené au calcul d'un petit élément de travail \(A_{i}\), pour une force \(\overrightarrow{F_{i}}\) constante, sur un déplacement \(\overrightarrow{\Delta l_{i}}\)~:
\[A_{i}=\overrightarrow{F_{i}}\cdot\overrightarrow{\Delta l_{i}}\]

Puis, on somme tous les \(A_{i}\) pour obtenir le travail total de A à B~:
\[A_{AB}\cong\sum_{i=1}^{n}A_{i}=\sum_{i=1}^{n}\overrightarrow{F}\cdot\overrightarrow{\Delta l_{i}}\]

Bien entendu, plus les segments \(\overrightarrow{\Delta l_{i}}\) sont petits, plus on {}``colle'' au parcours. A la limite, si les \(\overrightarrow{\Delta l_{i}}\) devenaient infiniment petits, on obtiendrait la valeur exacte du travail sur le trajet AB. On peut donc écrire~:
\[A_{AB}=\begin{array}[t]{c}
lim\\
\overrightarrow{\Delta l_{i}}\rightarrow0\end{array}\sum_{i}^{\infty}\overrightarrow{F}\cdot\overrightarrow{\Delta l_{i}}=\int_{A}^{B}\overrightarrow{F}\cdot\overrightarrow{dl}\]

La définition tout-à-fait générale du travail est donc finalement~:
\begin{equation}\label{travail}
\fbox{\(\displaystyle A=\int_{A}^{B}\overrightarrow{F}\cdot\overrightarrow{dl}\)}
\end{equation}
Finalement, il faut indiquer les unités SI du travail. On a~:
\[\left[A\right]=\left[F\right]\cdot\left[l\right]=N\cdot m=J=Joules\]

\subsubsection*{Exemples}

\begin{enumerate}
\item Quel est le travail simple effectué par une force F de \SI{5}{\newton}, sur une distance d de \SI{5}{\metre}~?

Solution~:
\[A = F\cdot d = 5\cdot 5 = \SI{25}{\joule}\]
\item Quel est le travail effectué par une force F de \SI{5}{\newton}, s'exerçant avec un angle de \SI{20}{\degree} par rapport au déplacement, sur une distance de \SI{5}{\metre}~?

Solution~:
\[A = F\cdot d\cdot\cos(\alpha)=5\cdot 5\cdot\cos(\SI{20}{\degree})=\SI{23,5}{\joule}\]
\item Quel est le travail effectué par une force de frottement F de \SI{5}{\newton}, sur une distance d de \SI{5}{\metre}~?

Solution~:
\[A = F\cdot d\cdot\cos(\alpha)=5\cdot 5\cdot\cos(\SI{180}{\degree})=\SI{-25}{\joule}\]
car la force de frottement s'exerce toujours dans le sens contraire du déplacement.
\item Quel est le travail effectué par une force F de valeur \(l\), colinéaire (parallèle) au déplacement rectiligne et de même sens, sur une distance de \SI{5}{\metre}.
\medskip

Solution~:
\[\begin{array}{cc}
A & =\int_{0}^{5}\overrightarrow{F\cdot}\overrightarrow{dl}\begin{array}[t]{c}
=\int_{0}^{5}F\cdot dl\\
\begin{array}[b]{c}
\uparrow\\
car\end{array}\overrightarrow{F}\left\uparrow \right\uparrow \overrightarrow{dl}\end{array}=\int_{0}^{5}l\cdot dl\\
 & =\left.\frac{1}{2}\cdot l^{2}\right|_{0}^{5}=\frac{1}{2}\cdot(25-0)=12,5\, J\end{array}\]
\end{enumerate}

\section{L'énergie\index{energie@énergie}}
\subsection{Introduction}
L'idée d'énergie est intimement liés à celle de travail\index{travail@travail}. En effet, lorsqu'on fournit un travail, quelque chose est produit. De la chaleur\index{chaleur@chaleur} par exemple lorsque qu'on s'intéresse au travail de la force de frottement\index{force@force!de frottement} d'une table sur laquelle on déplace un objet. Cependant, on peut se demander ce qui est produit lorsqu'on fournit un travail pour monter une masse ou pour augmenter sa vitesse. En réalité, dans les deux cas on produit de l'énergie, potentielle\index{energie@énergie!potentielle} et cinétique\index{energie@énergie!cinétique} respectivement.

\subsection{Énergie potentielle\index{energie@énergie!potentielle}}
Quand on travaille pour monter une charge, on produit de l'énergie potentielle. Cette énergie peut être retrouvée si on lâche alors la
masse. Arrivé en bas, cette dernière est capable de produire une déformation traduisant un travail\index{travail@travail}. Tout se passe donc comme si l'énergie potentielle était quelque chose de stocké dans la masse alors qu'elle se trouve à une hauteur déterminée. Bien entendu, plus la hauteur est grande, plus l'énergie potentielle est importante (une pierre de 10 g lâchée de 10 m fera plus de dégâts arrivée au sol que la même pierre lâchée de 1 m). De même pour la masse.

Pour déterminer la valeur de l'énergie potentielle contenue dans une masse \(m\) placée à une hauteur \(h\), il faut donc calculer le travail que cette masse produit en chutant depuis cette hauteur. Plus précisément, il faut calculer le travail du poids de la masse \(m\) se déplaçant sur la hauteur \(h\). On doit donc écrire~:
\begin{align*}
A &=\overrightarrow{F}\cdot\overrightarrow{d}=m\overrightarrow{g}\cdot\overrightarrow{h}=mg\cdot h&\text{car}\;m\overrightarrow{g}\left\uparrow \right\uparrow \overrightarrow{h}\\
&=mg\cdot(h_{i}-h_{f})=mgh_{i}-mgh_{f}\\
&=E_{pot\, i}-E_{pot\, f}=-\Delta E_{pot}
\end{align*}

où le déplacement \(h\) est décomposé en une différence de hauteur \(h_{i}-h_{f}\).

On remarque que ce travail se compose de deux parties. Chacune d'elle ne dépend que du lieu où elle est évaluée et de la masse de l'objet. On peut donc appeler chacun de ces termes "énergie potentielle\index{energie@énergie!potentielle}" à la hauteur considérée. Ainsi, le travail se traduit par une différence d'énergie potentielle. Et sa définition prend la forme suivante~:
\begin{equation}\label{enpotdef}
\fbox{\(\displaystyle E_{pot}=m\cdot g\cdot h\)}
\end{equation}

\subsection{Énergie cinétique\index{energie@énergie!cinétique}\label{énergie cinétique}}
Quand on travaille pour augmenter la vitesse d'un corps, on produit de l'énergie cinétique.

Pour déterminer la valeur de celle-ci lorsque le corps de masse \(m\) passe d'une vitesse \(v_{o}\) à une vitesse \(v\), il faut donc calculer
le travail\index{travail@travail} pour réaliser cette transformation. On a~:
\begin{align*}
A&=\overrightarrow{F}\cdot\overrightarrow{d}=F\cdot d &&\mbox{ car }\overrightarrow{F}\left\uparrow \right\uparrow \overrightarrow{d}\\
&=ma\cdot d &&\mbox{ car }F=m\cdot a\\
&=m\cdot\frac{v^{2}-v_{o}^{2}}{2\cdot d}\cdot d &&\mbox{ pour un MRUA}\\
&=\frac{1}{2}\cdot m\cdot v^{2}-\frac{1}{2}\cdot m\cdot v_{o}^{2}\\
&=E_{cin}-E_{cin\, o}=\Delta E_{cin}
 \end{align*}

On remarque que ce travail se compose de deux parties. Chacune d'elle ne dépend que de la vitesse à l'instant considéré et de la masse de
l'objet. On peut donc appeler chacun de ces termes "énergie cinétique\index{energie@énergie!cinétique}"
pour la vitesse considérée. Ainsi, le travail se traduit par une différence
d'énergie cinétique. Et sa définition prend la forme suivante~:
\begin{equation}\label{eqencindef}
\fbox{\(\displaystyle E_{cin}=\frac{1}{2}\cdot m\cdot v^{2}\)}
\end{equation}

\subsection{Énergie mécanique\index{energie@énergie!mécanique}}
Définissons encore la somme des énergie cinétique et potentielle comme l'énergie mécanique d'une masse m~:
\begin{equation}\label{enmec}
E_{m\acute ec}=E_{cin}+E_{pot}=\frac{1}{2}\cdot m\cdot v^{2}+m\cdot g\cdot h
\end{equation}

Celle-ci nous sera utile par la suite.

\subsection{Exemple}
Déterminez l'énergie mécanique d'une masse de 3 kg qui se trouve à un instant donné à une hauteur de 4 m et se déplace alors à une vitesse de 5 m/s.

Solution~:
\[E_{m\acute ec}=\frac{1}{2}\cdot3\cdot5^{2}+3\cdot9,81\cdot4=\SI{155,22}{\joule}\]

Bien entendu, on remarque que l'unité de l'énergie est la même que celle du travail, puisque le travail est une différence d'énergie. On a donc~:
\[\left[E_{m\acute ec}\right]=\left[E_{cin}\right]=\left[E_{pot}\right]=J\]

\section{Conservation de l'énergie\index{conservation@conservation!de l'énergie}}
\subsection{Introduction}
La notion de conservation\index{conservation@conservation} est fondamentale en physique. La première grandeur qui pourrait être conservée à laquelle
on pense est la masse. Malheureusement, on sait aujourd'hui qu'elle ne l'est pas. Par contre, l'énergie l'est. Nous allons voir dans ce chapitre ce que cela signifie en étudiant le cas de la conservation de l'énergie mécanique. Nous verrons que selon les cas, celle-ci peut aussi ne pas être conservée.

\subsection{Théorème de conservation de l'énergie mécanique\index{conservation@conservation!de l'énergie mécanique}}
L'idée est née de la situation suivante~: une masse tombe d'une certaine hauteur ; lorsqu'on la lâche celle-ci ne possède que de l'énergie potentielle ; en descendant, cette énergie diminue et en même temps, comme la vitesse augmente, son énergie cinétique augmente ; arrivée en bas, la masse n'a plus que de l'énergie cinétique. Tout s'est donc passé comme si l'énergie potentielle s'était transformée en énergie cinétique. Ainsi, on peut dire que l'énergie mécanique\index{energie@énergie!mécanique}, somme d'énergie potentielle et cinétique, est en fait restée constante tout au long de la chute.

Techniquement, on exprime cela de la manière suivante~:
\begin{center}
\fcolorbox[gray]{0}{0.90}{\fbox{\parbox[]{6.3cm}{
\begin{equation}\label{consenmec0}
E_{mec}=const.
\end{equation}
}}}
\end{center}

Ce qui signifie aussi~:
\begin{center}
\fcolorbox[gray]{0}{0.90}{\fbox{\parbox[]{7.3cm}{
\begin{align}\label{consenmec}
E_{mec\,2}=E_{mec\,1} & \Rightarrow\\
E_{mec\,2}-E_{mec\,1} &=0\\
\Delta E_{mec} &=0\\
 & ou\\
E_{cin\,2}+E_{pot\,2}-(E_{cin\,1}+E_{pot\,1}) &=0\\
E_{cin\,2}-E_{cin\,1}+E_{pot\,2}-E_{pot\,1} &=0\\
\Delta E_{cin}+\Delta E_{pot} &=0\\
\frac{1}{2} m v_{2}^{2}-\frac{1}{2} m v_{1}^{2}+m g h_{2}-m g h_{1} &=0
\end{align}
}}}
\end{center}

Toutes ces expressions sont équivalentes. Il est important de bien comprendre que celles-ci signifient que l'énergie mécanique reste la même au cours du temps.

Il est aussi important de dire que que cette loi n'est valable qu'en l'absence de frottements\index{frottement@frottement}. Nous reviendrons par la suite sur cette remarque.

\subsection{Exemples}
\begin{enumerate}
\item Un homme saute du plongeoir des 10 m. A quelle vitesse arrive-t-il dans l'eau ?\\
Solution~:\\
En l'absence de frottements, l'énergie mécanique est conservé. Avant de commencer, il est nécessaire de fixer le zéro\index{zero@zéro} de l'altitude~: on le choisi au niveau de l'eau. Ainsi, on peut évaluer l'énergie mécanique à 10 m et celle au niveau de l'eau. On a~:
\begin{align*}
E_{mec\,10m} &=E_{cin\,10m}+E_{pot\,10m}\\
&=\frac{1}{2}\cdot m\cdot0^{2}+m\cdot g\cdot10\\
&=100\cdot m\;(g\cong10\, m/s^{2})\\
&\\
E_{mec\, eau} &=E_{cin\, eau}+E_{pot\, eau}\\
&=\frac{1}{2}\cdot m\cdot v^{2}+m\cdot g\cdot0\\
&=\frac{1}{2}\cdot m\cdot v^{2}
\end{align*}

Ainsi, le théorème implique~:
\begin{align*}
E_{mec\, eau}-E_{mec\,10m} &=0\\
\frac{1}{2}\cdot m\cdot v^{2}-100\cdot m &=0\\
v &=\sqrt{200}\\
&=14\, m/s
\end{align*}

Pour une hauteur h quelconque, le même calcul mène à~:
\[v=\sqrt{2\cdot g\cdot h}\]

Remarque~:

Bien évidemment, on retrouve cette même expression en utilisant la cinématique. En effet, pour un MRUA, on a~:
\[v^{2}=v_{o}^{2}+2\cdot a\cdot d\]

Pour un objet lâché en chute libre, on a~: \(a=g\), \(d=h\) et \(v_{o}=0\, m/s^{2}\). Ainsi, on peut écrire~:
\[v^{2}=0+2\cdot g\cdot h\]

Ce qui mène à la relation trouvée précédemment.
\item Quelle est la hauteur atteinte par un objet qu'on lance verticalement avec une vitesse de 3 m/s ?\\
Solution~:

On place le zéro de l'axe au niveau du point de décollage et on l'oriente vers le haut. On peut ainsi déterminer l'énergie mécanique en ce point par~:
\[E_{mec\, bas}=\frac{1}{2}\cdot m\cdot v^{2}\]

Car l'énergie potentielle pour h = 0 est nulle.

D'autre part, au niveau le plus haut atteint par l'objet, sa vitesse étant nulle, l'énergie mécanique vaut~:
\[E_{mec\, haut}=m\cdot g\cdot h\]

La conservation de l'énergie\index{conservation@conservation!de l'énergie} implique alors~:
\begin{align*}
\frac{1}{2}\cdot m\cdot v^{2} &=m\cdot g\cdot h\Rightarrow\\
h &=\frac{v^{2}}{2\cdot g}\Rightarrow\\
h &=\frac{3^{2}}{2\cdot10}=0,45\, m
\end{align*}
\end{enumerate}

\section{Limite du théorème de conservation}
L'idée de conservation de l'énergie implique l'idée de récupérer l'énergie qu'on a donné. Ainsi, quand on augmente l'énergie potentielle d'une masse en la montant, on peut récupérer cette énergie en la laissant redescendre. La possibilité de récupérer l'énergie dépensée est en réalité une propriété de certaines forces dites conservatives\index{force@force!conservative}. Ce n'est que pour ce type de forces que l'on peut définir la notion d'énergie potentielle\index{energie@énergie!potentielle}. C'est le cas pour le poids\index{poids@poids}, qui est une force conservative, pour laquelle on peut définir une énergie potentielle par \(E_{pot}=m\cdot g\cdot h\). Or, toutes les forces ne sont pas conservatives. Pour celles qui ne le sont pas, on ne peut définir d'énergie potentielle. C'est le cas pour la force de frottement\index{frottement@frottement} par exemple, pour laquelle on ne peut définir d'énergie potentielle.

Ainsi, le théorème de conservation de l'énergie mécanique n'est valable qu'en présence de forces conservatives. Car, dans ce cas, toutes ces forces peuvent être représentées par une énergie potentielle et on peut écrire~:
\[\Delta E_{mec}=0\]

En réalité, en présence de forces non conservatives\index{force@force!non conservative}, on modifie le théorème de la manière suivante~:
\begin{center}
\fcolorbox[gray]{0}{0.90}{\fbox{\parbox[]{6.3cm}{
\begin{equation}\label{mecnc}
\Delta E_{mec}=A_{forces\, non\, conservatives}
\end{equation}
}}}
\end{center}

Reste à donner les conditions qui rendent une force conservative.

\section{Forces conservatives}\label{conservatives}
\subsection{Définition}
Une force est dite conservative\index{conservative@conservative} si et seulement si son travail\index{travail@travail} sur un parcours fermé est nul. Cela se traduit mathématiquement par~:
\begin{equation}\label{forceconservative}
\fbox{\(\displaystyle Force\, conservative\Leftrightarrow\oint\overrightarrow{F}\cdot\overrightarrow{dr}=0\)}
\end{equation}

Le rond sur l'intégrale signifie que le parcours est fermé.

Une autre manière de définir une force conservative\index{conservative@conservative} est de dire que son travail ne dépend pas du chemin\index{chemin@chemin} choisi pour passer d'un point \(A\) à un point \(B\). Autrement dit ce travail\index{travail@travail} ne dépend que des points \(A\) et \(B\).

\subsection{Exemples}
\subsubsection{Le poids}
Un excellent exemple de force conservative\index{force@force!conservative} est celui du poids\index{poids@poids}. Pour s'en rendre compte, calculons le travail de cette force sur un parcours fermé~: on monte une masse m sur une hauteur h, puis on la déplace horizontalement sur une distance d, on la redescend de h et on la ramène au départ (voir figure \ref{travailferme}).

\begin{figure}[ht]
\centering
\caption{Travail du poids\label{travailferme}}
\includegraphics{TravailFerme.eps}
\end{figure}

Le calcul du travail se fait alors de la manière suivante~:
\begin{align*}
A_{A-B} &=\oint\overrightarrow{F}\cdot\overrightarrow{dr}=\int_{A}^{A}\overrightarrow{F}\cdot\overrightarrow{dr}\\
&=\int_{A}^{B}\overrightarrow{F}\cdot\overrightarrow{dr}+\int_{B}^{C}\overrightarrow{F}\cdot\overrightarrow{dr}+\\
&\;\;\int_{C}^{D}\overrightarrow{F}\cdot\overrightarrow{dr}+\int_{D}^{A}\overrightarrow{F}\cdot\overrightarrow{dr}\\
&=-mg\cdot d_{AB}+0+mg\cdot d_{CD}+0\\
&=0
\end{align*}

car, sur le segment \(AB\) le poids\index{poids@poids} est parallèle, mais de sens opposé, au déplacement, ce qui introduit un signe négatif (sin(\ang{180}) = -1), sur le segment \(BC\) le poids est perpendiculaire au déplacement, ce qui annule le travail (sin(\SI{90}{\degree}) = 0), sur le segment \(CD\) le poids est parallèle et de même sens que le déplacement et celui-ci est identique en grandeur à celui du segment \(AB\) et sur le segment \(DA\) le poids est perpendiculaire au déplacement ce qui annule aussi le travail. Ainsi, le travail total est nul et la force est bien conservative\index{conservative@conservative}.

On peut aussi voir cela en calculant le travail\index{travail@travail} du poids pour passer d'un point \(A\) à un point \(B\)~:
\begin{align*}
A_{A-B} &=\int_{A}^{B}\overrightarrow{F}\cdot\overrightarrow{dr}=\int_{A}^{B}\overrightarrow{mg}\cdot\overrightarrow{dr}\\
&=\overrightarrow{mg}\cdot\int_{A}^{B}\overrightarrow{dr}=\overrightarrow{mg}\cdot\overrightarrow{AB}\\
&=\overrightarrow{mg}\cdot(\overrightarrow{B}-\overrightarrow{A})=\overrightarrow{mg}\cdot\overrightarrow{B}-\overrightarrow{mg}\cdot\overrightarrow{A}
\end{align*}

Ainsi, on constate que le travail\index{travail@travail} ne dépend que des points \(A\) et \(B\). La force est donc conservative\index{conservative@conservative}.

\subsubsection{Le ressort}
Un autre exemple intéressant de force conservative\index{force@force!conservative} est celui de la force du ressort dans la limite élastique. Rappelons la forme de cette force~:
\[F_{res}=-k\cdot x\]
où x est l'élongation du ressort par rapport à sa position de repos et où le signe négatif signifie qu'on a une force de rappel.

\smallskip
Pour montrer que cette force est conservative, il suffit de calculer le travail de cette force sur un parcours fermé, soit d'un point A à un point B suivi d'un retour en A. Cela peut s'écrire~:
\begin{align*}
A&=\oint -k\cdot x\cdot dx\\
&=\int_{A}^{B} -k\cdot x\cdot dx+\int_{B}^{A} -k\cdot x\cdot dx\\
&=\int_{A}^{B} -k\cdot x\cdot dx-\int_{A}^{B} -k\cdot x\cdot dx\\
&=0
\end{align*}
Naturellement, en raison des propriétés de l'intégrale, la démonstration est évidente.

\medskip
Comme la force du ressort est conservative, elle dérive d'un potentiel. Déterminons-en l'énergie en calculant plus précisément le travail de A à B~:
\begin{align*}
A&=\int_{A}^{B} -k\cdot x\cdot dx\\
&=-k\cdot \int_{A}^{B} x\cdot dx=-k\cdot\frac{1}{2}\cdot x^2|_{A}^{B}\\
&=-\frac{1}{2}\cdot k\cdot x^2|_{A}^{B}=\frac{1}{2}\cdot k\cdot (x_{A}^2-x_{B}^2)\\
&\textbf{ou~:}\\
&=-\frac{1}{2}\cdot k\cdot x_{B}^2-(-\frac{1}{2}\cdot k\cdot x_{A}^2)\\
&=-(\frac{1}{2}\cdot k\cdot x_{B}^2-\frac{1}{2}\cdot k\cdot x_{A}^2)
\end{align*}

On voit clairement que le travail ne dépend que des points A et B et que l'énergie potentielle en ces points prend la forme~:
\begin{equation}\label{enpotres}
\fbox{\(\displaystyle E_{pot\,res}=\frac{1}{2}\cdot k\cdot x^2\)}
\end{equation}
Ce qui permet de ré-écrire le travail en fonction de l'énergie potentielle~:
\begin{align*}
A&=-(\frac{1}{2}\cdot k\cdot x_{B}^2-\frac{1}{2}\cdot k\cdot x_{A}^2)\\
&=-(E_{pot\,res}(B)-E_{pot\,res}(A))\\
&=-\Delta E_{pot\,res}
\end{align*}
\subsection{Généralisation}
La dernière relation met en évidence une relation fondamentale entre une force conservative et son énergie potentielle. En effet, on peut écrire par définition du travail~:
\[A=F\cdot\Delta x=-\Delta E_{pot}\;\Rightarrow\;F=-\frac{\Delta E_{pot}}{\Delta x}\]
Ce qui se traduit en langage infinitésimal par~:
\begin{center}
\fcolorbox[gray]{0}{0.90}{\fbox{\parbox[]{6.3cm}{
\begin{equation}\label{fderiveenpot}
F=-\frac{d}{dx}E_{pot}
\end{equation}
}}}
\end{center}
On dira donc que la force dérive au signe près de l'énergie potentielle. Attention, la dérivée porte sur la variable d'espace et non le temps.

\smallskip
On peut aussi écrire inversement~:
\begin{equation}\label{fintenpot}
\Delta E_{pot}=-A=-\int_{A}^{B} F\cdot dx
\end{equation}
ou même, vectoriellement~:
\begin{center}
\fcolorbox[gray]{0}{0.90}{\fbox{\parbox[]{6.3cm}{
\begin{equation}\label{enpotvsf}
\Delta E_{pot}=-A=-\int_{A}^{B} \overrightarrow{F}\cdot \overrightarrow{ds}
\end{equation}
}}}
\end{center}
Cette équation permettant de déterminer par intégration l'énergie potentielle liée à un expression algébrique quelconque de la force, on peut l'utiliser pour la force issue de la loi de la gravitation universelle de Newton.

\subsubsection{Énergie gravifique}
Considérons donc l'expression de la force de gravitation~:
\begin{equation}\label{forcedegravitation}
F=G\cdot\frac{M\cdot m}{r^2}
\end{equation}
Il s'agit d'une force centrale, c'est-à-dire qui pointe toujours vers un point central. Le travail, comme projection du vecteur déplacement infinitésimal \(\Delta\overrightarrow{r}\) sur le vecteur force \(\overrightarrow{F}\), peut donc être étudié selon une droite radiale passant pas le point central. Mathématiquement, on a donc~:
\begin{align*}
A&=\int_{A}^{B} \overrightarrow{F}\cdot \overrightarrow{ds}=\int_{A}^{B} F\cdot dr\\
&=\int_{A}^{B} G\cdot\frac{M\cdot m}{r^2}\cdot dr=G\cdot M\cdot m\cdot \int_{A}^{B} \frac{dr}{r^2}\\
&=G\cdot M\cdot m\cdot \int_{A}^{B} r^{-2}\cdot dr\\
&=G\cdot M\cdot m\cdot (-r^{-1})|_{A}^{B}\\
&=-G\cdot M\cdot m\cdot (\frac{1}{r_{B}}-\frac{1}{r_{A}})\\
&=-(G\cdot \frac{M\cdot m}{r_{B}}-G\cdot \frac{M\cdot m}{r_{A}})
\end{align*}
Ce qui permet comme précédemment de ré-écrire le travail en fonction de l'énergie potentielle~:
\begin{align*}
A&=-(G\cdot \frac{M\cdot m}{r_{B}}-G\cdot \frac{M\cdot m}{r_{A}})\\
&=-(E_{pot\,grav}(B)-E_{pot\,grav}(A))\\
&=-\Delta E_{pot\,grav}
\end{align*}
On voit clairement que le travail ne dépend que des points A et B. Mais, contrairement à la forme de l'énergie potentielle issue du poids ou celle correspondant à la force d'un ressort, pour lesquelles le zéro de l'énergie correspond à l'origine de l'axe, on voit ici qu'à une distance nulle l'énergie devient infinie.

\smallskip
Se pose donc le problème du choix du zéro de l'énergie\index{zero@zéro!de l'énergie potentielle} potentielle. Pour les deux forces évoquées ci-dessus, il semble évident pour la première qu'en remontant le champ de gravitation, l'énergie potentielle augmente et pour la seconde qu'elle le fait en s'éloignant de la position de repos du ressort.

Pour la force de gravitation, la forme de l'énergie potentielle implique qu'elle ne peut être nulle qu'à l'infini. Pour que celle-ci augmente en fonction de la distance, elle ne peut être positive. On doit donc choisir la forme suivante~:
\begin{equation}\label{enpotgrav}
\fbox{\(\displaystyle E_{pot\,grav}=-G\cdot \frac{M\cdot m}{r}\)}
\end{equation}
Ainsi, l'énergie potentielle augmente, c'est-à-dire devient de moins en moins négative, en s'éloignant de l'origine et s'annule à l'infini.

\medskip
Considérons la force de gravitation donnée par l'équation \ref{forcedegravitation} et l'énergie potentielle correspondante donne par l'équation \ref{enpotgrav} d'un point de vue graphique. La force de gravitation a été affectée d'un signe négatif pour signifier qu'elle est attractive et s'exerce dans le sens contraire de l'axe. Elle est inversement proportionnelle au carré de la distance, alors que l'énergie potentielle est elle inversement proportionnelle à la distance. Le graphe \ref{forceetenpotST} résente la force et l'énergie potentielle de la Terre dans le champ de gravitation du Soleil.

\begin{figure}
\centering
\caption{Force et énergie potentielle de\\gravitation entre la Terre et le Soleil}\label{forceetenpotST}
\begin{gnuplot}[terminal=latex,terminaloptions=rotate,scale=0.6]
		# domaine de définition
        #set xrange [0:1E11]
        #set yrange [-1E40:1E40]
        set xrange [0:5]
        set yrange [-1E46:1E45]
        # flèches
        #set arrow 1 from 39.7,120 to 39.7,0 head filled
        #size screen 0.5,30
        #set label "Écart type" at 39.4,100 rotate by 90
        #set arrow 2 from 44.1,120 to 44.1,0 head filled
        #set label "Écart type" at 44.4,138 rotate by -90
        #set arrow 3 from 41.9,120 to 41.9,0 head filled
        #set label "Moyenne" at 42.2,80 rotate by -90
        #set grid
        #set title "Baguettes d'une année"
        # suppression de la légende
        #set key off
        set key right bottom
        # légendes des axes
        set xlabel "Distance (m)"
        #set ylabel "Gravitation" rotate by 90
        # largeur des colonnes (boxes)
        #set boxwidth 0.1
        #set arrow from graph 1,0 to graph 1.05,0 size screen 0.025,15,60 filled ls 11
		#set arrow from graph 0,1 to graph 0,1.05 size screen 0.025,15,60 filled ls 11
		set xzeroaxis
		G=6.67E-11
		M=1.99E30
		m=5.97E24
		F(x)=-G*M*m/x**2
		E(x)=-G*M*m/x   
        # tracé du graphe
        plot E(x) title "Énergie potentielle (J)", F(x) title "Force de gravitation (N)"
\end{gnuplot}
\end{figure}

L'étude des propriété de la fonction d'énergie potentielle sera un élément très important pour comprendre les mouvements des objets soumis à différentes force.

Plus généralement, nous reconsidérerons ce problème au chapitre \ref{physiquetheorique}.