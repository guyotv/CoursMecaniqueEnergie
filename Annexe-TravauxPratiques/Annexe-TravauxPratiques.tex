\myclearpage

\chapter{Travaux pratiques}
\setlength{\fboxsep}{7pt}	% met 7pts entre le texte et le cadre (cela aère un peu car sinon il est collé au texte)
\fancypage{\fbox}{}		% commence l'encadrement du plan d'un rapport de TP

\section{Le rapport de laboratoire}
\lettrine{T}{out d'abord}, un avertissement.\index{rapport!de laboratoire} La ``méthode'' de construction d'un rapport présentée ci-dessous est propre à celle d'une discipline scientifique. Elle constitue une manière de faire qui est actuellement reconnue. Pourtant l'histoire montre que la présentation des résultats a souvent adopté des formes très différentes. On doit donc plus la comprendre comme le rappel d'un nécessaire soucis de rigueur que comme une exigence formelle. De plus, si elle est un outil particulièrement bien adapté à la présentation de cette forme de savoir qu'est la science, elle ne l'est certainement pas pour d'autres formes de connaissances. En effet~:

\begin{quotation}
``\textit{La conception et la mise au point d'instruments spécifiques, toujours plus diversifiés, est l'une des marques du progrès technique. Comment croire que le progrès intellectuel puisse suivre un chemin inverse ? S'il existe aujourd'hui des dizaines de types de tournevis, de scies, de rabots, peut-on sérieusement imaginer que la pensée, elle, puisse se contenter d'une aussi pauvre panoplie que celle empruntée aux seules sciences dites exactes ?}''

\footnotesize{\cite[p. 13-14]{JL96}}
\end{quotation}

L'objectif premier d'un rapport est de réaliser une trace du travail effectué. C'est d'abord un souvenir destiné à l'expérimentateur.

Le second objectif est de permettre à d'autres personnes de s'intéresser au travail réalisé.

Ces deux objectifs ne peuvent être réalisés sans qu'une attention particulière ne soit portée sur la clarté\index{clarte@clarté} et la concision\index{concision}.

Les détails de la réalisation d'un rapport peuvent varier beaucoup en fonction des spécificités de l'expérience et de la personnalité de l'expérimentateur. Cependant, si une certaine liberté en ce domaine devrait être toujours possible, pour ne pas oublier de traiter des points importants et pour faciliter la lecture de personnes qui s'attendent à trouver certaines formes définies de rapport, il est recommandé de suivre une structure de rapport-type\index{structure!de rapport} déterminée par l'expérience.

Le rapport à présenter sur une expérience terminée est aussi important que l'expérience elle-même. Il doit être ordonné, exprimé dans un langage simple et concis, et ne comporter que les faits importants de l'expérience. Ce genre d'exercice se retrouve dans toutes les disciplines. La qualité d'un procès-verbal ou d'un rapport peut être déterminante pour des décisions importantes ou pour l'avenir de leur auteur.

Pensez, lorsque vous l'écrivez, que vous vous adressez à un lecteur du même niveau que vous, par exemple un de vos camarades qui était absent le jour de l'expérience, et imaginez qu'il doit prendre connaissance et comprendre l'essentiel de votre travail de laboratoire.

Lorsque deux élèves font une expérience, ils présentent un seul rapport et obtiennent ainsi une même note de laboratoire. Il s'agit d'un travail de groupe. Il est important que chaque élève participe à l'élaboration de chaque rapport en collaboration avec son collègue.

\subsection{Plan d'un rapport de travail pratique}

En règle générale, pour oublier le moins de choses importantes possibles, un rapport\index{rapport!de travail pratique} est structuré de la manière qui suit.

D'un point de vue typographique, il faut relever que les titres de sections ne doivent pas être soulignés, ni ponctués (ni par un point, ni par un double point). Ils sont simplement mis en évidence par une taille de caractère légèrement supérieure à celle du corps de base.

\subsubsection{Préliminaires}

Titre\index{titre} de l'expérience, noms des auteurs, classe et date. Le titre doit être explicite, c'est-à-dire qu'il doit donner une première idée du sujet abordé et non se limiter à dire qu'il s'agit de physique, comme par exemple~: ``Laboratoire de physique''. Il ne doit pas y avoir d'abréviations, ni d'acronymes.

\subsubsection{Résumé\index{resume@résumé}}

Relevons la nécessité de mettre juste après le titre\index{titre}, pour des rapports long (plus de deux pages), un résumé\index{resume@résumé} (aussi appelé en anglais ``abstract\index{abstract}'') complet de l'expérience.

Maximum cinq lignes !

Il s'agit d'une synthèse de tout le rapport. Un lecteur pressé devrait, en ne lisant que le résumé, se faire une bonne idée du contenu du document entier. Il faut donc, grâce au résumé, pouvoir se faire une idée du but, de la méthode, des résultats, de leur qualité et des problèmes rencontrés.

Bien que placé juste après le titre, il est bien évident que ce résumé ne peut se faire qu'en dernier lieu.

Ce résumé est difficile à réaliser mais est très important car il permet, pour un lecteur extérieur, comme pour l'expérimentateur, quelques années après la réalisation de l'expérience, de se rendre compte très rapidement (sans devoir relire l'ensemble du rapport) de l'intérêt du travail réalisé et donc de la nécessité éventuelle d'entamer la lecture du rapport lui-même.

\subsubsection{But\index{but du travail pratique}}

C'est l'indication de l'objectif du travail de laboratoire. Une ou deux lignes.

\subsubsection{Théorie\index{Partie théorique}}

Cela peut être l'énoncé d'une relation traitant du but de l'expérience et à confirmer. Dans ce cas, il n'est pas nécessaire d'en présenter tout le développement. Seule la relation et les éventuelles hypothèses sur lesquelles elle se base, avec une légende des termes utilisés, doivent être reportées.

En l'absence d'une relation théorique établie, cela peut aussi être un raisonnement, à confirmer expérimentalement, donnant une indication sur le résultat probable ou logique de l'expérience.

On peut avoir recours à une annexe\index{annexe} (placée en fin de rapport) pour développer des points théoriques non essentiels mais méritant de retenir notre attention.

\subsubsection{Description de l'expérience\index{description de l'experience@description de l'expérience}}

Il s'agit de décrire la méthode expérimentale sans trop entrer dans les détails. Il ne faut pas faire une liste des manipulations du type ``mode d'emploi'', ne pas écrire une procédure, mais rester au niveau de la méthode, de manière à ce qu'on comprenne le pourquoi de la réalisation expérimentale sans trop entrer dans les détails du comment. Un schéma du dispositif expérimental avec un titre et une légende complétera la description. Un exemple est donné à la figure \ref{railhorizontal}.

\begin{figure}[t]
\centering
\caption[Rail horizontal]{Rail horizontal\label{railhorizontal} \par \scriptsize{(Notez le titre et la référence dans le texte qui permet au
schéma de ne pas se trouver directement sous le texte.)}}
\includegraphics[width=7cm]{RailHorizontal.eps}
\end{figure}

\subsubsection{Résultats\index{resultats@résultats}}

Les résultats doivent impérativement figurer dans le corps du rapport. Selon les cas, ils peuvent être constitués de mesures brutes ou issus du calcul de grandeurs dérivées. Ils en constituent, avec la discussion (voir plus loin) l'une des deux parties principales et doivent, à ce titre, obligatoirement être présents dans le corps du rapport. Cependant, ils peuvent être présentés sous différents formes~:

\begin{enumerate}
\item \underbar{Graphiques des résultats}

C'est la présentation la plus concise possible des résultats obtenus. Elle est bien plus lisible que les tableaux et c'est pourquoi on doit la privilégier. Il faut cependant faire attention à présenter les grandeurs, brutes (mesures) ou dérivées (issues d'un calcul), qui représentent au mieux les résultats en fonction des buts choisis.

Tout graphique\index{graphique} doit avoir un titre, des axes portant clairement les symboles des grandeurs concernées et leurs unités. On n'indique pas les coordonnées des points sur les graduations, mais des valeurs d'accroissement régulier. Les points expérimentaux ne sont jamais reliés (ce qui pourrait faire croire que des mesures ont été faites entre les mesures principales), mais on peut faire passer la meilleure courbe possible à travers ceux-ci. On indique les barres d'incertitudes\index{incertitude@incertitude} de chaque axe aux points où les calculs ont été faits.

Un exemple est présenté à la figure \ref{explegraphe}.

\begin{figure}[ht]
\centering
\caption{Chute libre\label{explegraphe}}
\includegraphics{ExpleGraphe.eps}
\end{figure}

\item \underbar{Tableau des mesures}

Les mesures effectuées au laboratoire, lorsqu'elles sont peu nombreuses, peuvent être mises dans un ou plusieurs tableaux\index{tableau} ayant la forme présentée dans le tableau \ref{tableau}. La première ligne présente les grandeurs physiques et leurs symboles. La seconde donne les unités des grandeurs et la troisième les incertitudes absolues\index{incertitude@incertitude} des mesures directes (non calculées à partir d'autres grandeurs). Enfin, les suivantes présentent les résultats. Attention, présenter trop de résultats simultanément nuit à la lisibilité d'un tableau. Par ailleurs, une bonne lecture d'un tableau passe par sa simplification~: pas de colonne de valeurs identiques qu'on peut présenter une seule fois avant le tableau, pas de colonnes exagérément large, et donc de vide inutile, en raison d'un titre trop long, pas de colonnes de calculs intermédiaires sans raison aucune, etc.

\begin{table}[ht]
\centering
\caption{Tableau de mesures}
\label{tableau}
\begin{tabular}{|c|c|c|}
\hline 
\textbf{temps t} & \textbf{position x} & \textbf{vitesse v}\\
\hline 
s & cm & cm/s\\
\hline 
0,1 & 0,10 & \\
\hline
\hline 
\(\begin{array}{c}
2,0\\
2,4\\
2,6\end{array}\)&
\(\begin{array}{c}
31,6\\
17,8\\
12,4\end{array}\)&
\(\begin{array}{c}
1,8\\
3,2\\
4,8\end{array}\)\\
\hline
\end{tabular}
\end{table}

Si les mesures sont trop nombreuses, pour améliorer la lisibilité du rapport, on peut les reporter en annexe\index{annexe}. Encore une fois, il faut alors impérativement présenter les résultats principaux, significatifs ou importants qui constituent, à proprement parlé, les résultats de l'expérience. Par exemple, dans la recherche d'une accélération à partir de mesures d'une distance parcourue et d'un temps, on peut reporter en annexe les nombreuses mesures réalisées, mais pas les valeurs d'accélération qui en découlent et constituent les résultats de l'expérience.

Par ailleurs, il faut ici insister sur ce qui est nécessaire pour permettre la comparaison entre théorie et expérience. Une notion simple permet de quantifier la différence entre une mesure, notée \(valeur_{exp}\), et une valeur théorique, notée \(valeur_{th}\), celle d'écart\index{ecart@écart} ou d'erreur\index{erreur}~:
\begin{equation}\label{defecart}
\fbox{\(\displaystyle e(\%)=\frac{valeur_{th}-valeur_{exp}}{valeur_{th}}\cdot100\)}
\end{equation}

Lorsqu'une valeur théorique est très précisément connue, on parle d'erreur\index{erreur} à la valeur théorique plutôt que d'écart.
Relevons enfin la nécessité de maintenir le signe du résultat pour détecter une éventuelle erreur systématique\index{erreur!systématique}.
\medskip

\item \underbar{Exemples de calcul}

Lorsque des résultats sont obtenus à partir d'opérations sur des mesures directes, on donne un ou deux exemples des calculs\index{exemple de calcul} effectués en précisant bien leur position dans le tableau d'où elles ont été extraites.

\item \underbar{Calcul des incertitudes}

Les calculs des incertitudes\index{incertitude@incertitude} des grandeurs indirectement obtenues (par calcul) sont présentés dans un ou deux cas en précisant bien lesquels dans le tableau.

A défaut, quelques mesures permettant de se faire une idée des incertitudes doivent être présentées et brièvement commentées pour évaluer la qualité de l'expérience.
\end{enumerate}

\subsubsection{Discussion}

Il s'agit d'une analyse du travail. S'il n'existe pas de méthode permettant de faire une bonne discussion\index{discussion}, on peut relever que souvent les points suivants apparaissent.
\begin{enumerate}
\item Une explication en français des résultats présentés dans les graphes. Un graphe, s'il est plus lisible qu'un tableau, ne suffit pas. Des explications claires améliorent la compréhension.
\item On compare les résultats avec les prévisions théoriques. On met en évidence leur signification. On discute leur qualité. On insiste sur les mesures s'écartant des prévisions, on tente de les expliquer logiquement ou on présente d'autres mesures de confirmation. On remet en cause les mesures autant que la théorie si nécessaire.
\item On remet en question la méthode et on propose des améliorations.
\end{enumerate}

\emph{C'est la partie la plus dense et la plus importante du rapport.}

\subsubsection{Conclusion\index{conclusion du travail pratique}}

Il s'agit de dire très brièvement si le but a été atteint et de juger s'il l'a été bien, partiellement ou mal.

\subsubsection{Annexes}

Tout ce qui nuit à la lisibilité du rapport, mais a une certaine importance à vos yeux, doit être mis en annexe. Chacune de celles-ci doit comporter un titre relatif à son contenu.

\clearpage

\fancypage{}{}		% termine l'encadrement du plan de rapport de TP

\section{La nébuleuse du Crabe\index{nebuleuse@nébuleuse!du crabe}}

\subsection{Introduction}

La nébuleuse du Crabe est le reste de l'explosion d'une étoile arrivée en fin d'évolution~: les couches extérieures de l'étoile ont formé la nébuleuse actuelle, alors que le noyau s'est contracté brutalement, pour former une étoile à neutrons\index{etoile@étoile!à neutrons} qui rayonne ce qui lui reste d'énergie thermique. Cette étoile est un pulsar\index{pulsar} de très courte période (33 ms).

\subsection{But du travail pratique}

Deux photographies prises à plusieurs années d'intervalle montrent que la nébuleuse est en expansion\index{expansion}, ce qui permet d'estimer son âge en supposant que toute la matière était condensée dans l'étoile centrale.

\subsection{Dispositif expérimental}

Établir l'échelle, en secondes d'arc par millimètre, de chacune des photographies, sachant que les deux étoiles repérées par des flèches sont distantes de 576''.

\subsection{Mesures}

Identifier le pulsar sur les photographies (des deux étoiles centrales, c'est celle qui est au sud-ouest) et mesurer successivement les distances d'une douzaine de filaments\index{filaments} par rapport à lui. Choisir des filaments bien répartis sur la périphérie de la nébuleuse.

\subsection{Résultats}

Faire un tableau où figureront pour chaque filament~: la distance \(x_1\) en \si{\arcsecond} de la première photographie, la distance \(x_2\) en \si{\arcsecond}
de la seconde photo, et le mouvement propre
\[v=\frac{\Delta x}{\Delta t}\; \text{ où }\;\Delta x=x_{2}-x_{1}\; et\;\Delta t=\SI{34,1}{\year}\]

A titre de contrôle, mesurer de la même manière la distance de quelques étoiles au pulsar, sur chaque photographie.

Porter sur un graphique \(v\) en fonction de \(x_2\).

\subsection{Analyse}

Comment obtient-on l'âge de la nébuleuse ?

Des astronomes chinois du haut Moyen Âge ont signalé l'apparition, en l'année 1054, d'une étoile nouvelle\index{etoile@étoile!nouvelle} (qui fut visible en plein jour pendant trois semaines!) dans la direction de la nébuleuse actuelle. La date est-elle compatible avec l'âge que vous avez trouvé ?

\section{Le pendule\index{pendule} simple}

\subsection{Les mesures}

Une partie très importante du travail du physicien est de déterminer la (ou les) grandeur\index{grandeur} pertinente pour décrire le phénomène étudié.

Ici, on s'intéresse à la période\index{periode@période} du pendule, c'est-à-dire au temps qu'il met pour faire un aller-retour. Il s'agit de déterminer quelles variables\index{variable} (quels paramètres) pourraient influer cette grandeur. On peut citer entre autres la masse et la longueur du pendule, sa position initiale (l'angle du fil par rapport à la verticale), son poids, le fluide dans lequel il se trouve, etc. Tous ces paramètres n'ont pas forcément de liens avec la grandeur choisie pour décrire le phénomène. Dans un premier temps, on peut donc en éliminer certains qui paraissent en première approximation n'avoir aucun rôle, en raison des difficultés pour les mesurer, des impossibilités matérielles pour les déterminer ou du coût qu'ils engendrent. Bien entendu, il faut tenter de minimiser l'influence de paramètres que l'on ne pourrait prendre en considération pour diverses raisons tout en les sachant importants.

D'autre part, pratiquement, il est indispensable de réaliser l'expérience en ne faisant varier qu'un seul paramètre\index{parametre@paramètre} à la fois. Dans le cas présent, comme seules les variables masse, longueur et angle initial ont été choisies, il faut réaliser trois séries de mesures~:

\begin{itemize}
\item la masse et la longueur restent constantes et on ne fait varier que l'angle,
\item la masse et l'angle restent constants et on ne fait varier que la longueur,
\item la longueur et l'angle restent constants et on ne fait varier que la masse.
\end{itemize}
Bien entendu, cette procédure a une répercussion sur l'organisation des données dont nous verrons quelques éléments par la suite.

Finalement, il faut relever deux choses. Premièrement, la nécessité d'évaluer l'incertitude\index{incertitude@incertitude} des mesures. On peut procéder en effectuant une série de mesures avec strictement les mêmes paramètres. On prend ensuite le plus grand écart. Ce n'est pas très fiable mais constitue une bonne approximation. Secondement, il est important de relever le moindre petit problème survenu pendant les mesures. Un petit changement des conditions d'expérience pouvant avoir des répercussions non négligeables, il faut littéralement faire attention à tout.

\subsection{Organisation des données et graphiques}

L'objectif est avant tout la clarté\index{clarte@clarté}. L'organisation des données repose sur une grandeur (la période d'oscillation T) et trois variables (la masse m, la longueur L et l'angle initial \(\alpha\)). Il est fondamental d'étudier chacune de ces trois variables indépendamment. Pour cela on fixe une valeur pour les deux autres (en général la plus grande possible pour limiter les incertitudes, bien que pour l'angle initial il ne faudrait pas dépasser \SI{15}{\degree} pour que la théorie classique \(T\approx\sqrt{L}\) soit valable) et on ne fait varier que celle qui est choisie. Ainsi, dans le cas du pendule, on est amené à construire trois tableaux~: \(T(m)\), \(T(L)\) et \(T(\alpha)\). Pour des raisons de clarté, on ne répète pas pour chaque mesure la valeur des variables fixées.

En ce qui concerne les graphes, comme la variable change pour chaque expérience, il faut aussi construire trois graphes qui correspondent aux trois tableaux précédents. On ne représente sur ceux-ci que les points effectivement mesurés. On ne relie donc jamais les points. Finalement, il ne faut pas oublier le titre, la date, le nom des grandeurs et les unités obligatoirement présents sur chaque graphe.

\section{Mouvement simple\index{mouvement!simple}~: MRU\index{MRU}}

Il s'agit de tracer le graphe horaire de la position pour un mobile se déplaçant sur un rail avec différentes vitesses initiales et avec peu de frottements.

\subsection{Les mesures}

Les mesures sont celles du temps de parcours sur une distance donnée. Elles se réalisent avec deux cellules photoélectriques et un chronomètre. Il est important de soigner la réalisation~: horizontalité du rail, détermination précise des longueurs, etc. Il faut aussi évaluer l'incertitude des mesures en répétant quelques mesures plusieurs fois.

\subsection{Organisation des données et graphiques}

C'est l'occasion de réaliser un tableau avec une seule variable~: la distance parcourue et avec plusieurs grandeurs~: le temps mis pour parcourir la distance avec diverses poussées~: forte, moyenne et faible. Le fait que la variable soit commune à toutes les grandeurs (qui toutes représentent un temps) permet de ne réaliser qu'un seul graphique avec plusieurs courbes. Cela permet de mieux comparer les courbes et de montrer très clairement que plus la vitesse est grande plus la pente de la courbe est forte.

\subsection{Analyse des résultats}

Deux points sont essentiellement à relever~:
\begin{itemize}
\item la pente du graphe horaire\index{graphe horaire} de la position représente la vitesse du mobile et le point où le graphe coupe l'axe de la position correspond à la position du mobile au moment où on enclenche le chronomètre et
\item lorsque le frottement est important le graphe n'est pas linéaire (ou affine), mais sa pente diminue progressivement, indiquant une diminution de vitesse.
\end{itemize}

\section{Mouvement simple\index{mouvement!simple}~:\\MRUA\index{MRUA}}

\subsection{But}

Il s'agit de tracer le graphe horaire de la position d'un mobile descendant un rail incliné\index{rail incline@rail incliné} selon différentes pentes et avec peu de frottements.

Il s'agit aussi de trouver deux théories permettant de calculer l'accélération du mobile et de les comparer à une accélération expérimentale obtenue à partir des mesures.

\subsection{Théorie}

Pour déterminer l'accélération d'un mobile sur un plan incliné, on peut suivre deux raisonnements.
\begin{enumerate}
\item On peut établir une simple relation linéaire entre l'angle du plan et l'accélération. En effet, sachant que l'accélération d'un objet en chute libre vaut \(g=\SI{9,81}{\metre\per\second\squared}\), on peut faire les correspondances suivantes~: \(\SI{0}{\degree}\;\Rightarrow\;\SI{0}{\metre\per\second\squared}\) et \(\SI{90}{\degree}\;\Rightarrow\;\SI{9,81}{\metre\per\second\squared}\)

Cela mène à la relation suivante~: \[a=g\cdot \alpha/90\]
\item On peut considérer que l'accélération du mobile qui descend le long du rail incliné est la composante le long de ce plan du vecteur correspondant à l'accélération du mobile en chute libre. Il s'agit donc de projeter le vecteur \(\overrightarrow g\) de norme \(g=\SI{9,81}{\metre\per\second\squared}\) perpendiculairement au plan incliné. La figure représentant cette projection est un triangle rectangle d'hypoténuse \(g\) et de côté adjacent \(a\) recherché. On obtient donc la relation suivante~: \[a=g\cdot \sin(\alpha)\].
\end{enumerate}

\subsection{Les mesures}

Les mesures sont celles du temps parcouru sur une distance donnée. Elles se réalisent avec deux cellules photoélectriques et un chronomètre. Pour obtenir l'accélération expérimentale, on se place dans le cas d'un MRUA de vitesse initiale nulle. Il est donc nécessaire de lâcher le mobile et de veiller à ne pas le lancer.

\subsection{Organisation des données et graphiques}

On réalise un tableau avec une variable et deux grandeurs~: la distance parcourue, le temps correspondant et l'accélération expérimentale. On peut alors comparer par un écart la moyenne des accélérations expérimentales, obtenues grâce à l'hypothèse d'un MRUA qui nous permet d'écrire \(a_{exp}=2\cdot d/t^2\), pour chaque pente et chaque valeur d'accélération obtenue théoriquement.

La comparaison montre clairement que la dépendance sinusoïdale est réalisée.

\subsection{Galilée\index{Galilee@Galilée} et le plan incliné\index{plan incline@plan incliné}}

C'est un mouvement chargé d'histoire puisque Galilée l'étudia de manière approfondie pour en déduire que la distance parcourue par un objet en MRUA est proportionnelle au temps de parcours au carré.

L'idée était simple. Le mouvement de chute libre étant trop rapide pour pouvoir être aisément étudié, il faut le ralentir par un plan incliné. Cela amena Galilée à découvrir la relation entre la distance parcourue et le temps mis pour le faire par un mobile en mouvement rectiligne uniformément accéléré et plus particulièrement par un mobile en chute libre.

\section{La chute libre}

\subsection{Cette expérience donnant lieu à un rapport noté, elle n'est pas décrite.}

\subsection{Résultats}

Trois résultats importants concluent cette expérience~:

\begin{itemize}
\item l'accélération d'un objet en chute libre\index{chute libre} est constante (c'est
un MRUA\index{MRUA}),
\item cette accélération vaut \(a=g=\SI{9,81}{\metre\per\second\squared}\),
\item cette accélération est indépendante de la masse de l'objet.
\end{itemize}

\section{Le canon horizontal}

C'est une expérience dont le but est très simple. Avec un petit canon\index{canon} horizontal à ressort, il s'agit de tirer une bille du haut d'une table. On doit auparavant déterminer par calcul le lieu exact d'impact au sol. C'est une application des lois de la balistique\index{balistique}. Au départ, on ne s'autorise que des tirs verticaux. Ainsi on peut déterminer la vitesse de la bille à la sortie du canon. En faisant l'hypothèse qu'elle ne change pas lors d'un tir horizontal, on peut alors déterminer le point d'impact au sol. En effet, à l'aide de la hauteur à laquelle se trouve le canon par rapport au sol, on peut déterminer le temps de chute vertical de la bille. On utilise pour cela l'équation horaire de la position d'un objet en chute libre de vitesse initiale (verticale) nulle. Puis, on peut déterminer la distance horizontale parcourue en considérant un même temps pour le vol parabolique et le mouvement horizontal à vitesse constante. Celle-ci correspondant à la vitesse de sortie du canon précédemment calculée. Dans le détail, on a~:
\begin{description}
\item[Tir vertical.] C'est un MRUA d'accélération terrestre, pour lequel on peut poser~:
\[v^2=v_o^2-2\cdot g\cdot h\;\Rightarrow\;v_o=\sqrt{2\cdot g\cdot h}\]
car la vitesse au sommet de la trajectoire est nulle.
\item[MRU et MRUA.] Verticalement, on a un MRUA d'équation~:
\[h=\frac{1}{2}\cdot g\cdot t^2\;\Rightarrow\;t=\sqrt{\frac{2\cdot h}{g}}\]
Et horizontalement, on a un MRU d'équation~:
\[d=v_o\cdot t\]
Soit, en combinant les deux~:
\[d=v_o\cdot\sqrt{\frac{2\cdot h}{g}}\]
\end{description}


Cette expérience peut aussi se faire à l'aide de l'énergie\index{energie@énergie}. En effet, pour déterminer la vitesse de la bille à la sortie du canon, on peut mesurer la hauteur maximale qu'elle va atteindre et poser que l'énergie cinétique à la sortie du canon se transforme intégralement en énergie potentielle. Ainsi, on peut poser~:
\begin{align*}
\frac{1}{2}\cdot m\cdot v_o^2&=m\cdot g\cdot h\;\;\Rightarrow\\
v_o&=\sqrt{2\cdot g\cdot h}
\end{align*}
Ce qui correspond à la vitesse qu'on peut trouver de manière cinématique.

\section{Le chariot à masse pendante}

L'idée est d'accélérer un chariot au moyen d'une masse suspendue. Il se déplace sur un rail horizontal avec peu de frottements. La masse pendante y est attachée à l'aide d'une petite ficelle. Une poulie permet de faire descendre la masse tout en tirant le chariot horizontalement. On fait varier la masse pendante pour obtenir différentes accélérations.
C'est une expérience portant sur la seconde loi de Newton. Elle est intéressante si on laisse l'expérimentateur construire sa propre théorie menant à l'accélération du système chariot-masse pendante. Il est alors possible de comparer une théorie construite de toute pièce (sur la base de la seconde loi de Newton\index{seconde!loi!de Newton}) avec les résultats expérimentaux. Ceux-ci sont obtenus à partir de l'hypothèse d'un MRUA\index{MRUA} à l'aide de l'équation de la position utilisée avec une vitesse initiale nulle. Une série de mesures de diverses distances parcourues en fonction du temps, permet de trouver l'accélération\index{accélération}.

Le résultat ne sera pas explicité ici puisqu'il permettrait de découvrir la bonne théorie, ce qui n'est pas l'objectif recherché.