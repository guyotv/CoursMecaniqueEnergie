\myclearpage

\chapter{Mécanique différentielle}
%\minitoc
\section{Introduction}
Newton,avec Leibnitz, est considéré comme l'un des deux premiers auteurs du calcul différentiel et intégral. La théorie physique qu'il a développée nécessitait en effet de nouveaux outils pour apréhender les relations entre position, vitesse et accélération. Nous allons développer dans ce chapitre cette approche particulière de la mécanique. Naturellement, elle nécessite des connaissances en mathématiques qui peuvent être assez avancées. Celles-ci seront supposées connues, car il est impossible d'en faire l'exposé dans ce cours.

\section{Cinématique}\label{cinemadiff}
Les rapports mathématiques entretenu entre la position, la vitesse et l'accélération ont été esquissés à travers les équations \ref{vitinstant} et \ref{accinstant} qui sont rappelées ci-dessous~:

\begin{equation}
\fbox{$\displaystyle \overrightarrow{v}=\lim_{\Delta t\rightarrow0}\frac{\Delta \overrightarrow{x}}{\Delta t}=\frac{d\overrightarrow{x}}{dt}$}
\end{equation}

\begin{equation}
\fbox{$\displaystyle \overrightarrow{a}=\lim_{\Delta t\rightarrow0}\frac{\Delta \overrightarrow{v}}{\Delta t}=\frac{d\overrightarrow{v}}{dt}$}
\end{equation}

Mathématiquement, ces deux limites représentent des dérivées. Plus précisément, on peut dire que la vitesse est la dérivée de la position en fonction du temps et que l'accélération est la dérivée de la vitesse en fonction du temps. Dans les deux cas, la variable par rapport à laquelle on dérive est le temps.\\
C'est une des raison de la notation $\frac{d}{dt}$ qui précise que la dérivée se fait par rapport au temps. Une autre notation existe. On la dit de Newton. Mais elle ne fait que sous entendre la dérivation par rapport au temps~: $\dot x$ et $\dot v$. Évidemment, si la vitesse est la dérivée de la position et que l'accélération est la dérivée de la vitesse, l'accélération est la seconde dérivée de la position par rapport au temps, ce qui s'exprime ainsi~:

\begin{equation}
v(t)=\frac{dx}{dt}\;\mbox{ et }\;a(t)=\frac{dv}{dt}\;\Rightarrow\;a(t)=\frac{d^2x}{dt^2}
\end{equation}

ou en notation de Newton~:

\begin{equation}
v(t)=\dot x\;\mbox{ et }\;a(t)=\dot v\;\Rightarrow\;a(t)=\ddot x \label{posvitaccdiff}
\end{equation}


D'autre part, autant on peut dériver pour passer de la position à l'accélération, autant on peut intégrer pour passer de l'accélération à la position. Le fait que l'intégration constitue l'opération inverse de la dérivée le justifie. Ainsi, on a~:
\begin{equation}
v(t)=\int a(t)dt+v_o\;\mbox{ et }\;x(t)=\int v(t)dt+x_o
\end{equation}

Ou $v_o$ et $x_o$ sont les constantes inévitables d'un intégration indéfinie et représentent la vitesse et la position initiales. Bien entendu, on peut intégrer deux fois l'accélération pour obtenir la position.\\
Il faut relever aussi la possibilité d'utiliser une intégrale définie~:

\begin{equation}
v(t)=\int_{t_o}^t a(t)dt\;\mbox{ et }\;x(t)=\int_{t_o}^t v(t)dt
\end{equation}

Le fait qu'on puisse calculer la position, respectivement la vitesse, en intégrant la vitesse, respectivement l'accélératrion, constitue une généralisation du porblème du calcul de celui-ci par l'aire sous le graphe horaire de la vitesse, respectivement de l'accélération. En effet, calculer l'intégrale définie de la vitesse, respectivement de l'accélération, n'est en fin de compte que calculer l'aire sous leurs graphes horaires entre deux instants $t$ et $t_o$ donnés. Cela constitue une des propriétés mathématiques de l'intégrale. La généralisation tient au fait que l'horaire de la vitesse ou de l'accélération peut être n'importe quelle fonction, alors que précédemment, nous avions dû nous limiter aux fonctions simples telles que les fonctions constantes, linéaires et affines.

\subsection{Exemples}

\subsubsection{Mouvement rectiligne uniformément accéléré~: MRUA}
Déterminons les équations de son mouvement.
On a~:
\[a(t)=a_o=constante\]
Ainsi, par intégration définie entre $t_o$ et $t$~:
\begin{align*}
v(t)&=\int_{t_o}^t a_o dt=a_0 \int_{t_o}^t dt=a_o\cdot t|_{t_o}^t=\\
&=a_o (t-t_o)=a_o t-a_o t_o=a_o t+v_o
\end{align*}
car, à $t=0$, on a que $v(t)=v_o$, et par conséquent $-a_o t_o=v_o$.
\medskip

Ainsi, on obtient bien~:
\[v(t)=a_o\cdot t + v_o\]

Ce qui donne pour la position~:
\begin{align*}
x(t)&=\int_{t_o}^t (a_o t+v_o)dt\\
&=a_o \int_{t_o}^t t\cdot dt+\int_{t_o}^t v_o\cdot dt=\\
&=\frac{1}{2}a_o t^2|_{t_o}^t+v_o t|_{t_o}^t=\\
&=\frac{1}{2}a_o (t^2-t_o^2)+v_o (t-t_o)=\\
&=\frac{1}{2}a_o t^2+v_o t-\frac{1}{2}a_o t_o^2-v_o t_o=\\
&=\frac{1}{2}a_o t^2+v_o t+x_o
\end{align*}
car, à $t=0$, on a que $x(t)=x_0$, et par conséquent $-\frac{1}{2}a_o t_o^2-v_o t_o=x_0$.
\medskip

Ainsi, on obtient bien~:
\[x(t)=\frac{1}{2}a_o t^2+v_o t+x_o\]

\subsubsection{Porté maximum en balistique}
Si le calcul différentiel est intimement lié aux notions de position, vitesse et accélération, il existe des cas où son utilisation relève simplement d'un problème mathématique. Du point de vue de la physique, ces cas sont moins intéressants. Cependant, ils sont assez courant pour qu'on en donne ici un exemple simple.\\
Il s'agit de trouver sous quel angle on doit projetter un objet soumis exclusivement à son poids (mouvement balistique~: voir paragraphes \ref{balistique} et \ref{tirbalistique}) pour que sa portée soit maximale.\\
Selon l'équation \ref{Portée}, on a~:
\[x_P=\frac{v_o^2}{g}\cdot \sin(2\cdot \alpha)\]
Il s'agit ici de maximiser cette portée. Plus précisément de trouver l'angle $\alpha_{max}$ pour lequel celle-ci est maximale. La fonction est ainsi la portée $x_P$ et la variable $\alpha$. On doit donc dériver $x_P$ en fonction de $\alpha$ et l'annuler~:
\[\frac{d}{d\alpha}x_P(\alpha)=0\]
On obtient alors~:
\begin{align*}
\frac{dx_P}{d\alpha}&=\frac{v_o^2}{g}\cdot \frac{d}{d\alpha} \sin(2\cdot \alpha)\\
&=\frac{v_o^2}{g}\cdot 2\cdot \cos(2\cdot \alpha)=0\\
\end{align*}
c'est-à-dire~:
\[\cos(2\cdot \alpha)=0\]
d'où finalement~:
\[2\cdot \alpha=\pi{}\;\;\Rightarrow\;\;\alpha=\pi{}/2=45^\circ\]
Bien évidemment ce résultat n'est valable que pour un mouvement balistique, pour lequel l'objet n'est soumis à aucun frottement.

\section{Dynamique}
Le rapport différentiel entre la position, la vitesse et l'accélération implique deux types de traitement mathématique de la seconde loi de Newton pour déterminer la vitesse ou la position d'un objet à partir de la connaissance des forces qui s'exercent sur lui.
\medskip

Pour bien les comprendre, relevons tout d'abord encore une fois l'expression de la seconde loi de Newton. On a~:
\[\sum F^{ext}=m\cdot a\]
Cela peut aussi s'écrire~:
\[m\cdot a=\sum F^{ext}\]
ou, en raison des relations différentielles \ref{posvitaccdiff} entre la position, la vitesse et l'accélération~:
\begin{equation}
\fbox{$\displaystyle m\cdot \dot v=m\cdot \ddot x=\sum F^{ext}$} \label{diffma}
\end{equation}
Manifestement, cette équation est différentielle. Si le membre de droite de l'équation ne dépend ni de la vitesse, ni de la position, sa résolution se fait simplement par intérgration. Par contre, s'il dépend de l'une ou l'autre de ces deux grandeurs, on aura alors à traiter une équation différentielle.

\subsection{Intégration}
C'est le cas le plus simple. La partie gauche de l'équation \ref{diffma} est une fonction explicite du temps. On peut donc écrire~:
\begin{align*}
m\cdot \dot v&=\sum F^{ext}\;\;\Rightarrow \\
v&=\frac{1}{m}\cdot \int \sum F^{ext} dt\;\;\Rightarrow \\
x&=\int v\cdot dt
\end{align*}
Mais, pour mieux le comprendre, considérons des exemples simples.

\subsubsection{Chute libre}
Dans ce cas l'équation \ref{diffma} s'écrit~:
\[m\cdot \dot v=m\cdot g\]
D'où, par intégration~:
\[v=\int g\cdot dt+c=g\cdot t+v_o\]
en raison des conditions initiales qui impliquent par exemple que~:
\[v(t=0)=v_o\]
Puis, on intégre un fois de plus~:
\[x=\int (g\cdot t+v_o)\cdot dt+c'=\frac{1}{2}gt^2+v_ot+x_o\]
toujours en raison des conditions initiales.

\subsubsection{Freinage}
On considère une voiture par exemple qui décélère sous l'effet de la force de frottement exercée par la route sur les pneux. On peut écrire~:
\[m\cdot \dot v=-\mu_o\cdot mg\]
D'où, par intégration~:
\[v=-\frac{1}{m}\int \mu_o\cdot mg\cdot dt+c=-\mu_o\cdot g\cdot t+v_o\]
et~:
\[x=-\frac{1}{2}\mu_og\cdot t^2+v_o\cdot t+x_o\]

\subsection{Équation différentielle}
Le cas se complique si la partie gauche de l'équation \ref{diffma} est une fonction de la vitesse ou de la position. Plusieurs cas sont à considérer selon le type de fonction. C'est pourquoi nous allons considérer les exemples suivants.

\subsubsection{Chute dans un fluide visqueux}
Il s'agit de la chute d'un objet de masse $m$ sous l'effet de son poids et d'une force de frottement de type visqueux de la forme $F_{fr}=k\cdot v$. La situation est présentée à la figure \ref{chutevisqueuse}.

\begin{figure}[htbp]

\caption{Chute soumise à un frottement visqueux\label{chutevisqueuse}}

\begin{center}\includegraphics{Chutevisqueuse.eps}\end{center}
\end{figure}

La seconde loi de Newton, s'écrit selon l'axe de la figure \ref{chutevisqueuse}~:
\[P-F_{fr}=m\cdot a\]
Avec une force de frottement dépendant linéairement de la vitesse, on a~:
\[m\cdot g-k\cdot v=m\cdot a\]
D'un point de vue différentiel, cette équation donne~:
\[m\cdot g-k\cdot v=m\cdot \dot v\]
C'est une équation différentielle du premier ordre linéaire en $v$. On peut l'écrire sous forme canonique~:
\[\dot v+\frac{k}{m}\cdot v=g\]
La solution de cette équation différentielle est alors donnée par la somme de la solution générale de l'équation homogène (ou équation sans second membre) avec une solution particulière de l'équation complète (équation avec second membre).
\begin{enumerate}
\item Solution générale de l'équation homogène~:
\[\dot v+\frac{k}{m}\cdot v=0\]
Techniquement, pour trouver la solution de cette équation, il vaut mieux écrire~:
\[\frac{dv}{dt}+\frac{k}{m}\cdot v=0\]
Ainsi,on peut séparer les variables ($v$ et $t$) de chaque côté de l'équation~:
\begin{align*}
\frac{dv}{dt}&=-\frac{k}{m}\cdot v\;\;\Rightarrow\\
dv&=-\frac{k}{m}\cdot v\cdot dt\;\;\Rightarrow\\
\frac{dv}{v}&=-\frac{k}{m}\cdot dt
\end{align*}
Et il ne reste alors plus qu'à intégrer des deux côtés de l'équation~:
\begin{align*}
\int \frac{dv}{v}&=-\int \frac{k}{m}\cdot dt\;\;\Rightarrow\\
\int \frac{1}{v}\cdot dv&=-\frac{k}{m}\int dt\;\;\Rightarrow\\
\ln (v)&=-\frac{k}{m}\cdot t+C\;\;\Rightarrow\\
e^{\ln (v)}&=e^{-\frac{k}{m}\cdot t+C}\;\;\Rightarrow\\
v(t)&=e^{-\frac{k}{m}\cdot t}\cdot e^{C}\;\;\Rightarrow\\
v(t)&=D\cdot e^{-\frac{k}{m}\cdot t}
\end{align*}
Où $C$ et $D$ sont des constantes.
\item Solution particulière de l'équation différentielle~:
Deux méthodes se proposent à nous~: la variation des constantes ou un essai "inspiré". Nous verrons plus loin un exemple de la première méthode. Utilisons la seconde en tentant la solution particulière~:
\[v(t)=A\]
On remplace donc cette solution, et sa dérivée $\dot v=0$, dans l'équation différentielle~:
\begin{align*}
\dot v+\frac{k}{m}\cdot v&=g\;\;\Rightarrow\\
0+\frac{k}{m}\cdot A&=g\;\;\Rightarrow\\
A&=\frac{m}{k}\cdot g
\end{align*}
\end{enumerate}
Finalement, on construit la solution générale de l'équation différentielle par addition de la solution générale de l'équation homogène et de la solution particulière trouvée~:
\[v(t)=D\cdot e^{-\frac{k}{m}\cdot t}+\frac{m}{k}\cdot g\]
Reste à déterminer la valeur de la constante D. Pour cela, utilisons les conditions initiales en imaginant qu'à $t=0$ la vitesse $v=0$. On a alors~:
\begin{align*}
0&=D\cdot e^0+\frac{m}{k}\cdot g\;\;\Rightarrow\\
D&=-\frac{m}{k}\cdot g
\end{align*}
Enfin, on peut écrire~:
\begin{align*}
v(t)&=-\frac{m}{k}\cdot g\cdot e^{-\frac{k}{m}\cdot t}+\frac{m}{k}\cdot g\\
&=-\frac{m}{k}\cdot g\cdot (e^{-\frac{k}{m}\cdot t}-1)
\end{align*}
On vérifie bien qu'à $t=0$ la vitesse est nulle. On voit, par ailleurs, que pour $t\rightarrow\infty$, $e^{-\frac{k}{m}\cdot t}\rightarrow 0$ et la vitesse se stabilise à une valeur de
\[v=\frac{m}{k}\cdot g\]
A ce moment là, la force de frottement compense exactement le poids
\[F_{fr}=k\cdot v=k\cdot \frac{m}{k}\cdot g=m\cdot g\]
et la vitesse devient constante.\\
On peut ainsi représenter l'évolution de la vitesse en fonction du temps de la manière présentée sur la figure \ref{figchutevisqueuse}.

\begin{center}
\begin{figure}[t]

\caption{Chute visqueuse\label{figchutevisqueuse}}

\begin{center}
\includegraphics[width=4cm,angle=-90]{ChuteVisqueuse.eps}\end{center}
\end{figure}
\end{center}

\subsubsection{Mouvement harmonique}
Le mouvement que suit une masse oscillante attachée au bout d'un ressort est utile dans bien des cas, notamment en physique du solide. C'est un mouvement qui se déroule sur un seul axe. Il est pourtant complexe, car son analyse nécessite le calcul différentiel.\\
Considérons donc la figure \ref{masseoscillante}.

\begin{figure}[htbp]

\caption{Masse oscillante\label{masseoscillante}}

\begin{center}\includegraphics{Masseoscillante.eps}\end{center}
\end{figure}

Considérons un ressort dont on marque la position détendue par o. On attache à l'extrémité de celui-ci une masse $m$ et on comprime le ressort d'une distance $x_0$. Au départ sa vitesse est nulle. On le lâche alors en même temps qu'on enclanche le chronomètre.\\
L'équation du mouvement pour un tel système est unidimentionnelle. Elle s'écrit~:
\[\sum F^{ext}=F=-k\cdot x=m\cdot a\]
avec un signe négatif pour la force car la situation de la figure \ref{masseoscillante} décrit une position négative de la masse. Ainsi, pour que la force soit positive, le signe négatif est nécessaire.
En raison de la dépendance de la force en fonction de la position, il s'agit d'une équation différentielle qu'on peut réécrire ainsi~:
\[m\cdot \ddot x+k\cdot x=0\]
Cette équation est linéaire du second ordre à coefficients constants. Elle n'a pas de second membre. On parle d'équation homogène. Sa solution passe donc uniquement par la solution de l'équation caractéristique suivante~:
\[m\cdot r^2+k=0\]
qui se trouve être double et complexe~:
\[r=\pm \sqrt{-\frac{k}{m}}=\pm \sqrt{i^2\cdot \frac{k}{m}}=\pm i\cdot \sqrt{\frac{k}{m}}\]
Ainsi, la solution de l'équation différentielle se présente sous la forme~:
\[x(t)=c_1\cdot \cos(\sqrt{\frac{k}{m}}\cdot t)+c_2\cdot \sin(\sqrt{\frac{k}{m}}\cdot t)\]
où $c_1$ et $c_2$ sont des constantes à déterminer avec les conditions initiales.
Pour cela, on peut poser~:
\begin{equation}\label{cond_init}
x(t=0)=-x_o\;\;et\;\;v(t=0)=0
\end{equation}
A l'aide de la première relation \ref{cond_init}, on a~:
\[c_1=-x_o\]
et en dérivant la position par rapport au temps pour obtenir la vitesse~:
\begin{align*}
v(t)&=\frac{d}{dt}(c_1\cdot \cos(\sqrt{\frac{k}{m}}\cdot t)+c_2\cdot \sin(\sqrt{\frac{k}{m}}\cdot t))\\
&=-c_1\cdot \sqrt{\frac{k}{m}} \sin(\sqrt{\frac{k}{m}}\cdot t)\\
&+c_2\cdot \sqrt{\frac{k}{m}} \cos(\sqrt{\frac{k}{m}}\cdot t)
\end{align*}
on peut aussi utiliser la seconde équation \ref{cond_init}~:
\[c_2\cdot \sqrt{\frac{k}{m}}=0\;\;\Rightarrow\;\;c_2=0\]
Ainsi, on a pour solution finale~:
\begin{equation}\label{ressortposition}
x(t)=-x_o\cdot \cos(\sqrt{\frac{k}{m}}\cdot t)
\end{equation}
et pour la vitesse~:
\begin{equation}\label{ressortvitesse}
v(t)=x_o\cdot \sqrt{\frac{k}{m}} \sin(\sqrt{\frac{k}{m}}\cdot t)
\end{equation}
Maintenant que nous avons les équations du mouvement, nous pouvons nous intéresser à la vitesse maximale de la masse par exemple. Deux solutions s'offrent à nous~:
\begin{itemize}
\item Par symétrie, la vitesse maximale doit être atteinte quand la masse passe par l'origine. On a donc cette vitesse $v_{max}$ quand~:
\[x(t_{max})=0\]
Ainsi, on peut écrire à l'aide de l'équation \ref{ressortposition}~:
\[\cos(\sqrt{\frac{k}{m}}\cdot t_{max})=0\]
ce qui implique une première solution pour~:
\begin{align}\label{ressort1ersol}
\sqrt{\frac{k}{m}}\cdot t_{max}=\frac{\pi}{2}
\end{align}
soit un temps~:
\[t_{max}=\frac{\pi}{2}\cdot \sqrt{\frac{m}{k}}\]
Ensuite, on peut calculer la vitesse maximale $v_{max}$ à l'aide de l'équation \ref{ressortvitesse} pour le temps $t_{max}$~:
\begin{align}
v(t_{max})&=x_o\cdot \sqrt{\frac{k}{m}} \sin(\sqrt{\frac{k}{m}}\cdot t_{max}) \nonumber \\
&=x_o\cdot \sqrt{\frac{k}{m}} \sin(\frac{\pi}{2}) \nonumber \\
&=x_o\cdot \sqrt{\frac{k}{m}} \label{ressortvitmax}
\end{align}
\item Par annulation de la dérivée de l'équation \ref{ressortvitesse}, on peut aussi obtenir la vitesse maximale $v_{max}$~:
\begin{align*}
\frac{d}{dt}(x_o\cdot \sqrt{\frac{k}{m}} \sin(\sqrt{\frac{k}{m}}\cdot t_{max}))&=0\;\Rightarrow\\
\frac{d}{dt}(\sin(\sqrt{\frac{k}{m}}\cdot t_{max}))&=0\;\Rightarrow\\
\cos(\sqrt{\frac{k}{m}}\cdot t_{max})&=0\;\Rightarrow\\
\sqrt{\frac{k}{m}}\cdot t_{max}&=\frac{\pi}{2}
\end{align*}
Or cette condition est précisément celle obtenue en \ref{ressort1ersol}. La vitesse maximale est donc la même que celle en \ref{ressortvitmax}.
\end{itemize}

\subsubsection{Mouvement harmonique d'une masse pendante}
Parfois, l'équation différentielle du mouvement n'est pas homogène, c'est-à-dire qu'elle comporte un seconde membre. C'est le cas pour le système d'une masse oscillante suspendue à un ressort. Considérons donc la figure \ref{masseoscillantesuspendue}.

\begin{figure}[htbp]

\caption{Masse suspendue\label{massesuspendue}}

\begin{center}\includegraphics{Massesuspendue.eps}\end{center}
\end{figure}

La position d'équilibre, autour de laquelle la masse va osciller, est satisfaite par la condition d'équilibre statique~:
\[-k\cdot x_{\acute eq} + m\cdot g = 0\]
qui nous donne une position~:
\[x_{\acute eq}=\frac{m\cdot g}{k}\]

En considérant maintenant la figure \ref{masseoscillantesuspendue}

\begin{figure}[htbp]

\caption{Masse oscillante suspendue\label{masseoscillantesuspendue}}

\begin{center}\includegraphics{Masseoscillantesuspendue.eps}\end{center}
\end{figure}

on peut définir une nouvelle coordonnée $x'$ qui va nous permettre d'obtenir une solution oscillant autour de la position d'équilibre. On peut donc naturellement poser~:
\[x=x'+x_{\acute eq}=x'+\frac{m\cdot g}{k}\]
L'équation du mouvement devient alors~:
\[mg-kx=mg-k(x'+\frac{m\cdot g}{k})=m\cdot a\]
C'est une équation différentielle, à coefficients constants, du second degré. Elle peut s'écrire, en terme de position~:
\[m\cdot \ddot x+k\cdot x+\frac{m\cdot g}{k}-m\cdot g=0\]
ou, sous forme canonique~:
\begin{equation}
\ddot x+\frac{k}{m}\cdot x+\frac{1-k}{k}\cdot g=0
\end{equation}

\subsubsection{Mouvement non linéaire}
Le dernier type de mouvement auquel nous allons nous intéresser est celui d'une masse soumise à une force de frottement non-linéaire~: $F_{fr}=-k\cdot v^2$. C'est typiquement le type de force de frottement qui apparaît quand un objet tombe à grande vitesse dans l'air. Imaginons donc une masse $m$ soumise à son poids et à une telle force de frottement. La seconde loi de Newton peut s'écrire, selon un axe orienté dans le même sens que le poids~:
\[m\cdot g-k\cdot v^2=m\cdot a\]
ou sous forme différentielle~:

\begin{equation}\label{non-lineaire}
m\cdot \ddot x+k\cdot \dot x^2-m\cdot g=0
\end{equation}

Cette équation est du second degré et non linéaire en raison de la présence de $\dot x^2$. Elle ne possède aucune solution analytique et il faut utilier l'ordinateur pour la résoudre.\\
Cependant, pendant la chute, la force de frottement augmente progressivement jusqu'à compenser le poids. Alors la vitesse de l'objet devient constante. On la nomme vitesse limite de chute et on peut la calculer. En effet, si $v=const$, alors $a=0$ et $\ddot x=0$. L'équation \ref{non-lineaire} devient alors~:
\[k\cdot \dot x^2=m\cdot g\,\Rightarrow\,k\cdot \dot x^2=\frac{m\cdot g}{k}\]
soit finalement~:
\[v=\dot x=\sqrt{\frac{m\cdot g}{k}}\]