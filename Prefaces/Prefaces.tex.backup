\pagestyle{fancy}
%\lhead{Lycée Blaise Cendrars\\La Chaux-de-Fonds}
\onecolumn
%\pagenumbering{arabic}

\titlepage
\AddToShipoutPicture*{\BackgroundPic}	% pour mettre une image en fond de première page

\vspace*{2.5cm}
\begin{center}\textbf{\textsc{\fontsize{36}{12}\selectfont Physique \opt{OS}{OS}}}\end{center}{\Huge \par}

\vspace*{3cm}
\begin{center}\textbf{\textsc{\huge \textcolor{white}{Mécanique}}}\end{center}{\large \par}

\begin{center}\textbf{\textsc{\large \textcolor{white}{\&}}}\end{center}{\large \par}

\begin{center}\textbf{\textsc{\huge \textcolor{white}{Énergie}}}\end{center}{\large \par}

\vfill{}
%\begin{flushright}vg \end{flushright}

\begin{flushright}\textcolor{white}{\today}

\onecolumn\end{flushright}
\thispagestyle{empty}		% pas de numéro de page et d'entête pour cette page.

\begin{center}\textbf{Préambule} :\end{center}
\medskip

\medskip
Ce cours de mécanique et énergie a été écrit au début avec le logiciel de PAO Lyx, une interface graphique au célèbre Latex et aujourd'hui directement en latex. Il a donc été créé dans un environnement (ces deux logiciels tournant sous GNU-Linux) libre dont l'objectif est de contribuer au progrès en mettant à disposition de chacun, pour un coût moindre, le travail de milliers de programmeurs bénévoles. Dans ce cadre, il était naturel de permettre à chaque étudiant d'avoir accès à ce cours librement. C'est pourquoi, à l'instar des logiciels, il est distribué sous licence GFDL, licence de documentation libre. Attention cependant aux images qui ne sont pas toutes sous licence GFDL, mais peuvent être soumises à une autre licence libre ou être dans le domaine publique. Cela peut avoir une importance dans certains cas.

Normalement la licence GFDL\index{licence GFDL} doit figurer avec le cours. Ce n'est pas le cas et ce pour ne pas allonger trop le texte. Mais le texte de la GFDL se trouve partout sur internet et il suffit d'un moteur de recherche pour le trouver. Par ailleurs, le texte de ce cours est disponible en téléchargement à l'adresse

\begin{center}http://www.cvgg.org\end{center}

Pour tout renseignement complémentaire s'adresser à :

\medskip
\begin{center}Vincent Guyot\end{center}

\begin{center}Chapeau-Râblé 37\end{center}

\begin{center}2300 La Chaux-de-Fonds\end{center}

\begin{center}vincent@cvgg.org\end{center}

\bigskip{}
\begin{center}\textbf{Copyright 2007 Guyot Vincent}\end{center}

\begin{center}Permission vous est donnée de copier, distribuer et/ou modifier ce document selon les termes de la Licence GNU Free Documentation License, Version 1.1 ou ultérieure publiée par la Free Software Fundation ; avec les sections inaltérables suivantes :\end{center}

\begin{center}\emph{Pas de section inaltérable}\end{center}

\begin{center}avec le texte de première page de couverture suivant :\end{center}

\begin{center}\emph{Physique Mécanique \& Énergie}\end{center}

\begin{center}avec le texte de dernière page de couverture suivant :\end{center}

\begin{center}\emph{Pas de texte de dernière page de couverture}\end{center}

\begin{center}Une copie transparente de ce document se trouve à l'adresse
suivante :\end{center}

\begin{center}www.cvgg.org//Accueil/Physique/\end{center}
\vfill{}

%\newpage

\myclearpage

\textsc{Remerciements}

\bigskip

Je tiens ici à remercier tout particulièrement l'encyclopédie Wikipedia et la NASA, pour les nombreuses illustrations dont ce cours à pu bénéficier. Toutes deux rendent accessibles gratuitement à tous des savoirs importants.

\medskip
En particulier, je remercie Alain Riazuelo, cosmologue à l'Institut d'Astrophysique de Paris, chercheur au CNRS, pour la magnifique représentation (sous licence libre) d'un trou noir qui fait la couverture de cet ouvrage et qu'il est possible de consulter à l'adresse suivante :

\smallskip
\url=http://commons.wikimedia.org/wiki/Image:FY221c15.png=

\smallskip
Il faut aussi évoquer dans ce cadre le magnifique ouvrage de Hans-Peter Nollert et Hanns Ruder \cite{HNHR08} qui présente des images relativistes au voisinage de trous noirs dans l'esprit de celles d'Alain Riazuello. C'est un ouvrage d'abord pour les yeux, dans l'esprit de découverte de George Gamow, et pour l'esprit par les interrogations qu'il succite.

\smallskip
L'image de couverture d'Alain Riazuelo est particulièrement adaptée à ce cours. En effet, on verra que l'accent est mis sur la notion de gravitation\index{gravitation} à travers la chute libre\index{chute libre}, les satellites\index{satellite} et les marées\index{maree@marée}. Évidemment, il s'agit de physique classique. Pas question d'aborder ici la relativité générale\index{relativite@relativité!générale}, au c\oe ur de la description des trous noirs\index{trous noir}. Cependant, comme ils sont issus de la plus moderne des théories de la gravitation et qu'aujourd'hui ils sont autant vus à travers la relativité que sous l'angle de la physique quantique\index{physique!quantique}, ils sont la conclusion provisoire presque naturelle de notre compréhension de la gravitation dans cette incroyable courbure de l'espace qui les enveloppe.

\bigskip

Je tiens aussi à remercier mes collègues MM. Michel Augsburger et surtout Marcel Fiechter pour leur travail, ingrat mais nécessaire, de relecture attentive, de corrections et de suggestions pertinentes qui ont grandement augmenté la qualité de ce cours.

\bigskip
Le chapitre d'introduction est un résumé d'une présentation d'introduction à la physique réalisée avec le logiciel \emph{\textsc{BEAMER}}. Cette présentation est publiée en licence GFDL à la même adresse (www.cvgg.org) que ce cours. Toutes les images utilisées sont libres (dans le domaine publique ou en GFDL) et référencées. Elle constitue un complément à ce cours.

\bigskip
\textsc{Avertissements}

\bigskip

A l'instar des logiciels libres, je décline toute responsabilité relative à l'utilisation de ce cours. Comme je tiens à le mettre aussi vite que possible à disposition de tous, je mets clairement l'accent sur la réalisation de son contenu, plutôt que sur les nombreuses relectures qu'il nécessite assurément. Si la nécessité est claire, le temps manque et il faut faire des choix. J'encourage donc toute personne intéressée à collaborer avec moi à la création de ce cours, sous toute ses formes, à me contacter à l'adresse mail ci-dessus. Reste que si toutes les suggestions de corrections sont les bienvenues, cela ne veut pas dire qu'elles seront prises en considération immédiatement, pour des raisons de temps. Si cela vous semble insuffisant, n'hésitez pas à me faire parvenir vos modifications, corrections, ajouts déjà rédigés sous une forme directement intégrable au cours, en licence GFDL naturellemnt. Cela me permettra de les reporter plus rapidement dans le cours. Si cela est encore insuffisant, le mieux est de participer directement à la rédaction en prenant contact avec moi.

\myclearpage