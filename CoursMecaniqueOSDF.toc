\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{21}
\contentsline {section}{\numberline {1.1}Du temps}{21}
\contentsline {section}{\numberline {1.2}De l'infiniment grand \IeC {\`a} l'infiniment petit}{21}
\contentsline {subsection}{\numberline {1.2.1}L'Univers}{21}
\contentsline {subsection}{\numberline {1.2.2}Les amas de galaxies}{22}
\contentsline {subsection}{\numberline {1.2.3}Les galaxies}{23}
\contentsline {subsection}{\numberline {1.2.4}Les \IeC {\'e}toiles}{24}
\contentsline {subsection}{\numberline {1.2.5}Le syst\IeC {\`e}me solaire}{24}
\contentsline {subsection}{\numberline {1.2.6}La Terre et la Lune}{26}
\contentsline {subsubsection}{La Terre}{26}
\contentsline {subsubsection}{La Lune}{27}
\contentsline {subsubsection}{Les mar\IeC {\'e}es}{29}
\contentsline {subsection}{\numberline {1.2.7}Le monde subatomique}{32}
\contentsline {subsubsection}{Conclusion}{36}
\contentsline {chapter}{\numberline {2}La cin\IeC {\'e}matique}{37}
\contentsline {section}{\numberline {2.1}Introduction}{37}
\contentsline {section}{\numberline {2.2}Position}{37}
\contentsline {subsection}{\numberline {2.2.1}Dimensions}{37}
\contentsline {subsection}{\numberline {2.2.2}Syst\IeC {\`e}me d'axes}{37}
\contentsline {subsection}{\numberline {2.2.3}Position}{37}
\contentsline {subsection}{\numberline {2.2.4}D\IeC {\'e}placement}{38}
\contentsline {subsection}{\numberline {2.2.5}Distance parcourue}{38}
\contentsline {section}{\numberline {2.3}Vitesse}{38}
\contentsline {subsection}{\numberline {2.3.1}Vitesse moyenne}{38}
\contentsline {subsection}{\numberline {2.3.2}Exemples}{38}
\contentsline {subsubsection}{Exemple 1}{38}
\contentsline {subsubsection}{Exemple 2}{38}
\contentsline {subsection}{\numberline {2.3.3}Vitesse instantan\IeC {\'e}e}{38}
\contentsline {section}{\numberline {2.4}Acc\IeC {\'e}l\IeC {\'e}ration}{39}
\contentsline {subsection}{\numberline {2.4.1}Acc\IeC {\'e}l\IeC {\'e}ration moyenne}{39}
\contentsline {subsection}{\numberline {2.4.2}Exemples}{39}
\contentsline {subsubsection}{Exemple 1}{39}
\contentsline {subsubsection}{Exemple 2}{39}
\contentsline {subsubsection}{Remarque~:}{39}
\contentsline {subsection}{\numberline {2.4.3}L'acc\IeC {\'e}l\IeC {\'e}ration instantan\IeC {\'e}e}{39}
\contentsline {section}{\numberline {2.5}Mouvements simples}{39}
\contentsline {subsection}{\numberline {2.5.1}Le mouvement rectiligne uniforme}{39}
\contentsline {subsubsection}{D\IeC {\'e}finition}{39}
\contentsline {subsubsection}{Propri\IeC {\'e}t\IeC {\'e}s}{39}
\contentsline {subsubsection}{Un exemple~: Apollo en route vers la Lune}{40}
\contentsline {subsubsection}{Autre exemple~: le d\IeC {\'e}placement d'Androm\IeC {\`e}de}{41}
\contentsline {subsection}{\numberline {2.5.2}Mouvement rectiligne uniform\IeC {\'e}ment acc\IeC {\'e}l\IeC {\'e}r\IeC {\'e} (MRUA)}{41}
\contentsline {subsubsection}{D\IeC {\'e}finition}{41}
\contentsline {subsubsection}{Propri\IeC {\'e}t\IeC {\'e}s}{41}
\contentsline {subsection}{\numberline {2.5.3}La chute libre}{42}
\contentsline {paragraph}{D\IeC {\'e}finition}{42}
\contentsline {paragraph}{Propri\IeC {\'e}t\IeC {\'e}s}{42}
\contentsline {paragraph}{Exp\IeC {\'e}rience}{42}
\contentsline {paragraph}{Calculs}{42}
\contentsline {paragraph}{Analyse des r\IeC {\'e}sultats}{43}
\contentsline {paragraph}{Conclusions}{44}
\contentsline {subsection}{\numberline {2.5.4}Balistique}{44}
\contentsline {paragraph}{D\IeC {\'e}finition}{44}
\contentsline {paragraph}{Propri\IeC {\'e}t\IeC {\'e}s}{44}
\contentsline {paragraph}{\IeC {\'E}quations}{44}
\contentsline {paragraph}{Premier exemple}{45}
\contentsline {paragraph}{Second exemple}{45}
\contentsline {subsection}{\numberline {2.5.5}La chute libre ... de la Lune}{45}
\contentsline {subsection}{\numberline {2.5.6}Mouvement circulaire uniforme (MCU)}{47}
\contentsline {subsubsection}{Relation importante}{47}
\contentsline {subsection}{\numberline {2.5.7}Lois de Kepler}{48}
\contentsline {chapter}{\numberline {3}La m\IeC {\'e}canique}{51}
\contentsline {section}{\numberline {3.1}La \FB@og m\IeC {\'e}canique\fg {} d'Aristote}{51}
\contentsline {subsection}{\numberline {3.1.1}Introduction}{51}
\contentsline {subsection}{\numberline {3.1.2}Platon}{51}
\contentsline {subsection}{\numberline {3.1.3}Aristote}{52}
\contentsline {subsubsection}{Cin\IeC {\'e}matique}{52}
\contentsline {subsubsection}{Dynamique}{52}
\contentsline {section}{\numberline {3.2}M\IeC {\'e}canique de Newton}{55}
\contentsline {subsection}{\numberline {3.2.1}Introduction}{55}
\contentsline {subsection}{\numberline {3.2.2}M\IeC {\'e}canique}{55}
\contentsline {subsubsection}{Les trois lois de Newton}{55}
\contentsline {subsubsection}{Force ext\IeC {\'e}rieure}{56}
\contentsline {subsubsection}{Exemples}{57}
\contentsline {subsection}{\numberline {3.2.3}Types de forces}{57}
\contentsline {subsubsection}{Loi de la gravitation universelle}{58}
\contentsline {subsubsection}{Le poids}{58}
\contentsline {paragraph}{Exemple}{59}
\contentsline {subsubsection}{Masse et poids}{60}
\contentsline {subsubsection}{Poids apparent}{60}
\contentsline {subsubsection}{Gravitation et MCU}{62}
\contentsline {subsubsection}{Troisi\IeC {\`e}me loi de Kepler}{63}
\contentsline {subsubsection}{Les mar\IeC {\'e}es}{65}
\contentsline {subsubsection}{Le frottement}{69}
\contentsline {paragraph}{Exemple}{70}
\contentsline {subsubsection}{La force d'un ressort}{70}
\contentsline {paragraph}{Exemple}{70}
\contentsline {chapter}{\numberline {4}M\IeC {\'e}canique en plusieurs dimensions}{73}
\contentsline {section}{\numberline {4.1}Pr\IeC {\'e}liminaires}{73}
\contentsline {subsection}{\numberline {4.1.1}Dimensions}{73}
\contentsline {subsection}{\numberline {4.1.2}Syst\IeC {\`e}me d'axes}{73}
\contentsline {section}{\numberline {4.2}Notion de vecteur en physique}{73}
\contentsline {subsection}{\numberline {4.2.1}Norme d'un vecteur}{73}
\contentsline {subsection}{\numberline {4.2.2}Op\IeC {\'e}rations vectorielles}{74}
\contentsline {subsubsection}{produit scalaire}{74}
\contentsline {subsubsection}{Produit vectoriel}{74}
\contentsline {section}{\numberline {4.3}M\IeC {\'e}canique}{74}
\contentsline {subsection}{\numberline {4.3.1}Cin\IeC {\'e}matique}{74}
\contentsline {subsubsection}{Position}{74}
\contentsline {subsubsection}{Vitesse}{75}
\contentsline {subsubsection}{Acc\IeC {\'e}l\IeC {\'e}ration}{75}
\contentsline {subsection}{\numberline {4.3.2}Dynamique}{75}
\contentsline {subsubsection}{Premi\IeC {\`e}re loi}{75}
\contentsline {subsubsection}{Seconde loi}{75}
\contentsline {subsubsection}{Troisi\IeC {\`e}me loi}{75}
\contentsline {section}{\numberline {4.4}Exemples}{75}
\contentsline {subsection}{\numberline {4.4.1}Statique}{75}
\contentsline {subsection}{\numberline {4.4.2}Plan inclin\IeC {\'e}}{76}
\contentsline {subsection}{\numberline {4.4.3}Balistique}{78}
\contentsline {subsection}{\numberline {4.4.4}Mouvement circulaire uniforme~: MCU}{80}
\contentsline {subsubsection}{D\IeC {\'e}finition}{80}
\contentsline {subsubsection}{Cin\IeC {\'e}matique}{80}
\contentsline {subsubsection}{Relation importante}{81}
\contentsline {subsubsection}{Dynamique}{81}
\contentsline {subsubsection}{Virages inclin\IeC {\'e}s}{81}
\contentsline {paragraph}{Vitesses minimales}{82}
\contentsline {paragraph}{Vitesses maximales}{83}
\contentsline {subsection}{\numberline {4.4.5}Satellite en orbite g\IeC {\'e}ostationnaire}{84}
\contentsline {subsubsection}{Introduction}{84}
\contentsline {subsubsection}{Th\IeC {\'e}oriquement}{84}
\contentsline {subsubsection}{Num\IeC {\'e}riquement}{84}
\contentsline {subsection}{\numberline {4.4.6}Mouvement central}{84}
\contentsline {subsubsection}{Mouvement kepleriens}{84}
\contentsline {subsubsection}{Loi de Kepler}{84}
\contentsline {chapter}{\numberline {5}M\IeC {\'e}canique diff\IeC {\'e}rentielle}{87}
\contentsline {section}{\numberline {5.1}Introduction}{87}
\contentsline {section}{\numberline {5.2}Cin\IeC {\'e}matique}{87}
\contentsline {subsection}{\numberline {5.2.1}Exemples}{88}
\contentsline {subsubsection}{Mouvement rectiligne uniform\IeC {\'e}ment acc\IeC {\'e}l\IeC {\'e}r\IeC {\'e}~: MRUA}{88}
\contentsline {subsubsection}{Port\IeC {\'e} maximum en balistique}{88}
\contentsline {section}{\numberline {5.3}Dynamique}{89}
\contentsline {subsection}{\numberline {5.3.1}Int\IeC {\'e}gration}{89}
\contentsline {subsubsection}{Chute libre}{89}
\contentsline {subsubsection}{Freinage}{89}
\contentsline {subsection}{\numberline {5.3.2}\IeC {\'E}quation diff\IeC {\'e}rentielle}{89}
\contentsline {subsubsection}{Chute dans un fluide visqueux}{89}
\contentsline {subsubsection}{Mouvement harmonique}{91}
\contentsline {subsubsection}{Mouvement harmonique d'une masse pendante}{92}
\contentsline {subsubsection}{Mouvement non lin\IeC {\'e}aire}{93}
\contentsline {chapter}{\numberline {6}Quantit\IeC {\'e} de mouvement}{95}
\contentsline {section}{\numberline {6.1}Introduction}{95}
\contentsline {section}{\numberline {6.2}Quantit\IeC {\'e} de mouvement}{95}
\contentsline {subsection}{\numberline {6.2.1}Masse d'inertie}{95}
\contentsline {section}{\numberline {6.3}\IeC {\'E}nergie cin\IeC {\'e}tique}{96}
\contentsline {section}{\numberline {6.4}Choc parfaitement \IeC {\'e}lastique}{96}
\contentsline {subsection}{\numberline {6.4.1}Exemple}{96}
\contentsline {section}{\numberline {6.5}Choc parfaitement mou}{97}
\contentsline {subsection}{\numberline {6.5.1}Exemple}{97}
\contentsline {section}{\numberline {6.6}choc bidimentionnel}{98}
\contentsline {subsection}{\numberline {6.6.1}Exemple}{98}
\contentsline {section}{\numberline {6.7}Impulsion}{99}
\contentsline {chapter}{\numberline {7}L'\IeC {\'e}nergie}{101}
\contentsline {section}{\numberline {7.1}Introduction}{101}
\contentsline {section}{\numberline {7.2}Travail}{101}
\contentsline {section}{\numberline {7.3}Puissance}{102}
\contentsline {section}{\numberline {7.4}\IeC {\'E}nergie potentielle}{102}
\contentsline {section}{\numberline {7.5}\IeC {\'E}nergie cin\IeC {\'e}tique}{103}
\contentsline {section}{\numberline {7.6}Th\IeC {\'e}or\IeC {\`e}me de l'\IeC {\'e}nergie cin\IeC {\'e}tique}{103}
\contentsline {section}{\numberline {7.7}Conservation de l'\IeC {\'e}nergie m\IeC {\'e}canique}{103}
\contentsline {section}{\numberline {7.8}Variation de l'\IeC {\'e}nergie m\IeC {\'e}canique}{105}
\contentsline {section}{\numberline {7.9}\IeC {\'E}nergies renouvelables}{105}
\contentsline {subsection}{\numberline {7.9.1}\IeC {\'E}nergie hydraulique}{105}
\contentsline {subsubsection}{Exemple}{106}
\contentsline {subsubsection}{Types de turbines}{107}
\contentsline {subsubsection}{Alternateur}{107}
\contentsline {subsubsection}{Probl\IeC {\`e}mes rencontr\IeC {\'e}s}{107}
\contentsline {subsection}{\numberline {7.9.2}\IeC {\'E}nergie \IeC {\'e}olienne}{108}
\contentsline {subsection}{\numberline {7.9.3}\IeC {\'E}nergie solaire}{109}
\contentsline {subsubsection}{\IeC {\'E}nergie solaire thermique}{109}
\contentsline {subsubsection}{\IeC {\'E}nergie solaire \IeC {\'e}lectrique}{111}
\contentsline {subsection}{\numberline {7.9.4}\IeC {\'E}nergie g\IeC {\'e}othermique}{112}
\contentsline {section}{\numberline {7.10}\IeC {\'E}nergies non renouvelables}{112}
\contentsline {subsection}{\numberline {7.10.1}\IeC {\'E}nergie nucl\IeC {\'e}aire}{113}
\contentsline {subsubsection}{Fission}{113}
\contentsline {paragraph}{D\IeC {\'e}chets radioactifs}{114}
\contentsline {paragraph}{Accidents nucl\IeC {\'e}aires}{115}
\contentsline {subsubsection}{Fusion}{115}
\contentsline {subsection}{\numberline {7.10.2}\IeC {\'E}nergie de combustion~: p\IeC {\'e}trole et gaz}{115}
\contentsline {chapter}{\numberline {8}L'\IeC {\'e}nergie}{119}
\contentsline {section}{\numberline {8.1}Introduction}{119}
\contentsline {section}{\numberline {8.2}Le travail}{119}
\contentsline {subsection}{\numberline {8.2.1}Historiquement}{119}
\contentsline {subsection}{\numberline {8.2.2}D\IeC {\'e}finition}{120}
\contentsline {subsubsection}{Travail simple}{120}
\contentsline {subsubsection}{Travail et produit scalaire}{120}
\contentsline {subsubsection}{Travail cas g\IeC {\'e}n\IeC {\'e}ral}{120}
\contentsline {section}{\numberline {8.3}L'\IeC {\'e}nergie}{121}
\contentsline {subsection}{\numberline {8.3.1}Introduction}{121}
\contentsline {subsection}{\numberline {8.3.2}\IeC {\'E}nergie potentielle}{121}
\contentsline {subsection}{\numberline {8.3.3}\IeC {\'E}nergie cin\IeC {\'e}tique}{122}
\contentsline {subsection}{\numberline {8.3.4}\IeC {\'E}nergie m\IeC {\'e}canique}{122}
\contentsline {subsection}{\numberline {8.3.5}Exemple}{122}
\contentsline {section}{\numberline {8.4}Conservation de l'\IeC {\'e}nergie}{122}
\contentsline {subsection}{\numberline {8.4.1}Introduction}{122}
\contentsline {subsection}{\numberline {8.4.2}Th\IeC {\'e}or\IeC {\`e}me de conservation de l'\IeC {\'e}nergie m\IeC {\'e}canique}{122}
\contentsline {subsection}{\numberline {8.4.3}Exemples}{123}
\contentsline {section}{\numberline {8.5}Limite du th\IeC {\'e}or\IeC {\`e}me de conservation}{124}
\contentsline {section}{\numberline {8.6}Forces conservatives}{124}
\contentsline {subsection}{\numberline {8.6.1}D\IeC {\'e}finition}{124}
\contentsline {subsection}{\numberline {8.6.2}Exemples}{124}
\contentsline {subsubsection}{Le poids}{124}
\contentsline {subsubsection}{Le ressort}{125}
\contentsline {subsection}{\numberline {8.6.3}G\IeC {\'e}n\IeC {\'e}ralisation}{125}
\contentsline {subsubsection}{\IeC {\'E}nergie gravifique}{126}
\contentsline {chapter}{\numberline {9}Physique th\IeC {\'e}orique}{129}
\contentsline {section}{\numberline {9.1}Introduction}{129}
\contentsline {section}{\numberline {9.2}\IeC {\'E}nergie potentielle}{129}
\contentsline {section}{\numberline {9.3}\IeC {\'E}nergie cin\IeC {\'e}tique}{129}
\contentsline {section}{\numberline {9.4}Quantit\IeC {\'e} de mouvement}{129}
\contentsline {section}{\numberline {9.5}Conservation de l'\IeC {\'e}nergie}{130}
\contentsline {section}{\numberline {9.6}Principe de moindre action}{130}
\contentsline {subsection}{\numberline {9.6.1}Lagrangien}{130}
\contentsline {subsection}{\numberline {9.6.2}Action}{130}
\contentsline {subsection}{\numberline {9.6.3}Minimalisation}{131}
\contentsline {section}{\numberline {9.7}Euler-Lagrange}{131}
\contentsline {chapter}{\numberline {10}Thermodynamique}{133}
\contentsline {section}{\numberline {10.1}Introduction}{133}
\contentsline {section}{\numberline {10.2}Temp\IeC {\'e}rature}{133}
\contentsline {subsection}{\numberline {10.2.1}Celsius}{133}
\contentsline {subsection}{\numberline {10.2.2}Fahrenheit}{133}
\contentsline {subsection}{\numberline {10.2.3}Kelvin}{134}
\contentsline {subsection}{\numberline {10.2.4}Agitation mol\IeC {\'e}culaire}{134}
\contentsline {section}{\numberline {10.3}Dilatation}{134}
\contentsline {section}{\numberline {10.4}Chaleur}{135}
\contentsline {subsection}{\numberline {10.4.1}Chaleur sp\IeC {\'e}cifique}{135}
\contentsline {subsection}{\numberline {10.4.2}Chaleur latente}{135}
\contentsline {section}{\numberline {10.5}\IeC {\'E}nergie thermique}{136}
\contentsline {subsection}{\numberline {10.5.1}Premier principe}{137}
\contentsline {chapter}{\numberline {11}Thermodynamique}{139}
\contentsline {section}{\numberline {11.1}Temp\IeC {\'e}rature et dilatation}{139}
\contentsline {subsection}{\numberline {11.1.1}Temp\IeC {\'e}rature}{139}
\contentsline {subsection}{\numberline {11.1.2}Dilatation}{139}
\contentsline {section}{\numberline {11.2}Chaleurs sp\IeC {\'e}cifique et latente}{141}
\contentsline {subsection}{\numberline {11.2.1}Introduction}{141}
\contentsline {subsection}{\numberline {11.2.2}Chaleur sp\IeC {\'e}cifique}{141}
\contentsline {subsubsection}{Chaleur massique}{141}
\contentsline {subsubsection}{Capacit\IeC {\'e} thermique}{142}
\contentsline {subsubsection}{Notion de mole}{142}
\contentsline {subsubsection}{Chaleur molaire}{142}
\contentsline {subsubsection}{Relation entre chaleur massique et molaire}{143}
\contentsline {subsubsection}{Chaleur latente}{143}
\contentsline {subsubsection}{\IeC {\'E}vaporation}{143}
\contentsline {subsection}{\numberline {11.2.3}Bilan thermique}{144}
\contentsline {section}{\numberline {11.3}Loi des gaz parfaits}{145}
\contentsline {subsection}{\numberline {11.3.1}\IeC {\'E}quation d'\IeC {\'e}tat}{145}
\contentsline {subsubsection}{Pression}{145}
\contentsline {subsubsection}{\IeC {\'E}tat d'un gaz parfait}{145}
\contentsline {subsubsection}{Approche intuitive}{145}
\contentsline {subsubsection}{Approche mol\IeC {\'e}culaire}{146}
\contentsline {subsection}{\numberline {11.3.2}Gaz parfait}{147}
\contentsline {section}{\numberline {11.4}Premier principe}{147}
\contentsline {subsection}{\numberline {11.4.1}Chaleur}{147}
\contentsline {subsection}{\numberline {11.4.2}Travail}{148}
\contentsline {subsection}{\numberline {11.4.3}\IeC {\'E}nergie interne}{148}
\contentsline {subsubsection}{\IeC {\'E}quipartition de l'\IeC {\'e}nergie}{149}
\contentsline {subsection}{\numberline {11.4.4}Premier principe}{149}
\contentsline {subsection}{\numberline {11.4.5}Changements d'\IeC {\'e}tats}{149}
\contentsline {subsubsection}{Transformation isobare}{150}
\contentsline {subsubsection}{Transformation isochore}{150}
\contentsline {subsubsection}{Transformation isotherme}{151}
\contentsline {subsubsection}{Transformation adiabatique}{151}
\contentsline {subsection}{\numberline {11.4.6}Chaleurs sp\IeC {\'e}cifiques}{153}
\contentsline {section}{\numberline {11.5}Machines thermiques}{154}
\contentsline {subsection}{\numberline {11.5.1}Machine simple}{154}
\contentsline {subsection}{\numberline {11.5.2}Moteur \IeC {\`a} explosion}{156}
\contentsline {subsection}{\numberline {11.5.3}Moteur Diesel}{158}
\contentsline {subsection}{\numberline {11.5.4}Machine de Stirling}{160}
\contentsline {subsection}{\numberline {11.5.5}Climatiseur}{160}
\contentsline {subsection}{\numberline {11.5.6}R\IeC {\'e}frig\IeC {\'e}rateur}{160}
\contentsline {subsection}{\numberline {11.5.7}Pompe \IeC {\`a} chaleur}{160}
\contentsline {subsection}{\numberline {11.5.8}Cycle de Carnot}{160}
\contentsline {section}{\numberline {11.6}Thermodynamique statistique}{161}
\contentsline {section}{\numberline {11.7}Second principe}{162}
\contentsline {chapter}{\numberline {A}Syst\IeC {\`e}mes d'unit\IeC {\'e}s}{163}
\contentsline {section}{\numberline {A.1}Introduction}{163}
\contentsline {section}{\numberline {A.2}Op\IeC {\'e}rateur d'unit\IeC {\'e}s}{163}
\contentsline {section}{\numberline {A.3}Analyse dimensionnelle}{164}
\contentsline {section}{\numberline {A.4}Les unit\IeC {\'e}s du Syst\IeC {\`e}me International}{165}
\contentsline {subsection}{\numberline {A.4.1}Exemple}{165}
\contentsline {section}{\numberline {A.5}Conversions}{165}
\contentsline {section}{\numberline {A.6}Sous-multiples}{166}
\contentsline {section}{\numberline {A.7}Notation scientifique}{166}
\contentsline {section}{\numberline {A.8}R\IeC {\`e}gles de calcul}{167}
\contentsline {chapter}{\numberline {B}Deux syst\IeC {\`e}mes de coordonn\IeC {\'e}es}{169}
\contentsline {section}{\numberline {B.1}Le syst\IeC {\`e}me de coordonn\IeC {\'e}es circulaires}{169}
\contentsline {subsection}{\numberline {B.1.1}Introduction}{169}
\contentsline {subsection}{\numberline {B.1.2}Description}{169}
\contentsline {section}{\numberline {B.2}Coordonn\IeC {\'e}es sph\IeC {\'e}riques}{169}
\contentsline {subsection}{\numberline {B.2.1}Introduction}{169}
\contentsline {subsection}{\numberline {B.2.2}Description}{169}
\contentsline {subsection}{\numberline {B.2.3}Latitude et longitude}{170}
\contentsline {chapter}{\numberline {C}Mesures de distances}{171}
\contentsline {section}{\numberline {C.1}La taille de la Terre}{171}
\contentsline {subsection}{\numberline {C.1.1}Le principe}{171}
\contentsline {subsection}{\numberline {C.1.2}Techniquement}{172}
\contentsline {section}{\numberline {C.2}La taille de la Lune}{173}
\contentsline {section}{\numberline {C.3}La distance Terre-Lune}{173}
\contentsline {section}{\numberline {C.4}La distance Terre-Soleil}{174}
\contentsline {section}{\numberline {C.5}La distance des \IeC {\'e}toiles}{176}
\contentsline {chapter}{\numberline {D}Travaux pratiques}{177}
\contentsline {section}{\numberline {D.1}Le rapport de laboratoire}{177}
\contentsline {subsection}{\numberline {D.1.1}Plan d'un rapport de travail pratique}{178}
\contentsline {subsubsection}{Pr\IeC {\'e}liminaires}{178}
\contentsline {subsubsection}{R\IeC {\'e}sum\IeC {\'e}}{178}
\contentsline {subsubsection}{But}{178}
\contentsline {subsubsection}{Th\IeC {\'e}orie}{178}
\contentsline {subsubsection}{Description de l'exp\IeC {\'e}rience}{178}
\contentsline {subsubsection}{R\IeC {\'e}sultats}{178}
\contentsline {subsubsection}{Discussion}{180}
\contentsline {subsubsection}{Conclusion}{180}
\contentsline {subsubsection}{Annexes}{180}
\contentsline {section}{\numberline {D.2}La n\IeC {\'e}buleuse du Crabe}{181}
\contentsline {subsection}{\numberline {D.2.1}Introduction}{181}
\contentsline {subsection}{\numberline {D.2.2}But du travail pratique}{181}
\contentsline {subsection}{\numberline {D.2.3}Dispositif exp\IeC {\'e}rimental}{181}
\contentsline {subsection}{\numberline {D.2.4}Mesures}{181}
\contentsline {subsection}{\numberline {D.2.5}R\IeC {\'e}sultats}{181}
\contentsline {subsection}{\numberline {D.2.6}Analyse}{181}
\contentsline {section}{\numberline {D.3}Le pendule simple}{181}
\contentsline {subsection}{\numberline {D.3.1}Les mesures}{181}
\contentsline {subsection}{\numberline {D.3.2}Organisation des donn\IeC {\'e}es et graphiques}{182}
\contentsline {section}{\numberline {D.4}Mouvement simple~: MRU}{182}
\contentsline {subsection}{\numberline {D.4.1}Les mesures}{182}
\contentsline {subsection}{\numberline {D.4.2}Organisation des donn\IeC {\'e}es et graphiques}{182}
\contentsline {subsection}{\numberline {D.4.3}Analyse des r\IeC {\'e}sultats}{182}
\contentsline {section}{\numberline {D.5}Mouvement simple~:\\MRUA}{182}
\contentsline {subsection}{\numberline {D.5.1}But}{182}
\contentsline {subsection}{\numberline {D.5.2}Th\IeC {\'e}orie}{183}
\contentsline {subsection}{\numberline {D.5.3}Les mesures}{183}
\contentsline {subsection}{\numberline {D.5.4}Organisation des donn\IeC {\'e}es et graphiques}{183}
\contentsline {subsection}{\numberline {D.5.5}Galil\IeC {\'e}e et le plan inclin\IeC {\'e}}{183}
\contentsline {section}{\numberline {D.6}La chute libre}{183}
\contentsline {subsection}{\numberline {D.6.1}Cette exp\IeC {\'e}rience donnant lieu \IeC {\`a} un rapport not\IeC {\'e}, elle n'est pas d\IeC {\'e}crite.}{183}
\contentsline {subsection}{\numberline {D.6.2}R\IeC {\'e}sultats}{183}
\contentsline {section}{\numberline {D.7}Le canon horizontal}{183}
\contentsline {section}{\numberline {D.8}Le chariot \IeC {\`a} masse pendante}{184}
\contentsline {chapter}{\numberline {E}Rotations}{185}
\contentsline {section}{\numberline {E.1}Rotation de la Terre sur elle-m\IeC {\^e}me}{185}
\contentsline {section}{\numberline {E.2}Rotation de la Terre autour du Soleil}{185}
\contentsline {section}{\numberline {E.3}Rotation du Soleil dans la Voie Lact\IeC {\'e}e}{187}
\contentsline {section}{\numberline {E.4}Vitesse et r\IeC {\'e}f\IeC {\'e}rentiel}{187}
\contentsline {chapter}{\numberline {F}MRUA d\IeC {\'e}veloppements}{189}
\contentsline {section}{\numberline {F.1}La position}{189}
\contentsline {section}{\numberline {F.2}Une autre relation bien pratique}{189}
\contentsline {subsection}{\numberline {F.2.1}Cin\IeC {\'e}matique}{189}
\contentsline {subsection}{\numberline {F.2.2}\IeC {\'E}nergie}{190}
\contentsline {chapter}{\numberline {G}Chute de la Lune}{191}
\contentsline {section}{\numberline {G.1}Introduction}{191}
\contentsline {section}{\numberline {G.2}Acc\IeC {\'e}l\IeC {\'e}ration}{191}
\contentsline {section}{\numberline {G.3}Force de gravitation}{192}
\contentsline {chapter}{\numberline {H}Satellite en orbite g\IeC {\'e}ostationnaire}{193}
\contentsline {section}{\numberline {H.1}Introduction}{193}
\contentsline {section}{\numberline {H.2}Th\IeC {\'e}oriquement}{193}
\contentsline {section}{\numberline {H.3}Num\IeC {\'e}riquement}{194}
\contentsline {section}{\numberline {H.4}Loi de Kepler}{194}
\contentsline {chapter}{\numberline {I}Relativit\IeC {\'e}}{195}
\contentsline {section}{\numberline {I.1}Relativit\IeC {\'e} galil\IeC {\'e}enne}{195}
\contentsline {section}{\numberline {I.2}Transformation galil\IeC {\'e}enne}{196}
\contentsline {section}{\numberline {I.3}Invariance}{197}
\contentsline {section}{\numberline {I.4}Forces inertielles}{197}
\contentsline {subsection}{\numberline {I.4.1}Force d'inertie}{197}
\contentsline {subsection}{\numberline {I.4.2}Force centrifuge}{198}
\contentsline {chapter}{\numberline {J}Mar\IeC {\'e}es}{199}
\contentsline {section}{\numberline {J.1}Introduction}{199}
\contentsline {section}{\numberline {J.2}Centre de gravit\IeC {\'e}}{199}
\contentsline {section}{\numberline {J.3}Force d'inertie}{200}
\contentsline {subsection}{\numberline {J.3.1}Vitesse angulaire}{200}
\contentsline {subsection}{\numberline {J.3.2}Force d'inertie}{200}
\contentsline {section}{\numberline {J.4}Poids relatif}{201}
\contentsline {section}{\numberline {J.5}Analyse diff\IeC {\'e}rentielle}{201}
\contentsline {section}{\numberline {J.6}Autres rythmes}{201}
\contentsline {subsection}{\numberline {J.6.1}D\IeC {\'e}calages}{202}
\contentsline {subsection}{\numberline {J.6.2}Mar\IeC {\'e}es de vives et mortes eaux}{202}
\contentsline {subsection}{\numberline {J.6.3}Mar\IeC {\'e}es d'\IeC {\'e}quinoxes}{202}
\contentsline {subsection}{\numberline {J.6.4}Mar\IeC {\'e}es de p\IeC {\'e}rig\IeC {\'e}e et p\IeC {\'e}rih\IeC {\'e}lie}{203}
\contentsline {subsection}{\numberline {J.6.5}Mar\IeC {\'e}es de d\IeC {\'e}clinaison}{203}
\contentsline {subsection}{\numberline {J.6.6}Retards et mar\IeC {\'e}es c\IeC {\^o}ti\IeC {\`e}res}{203}
\contentsline {section}{\numberline {J.7}Limite de Roche}{203}
\contentsline {subsection}{\numberline {J.7.1}Mod\IeC {\`e}le simplifi\IeC {\'e}}{204}
\contentsline {subsection}{\numberline {J.7.2}Exemples}{205}
\contentsline {chapter}{\numberline {K}\IeC {\'E}nergies}{207}
\contentsline {section}{\numberline {K.1}Introduction}{207}
\contentsline {section}{\numberline {K.2}\IeC {\'E}nergie hydraulique}{207}
\contentsline {section}{\numberline {K.3}\IeC {\'E}nergie \IeC {\'e}olienne}{208}
\contentsline {subsection}{\numberline {K.3.1}R\IeC {\`e}gle de Betz}{208}
\contentsline {subsection}{\numberline {K.3.2}\IeC {\'E}oliennes}{209}
\contentsline {subsubsection}{\IeC {\'E}olienne de Collonges-Dor\IeC {\'e}naz}{209}
\contentsline {subsubsection}{\IeC {\'E}oliennes du Mont Soleil (Jura suisse)}{209}
\contentsline {section}{\numberline {K.4}G\IeC {\'e}othermie}{209}
\contentsline {section}{\numberline {K.5}\IeC {\'E}nergie de combustion des d\IeC {\'e}chets}{210}
\contentsline {chapter}{\numberline {L}Exercices}{211}
\contentsline {section}{\numberline {L.1}Probl\IeC {\`e}mes}{211}
\contentsline {subsection}{\numberline {L.1.1}Relatifs \IeC {\`a} la conversion d'unit\IeC {\'e}s et \IeC {\`a} la notation scientifique}{211}
\contentsline {subsection}{\numberline {L.1.2}Relatifs aux notions de d\IeC {\'e}placement, position et distance parcourue}{211}
\contentsline {subsection}{\numberline {L.1.3}Relatifs \IeC {\`a} la notion de vitesse}{212}
\contentsline {subsection}{\numberline {L.1.4}Relatif \IeC {\`a} la notion d'acc\IeC {\'e}l\IeC {\'e}ration}{212}
\contentsline {subsection}{\numberline {L.1.5}Relatif au MRU}{213}
\contentsline {subsection}{\numberline {L.1.6}Relatif au MRUA}{213}
\contentsline {subsection}{\numberline {L.1.7}Relatifs \IeC {\`a} la physique aristot\IeC {\'e}licienne}{214}
\contentsline {subsection}{\numberline {L.1.8}Relatifs \IeC {\`a} la physique newtonienne}{214}
\contentsline {subsection}{\numberline {L.1.9}Relatifs aux forces}{216}
\contentsline {subsection}{\numberline {L.1.10}Relatifs \IeC {\`a} l'\IeC {\'e}nergie}{218}
\contentsline {subsection}{\numberline {L.1.11}Relatifs \IeC {\`a} la conservation de l'\IeC {\'e}nergie}{218}
\contentsline {subsection}{\numberline {L.1.12}Relatifs \IeC {\`a} l'\IeC {\'e}nergie hydraulique}{219}
\contentsline {subsection}{\numberline {L.1.13}Relatifs \IeC {\`a} l'\IeC {\'e}nergie \IeC {\'e}olienne}{219}
\contentsline {subsection}{\numberline {L.1.14}Relatifs \IeC {\`a} l'\IeC {\'e}nergie solaire}{219}
\contentsline {section}{\numberline {L.2}Solutions}{220}
\contentsline {section}{\numberline {L.3}Solutions OS}{235}
\contentsline {chapter}{\numberline {M}Ordre de grandeur, erreur et incertitudes}{247}
\contentsline {section}{\numberline {M.1}Ordre de grandeur}{247}
\contentsline {subsection}{\numberline {M.1.1}Chiffres significatifs}{247}
\contentsline {subsection}{\numberline {M.1.2}Ordre de grandeur}{247}
\contentsline {section}{\numberline {M.2}\IeC {\'E}cart et erreur}{248}
\contentsline {section}{\numberline {M.3}Incertitude}{250}
\contentsline {subsection}{\numberline {M.3.1}Addition/soustraction}{250}
\contentsline {subsection}{\numberline {M.3.2}Multiplication par un entier}{251}
\contentsline {subsection}{\numberline {M.3.3}Multiplication/division}{251}
\contentsline {subsection}{\numberline {M.3.4}Puissance}{252}
\contentsline {subsection}{\numberline {M.3.5}R\IeC {\'e}sum\IeC {\'e}}{252}
\contentsline {subsection}{\numberline {M.3.6}Exemples}{253}
