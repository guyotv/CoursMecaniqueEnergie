\myclearpage

\chapter{Relativité\index{relativite@relativité}}\label{relativite}
\lettrine{L}{a notion de relativité} est entourée d'une aura particulière. Elle évoque immanquablement Einstein\index{Einstein} et des idées entourées de mystère comme la dilatation du temps\index{dilatation du temps} ou la courbure de l'espace\index{courbure de l'espace}. Même si cette notion se comprend de nos jours à travers les relativités restreinte et générale d'Einstein, elle est déjà présente chez Bruno\index{Bruno} au \siecle{16} et Galilée\index{Galilee@Galilée} au \siecle{17}. On parle aujourd'hui de relativité galiléenne\index{relativite@relativité!galiléenne} pour faire la différence avec celles d'Einstein. Dans tous les cas, cette notion nous mène à considérer des changements de référentiels et à étudier le changement de forme des lois de la physique à travers ceux-ci. Sans aborder les relativités restreinte et générale d'Einstein, qui traitent des lois de l'électromagnétisme et de la gravitation à travers ces changements de référentiels, on va s'intéresser ici à la notion de relativité galiléenne et à ses implications pour la physique newtonienne.

\section{Relativité galiléenne}
Deux textes historiques fondent l'idée de relativité. Le plus connu est celui de Galileo Galilei (Galilée) :
\begin{quotation}
``\textit{Enfermez-vous avec un ami dans la cabine principale à l'intérieur d'un grand bateau et prenez avec vous des mouches, des papillons et d'autres petits animaux volants. Prenez une grande cuve d'eau avec un poisson dedans, suspendez une bouteille qui se vide goutte à goutte dans un grand récipient en dessous d'elle. Avec le bateau à l'arrêt, observez soigneusement comment les petits animaux volent à des vitesses égales vers tous les côtés de la cabine. Le poisson nage indifféremment dans toutes les directions, les gouttes tombent dans le récipient en dessous, et si vous lancez quelque chose à votre ami, vous n'avez pas besoin de le lancer plus fort dans une direction que dans une autre, les distances étant égales, et si vous sautez à pieds joints, vous franchissez des distances égales dans toutes les directions. Lorsque vous aurez observé toutes ces choses soigneusement (bien qu'il n'y ait aucun doute que lorsque le bateau est à l'arrêt, les choses doivent se passer ainsi), faites avancer le bateau à l'allure qui vous plaira, pour autant que la vitesse soit uniforme [c'est-à-dire constante] et ne fluctue pas de part et d'autre. Vous ne verrez pas le moindre changement dans aucun des effets mentionnés et même aucun d'eux ne vous permettra de dire si le bateau est en mouvement ou à l'arrêt~\dots}'' Galileo Galilei, dans ``Dialogue concernant les deux plus grands systèmes du monde'', deuxième journée, 1632.

\footnotesize{\cite[p. 400.]{GC88}}
\end{quotation}
Galilée décrit tout d'abord un changement de référentiel particulier. Il s'agit de passer d'un lieu immobile sur la terre ferme à la cabine d'un navire se déplaçant uniformément, c'est-à-dire à vitesse constante\index{vitesse!constante}. Il n'est donc pas question d'un référentiel en accélération par exemple. On parlera donc de relativité restreinte\index{relativite@relativité!restreinte}.
Puis, Galilée compare les mouvements dans ces deux référentiels et conclut qu'il n'y a pas de moyens de les différencier. L'explication actuelle complète et étend ce résultat qui constitue l'idée de relativité. Elle s'exprime par :

\begin{quotation}
Les lois de la physique sont formellement les mêmes dans deux référentiels en translation à vitesse constante l'un par rapport à l'autre.
\end{quotation}

Cette idée, au contraire de dire que ``tout est relatif'', affirme que la forme des lois de la physique est préservée par ce type de changement de référentiel. Elle en assure la généralité pour des observateurs en mouvement rectiligne uniforme les uns par rapport aux autres.

Le texte le plus ancien formulant clairement l'idée de relativité à pour auteur Giordano Bruno.
\begin{quotation}
``\textit{Si nous supposons une personne sur un bateau en mouvement au milieu des flots, celui qui ne sait pas que l'eau est en mouvement et qui ne voit pas la terre ferme, ne sera pas conscient du mouvement du bateau . Pour cette raison, j'en viendrai à mettre en doute la quiétude et la fixité de notre Terre. Et je suis en mesure de croire que si j'étais sur le Soleil, la Lune ou sur une autre étoile, je m'imaginerais toujours au centre du monde sans mouvement autour duquel semblerait tourner l'univers entier, bien qu'en vérité le corps contenant sur lequel je me trouve serait en train de tourner sur lui-même. Ainsi je ne puis en rien être certain de ce qui distingue un corps mobile d'un corps stable.}'' Giordano Bruno, dans ``L'Infini, l'univers et les mondes'', troisième dialogue, 1584.

\footnotesize{\cite[p. 170.]{JR00}}
\end{quotation}
Bruno\index{Bruno}, bien avant Galilée avait posé très clairement les bases du principe de relativité. Et Bruno alla plus loin que Galilée en postulant la pluralité des mondes\index{pluralite@pluralité des mondes}. Cela lui coûta la vie puisqu'il fut brûlé vif en 1600.

\section{Transformation galiléenne\index{transformation!galiléenne}}
Le changement de référentiel sur lequel se base la relativité restreinte de Galilée (ou de Bruno) a de nos jours une formulation mathématique précise. Pour la découvrir, considérons la figure \ref{transgalilee} qui présente deux référentiels \(R\) et \(R'\) dont l'un, \(R'\), se déplace en ligne droite à vitesse constante \(v_{ref}\) par rapport à l'autre. Pour simplifier les calculs, le déplacement de \(R'\) se fait selon l'axe \(x\) du référentiel \(R\).

\begin{figure}[t]
\centering
\caption[Transformation de galilée]{Transformation de Galilée\label{transgalilee} \par \scriptsize{Selon l'axe \(x\).}}
\includegraphics[width=6cm]{TransGalilee.eps}
\end{figure}

On peut donner la position du point \(P\) dans chaque référentiel à l'aide des coordonnées \(x\) et \(x'\). Si on admet, pour simplifier, que les deux référentiels avaient à \(t=\unit{0}{\second}\) la même origine, la position de l'origine du référentiel \(R'\) à un instant \(t\) par rapport à \(R\) vaut alors \(v_{ref}\cdot t\). On peut alors écrire pour le point \(P\) :
\begin{equation}\label{eqtransgalilee}
\fbox{$\displaystyle x'=x-v_{ref}\cdot t$}
\end{equation}
L'équation \ref{eqtransgalilee} constitue la transformation de Galilée, sous sa forme la plus simplifiée. Il faut cependant lui ajouter une équation ici évidente :
\[t'=t\]
qui postule l'invariance du temps\index{invariance!du temps} par changement de référentiel.

\medskip
On peut déduire de l'équation \ref{eqtransgalilee} un corollaire important : le théorème d'addition des vitesses\index{theoreme@théorème!d'addition des vitesses}. Considérons le déplacement du point \(P\) le long de l'axe \(x\). Notons \(x_f\) la position finale et \(x_i\) la position initiale du point \(P\). En notant \(v\) la vitesse du point \(P\), en utilisant le ' pour les grandeurs dans le référentiel \(R'\) et avec la transformation de Galilée, on a :
\begin{align}
v'&=\frac{x'_f-x'_i}{t'_f-t'_i}=\frac{(x_f-v_{ref}\cdot t_f)-(x_i-v_{ref}\cdot t_i}{t_f-t_i}\nonumber\\
&=\frac{x_f-x_i-v_{ref}\cdot (t_f-t_i)}{t_f-t_i}=\frac{x_f-x_i}{t_f-t_i}-v_{ref}\nonumber\\
v'&=v-v_{ref}\label{thmaddvit}
\end{align}
L'expression \ref{thmaddvit} constitue le théorème d'addition des vitesses. Il traduit l'idée que la vitesse d'un point \(P\) dans le référentiel \(R'\) correspond à sa vitesse dans le référentiel \(R\) à laquelle on soustrait la vitesse du référentiel \(R'\) lui-même. Plus simplement, en inversant l'équation \ref{thmaddvit}, on a :
\begin{equation}\label{thmaddvit2}
\fbox{$\displaystyle v=v'+v_{ref}$}
\end{equation}
L'équation \ref{thmaddvit2} se comprend ainsi : par rapport au sol, la vitesse du passager d'un train correspond à sa vitesse par rapport au train à laquelle on ajoute la vitesse du train lui-même (par rapport au sol).

\section{Invariance\index{invariance}}
La transformation de Galilée permet maintenant d'étudier précisément le changement des lois de la physique par changement de référentiel. La transformation de Galilée se restreint aux référentiels en MRU\index{MRU} les uns par rapport aux autres. La relativité de Galilée\index{relativite@relativité!de Galilée} utilisant implicitement cette transformation est donc une relativité restreinte\index{relativite@relativité!restreinte}. Voyons donc dans quelle mesure la transformation de Galilée modifie la seconde loi de Newton.

\medskip
Comme précédemment, notons avec un indice $_i$ l'instant initial et un indice $_f$ l'instant final et calculons la force \(F'\) exercée sur un objet de masse m dont l'accélération dans le référentiel \(R'\) vaut \(a'\) :
\begin{align*}
F'&=m\cdot a'=m\cdot \frac{v'_f-v'_i}{t'_f-t'_i}\\
&=m\cdot \frac{(v_f-v_{ref})-(v_i-v_{ref})}{t_f-t_i}\\
&=m\cdot \frac{v_f-v_i}{t_f-t_i}=m\cdot a=F
\end{align*}
On utilise l'équation \ref{thmaddvit} pour changer de référentiel et le fait qu'en relativité galiléenne la masse \(m\) est invariante par changement de référentiel. Finalement, on obtient :
\begin{equation}
\fbox{$\displaystyle F'=m\cdot a'=m\cdot a=F$}
\end{equation}
Ainsi, la seconde loi\index{seconde loi} de Newton s'écrit de la même manière dans les deux référentiels \(R\) et \(R'\). On dit qu'elle est formellement invariante\index{invariance!formelle} par changement de référentiel. En d'autres termes, aucune expérience vérifiant la seconde loi de Newton ne peut permettre de faire la distinction entre les référentiels \(R\) et \(R'\). Pour Bruno\index{Bruno}, cela s'exprime par : ``Ainsi je ne puis en rien être certain de ce qui distingue un corps mobile d'un corps stable.'' et pour Galilée, ``\dots aucun des effets mentionnés [\dots] ne vous permettra de dire si le bateau est en mouvement ou à l'arrêt~\dots'' comme on l'a vu dans les citations ci-dessus.

Les lois de la physique sont donc formellement les mêmes dans les deux référentiels et rien ne permet de dire que l'un est en mouvement et l'autre pas.

\section{Forces inertielles\index{force!inertielle}}
Comme cela a été dit plus haut, l'invariance\index{invariance} de la seconde loi de Newton n'existe que dans le cadre de la relativité restreinte\index{relativite@relativité!restreinte}, pour des référentiels dits \emph{inertiels}\index{referentiel@référentiel!inertiel}, c'est-à-dire se déplaçant les uns par rapport aux autres en mouvement rectiligne uniforme\index{mouvement!rectiligne uniforme}. Mais qu'en est-il quand on considère d'autres référentiels qui ne se déplacent pas l'un par rapport à l'autre en ligne droite et à vitesse constante ? Dans le cas de référentiels en rotation par exemple, la seconde loi de Newton est-elle vraiment caduque ?

Il ne s'agit pas ici de développer la dynamique de tels référentiels. Seuls deux aspects qu'on évoque parfois, sans vraiment savoir ce dont il s'agit, vont être abordés. Il s'agit du concept de force d'inertie\index{force!d'inertie} et de celui de force centrifuge\index{force!centrifuge}.

\subsection{Force d'inertie\index{force!d'inertie}}
La notion de force d'inertie est très complexe, tant au niveau mathématique (produit vectoriel) qu'au niveau conceptuel (voir le paragraphe \ref{forcecentrifuge}). L'idée est ici de montrer que la seconde loi\index{seconde loi} de Newton n'est pas invariante par un changement de référentiel non-inertiel\index{referentiel@référentiel!non inertiel}. En d'autres termes, pour décrire le mouvement d'une particule dans un référentiel tournant\index{referentiel@référentiel!tournant}, il faut soit ajouter des forces fictives\index{force!fictive} (point de vue de Newton) dont l'une est la force d'inertie\index{force!d'inertie}, soit interpréter celle-ci comme un champ de gravitation\index{champ!de gravitation} supplémentaire. On va présenter ici le premier point de vue.

\medskip
Considérons deux référentiels \(R\) et \(R'\) qui ne sont pas en MRU l'un par rapport à l'autre, mais dont le second, \(R'\), est en rotation à la vitesse angulaire \(\omega_{ref}\) par rapport au premier, \(R\). Contrairement à l'invariance\index{invariance} de l'accélération par une transformation de Galilée\index{transformation!de Galilée}, on montre (voir \cite[p. 202-204.]{GC88}) que l'accélération d'un objet dans le référentiel \(R'\) n'est pas la même que dans le référentiel \(R\). Plus précisément, l'accélération \(a\) dans le référentiel \(R\) vaut :
\begin{equation}\label{accobjetdsreftournant}
a=a'+a_{ref}
\end{equation}
où \(a'\) est l'accélération dans le référentiel \(R'\) et \(a_{ref}\) l'accélération du référentiel \(R'\) par rapport à \(R\).

\medskip
C'est là un changement fondamental qui restreint la portée de la relativité galiléenne\index{relativite@relativité!galiléenne} (et qui a mené Einstein vers la relativité générale\index{relativite@relativité!générale}).

\smallskip
Comme on a vu au paragraphe \ref{accmcu}, page \pageref{accmcu}, qu'un objet en rotation circulaire uniforme a une accélération centripète\index{acceleration@accélération!centripète}, c'est-à-dire dirigée vers le centre du cercle, donnée par :
\[a_c=\frac{v^2}{r}\]
où \(v\) est la vitesse linéaire de l'objet et \(r\) le rayon du cercle, on peut appliquer cette relation pour exprimer l'accélération \(a_{ref}\) de \(R'\) par rapport à \(R\) à l'aide de la vitesse \(v\) d'un point du référentiel \(R'\) situé à une distance \(r\) de l'axe de rotation. De plus, l'équation \ref{vomegar} permet d'exprimer cette accélération en fonction de la vitesse angulaire \(\omega_{ref}\). On a donc :
\begin{equation}\label{accreftournant}
a_{ref}=\frac{v^2}{r}=\frac{(\omega\cdot r)^2}{r}=\omega^2\cdot r
\end{equation}
L'équation \ref{accobjetdsreftournant} de l'accélération d'un objet par rapport au référentiel \(R'\) peut alors s'écrire :
\begin{equation}\label{acctournantfinale}
a=a'+\omega^2\cdot r
\end{equation}
Les équations de la cinématique\index{cinematique@cinématique} ne sont donc pas préservées par un tel changement de référentiel (non inertiel).

\medskip
La dynamique n'est évidemment pas non plus préservée. L'accélération se trouvant au c\oe ur de la seconde loi de Newton, elle ne peut rester formellement invariante\index{invariance!formelle}. En effet, si elle s'écrit dans le référentiel \(R\) :
\begin{equation}\label{secondeR}
\sum F^{ext}=m\cdot a
\end{equation}


elle doit s'écrire dans le référentiel \(R'\) :
\begin{align}
\sum F'^{ext}&=m\cdot a'\nonumber\\
&=m\cdot (a-\omega^2\cdot r)\nonumber\\
&=m\cdot a-m\cdot\omega^2\cdot r\label{secondeR'}
\end{align}
Visiblement l'équation \ref{secondeR} est formellement différente de \ref{secondeR'}.

\medskip
On peut cependant modifier l'équation \ref{secondeR'} décrivant le mouvement dans le référentiel en rotation \(R'\) pour qu'elle corresponde à l'expression \ref{secondeR} donnée pour le référentiel \(R\) :
\begin{align}
\sum F'^{ext}+m\cdot\omega^2\cdot r&=m\cdot a\\
\sum F^{ext}=\sum F'^{ext}+F_{in}&=m\cdot a
\end{align}
En réinterprétant le terme correspondant à l'accélération du référentiel \(R'\) comme une force d'inertie\index{force!d'inertie} (\(F_{in}\)) qui s'additionne aux forces réellement exercées sur l'objet, on peut s'imaginer préserver la forme de la seconde loi de Newton. Bien entendu, ce n'est qu'une manière de masquer le problème, puisque pour le référentiel \(R'\) uniquement, il faut ajouter la force d'inertie. Et comme cette force n'a aucune réalité, comme aucun corps ne l'exerce, elle est considérée comme une force fictive\index{force!fictive}, une  pseudo-force\index{pseudo-force} qui n'a pas d'existence propre. On lui donne un nom : la force centrifuge\index{force!centrifuge}.

\subsection{Force centrifuge\index{force!centrifuge}}\label{forcecentrifuge}
Du point de vue de la mécanique newtonienne, la force centrifuge est donc une pseudo-force à n'introduire dans la seconde loi que pour des référentiels non inertiels\index{referentiel@référentiel!non inertiel} (c'est-à-dire accélérés). Dans le cadre d'une annexe portant sur la relativité, il convient de mentionner que cette force peut ne pas être vue comme une pseudo-force\index{pseudo-force}. Einstein va lui donner une ``réalité'' en la considérant comme une force de gravitation\index{force!de gravitation}\footnote{Le cas d'un pendule placé dans un train qui accélère permet de comprendre le lien entre l'inertie et la gravitation (voir \cite[p. 400-404.]{GC88})}. Cela va le mener à redéfinir la notion même de gravitation à travers la courbure de l'espace\index{courbure de l'espace}.