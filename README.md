Ce cours de physique pour débutant (DF : discipline fondamentale) et
simultanément avancé (OS : option spécifique) est écrit en LaTeX.

----

Pour le compiler, le paquet compressé fourni est suffisant.

----

D'autre part, pour choisir le cours débutant ou avancé, décommentez l'une ou
l'autre des lignes suivantes de ce même fichier (CoursMecaniqueOSDF.tex) :

%\usepackage[DF]{optional}

ou

\usepackage[OS]{optional}

Ici, par exemple, c'est le cours d'OS qui sera produit.