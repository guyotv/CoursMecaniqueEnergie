\myclearpage

\chapter{Satellite en orbite géostationnaire}\label{geostat}

\section{Introduction}

\lettrine{U}{n exemple} intéressant de l'utilisation de la seconde loi de Newton\index{seconde!loi!de Newton}, du mouvement circulaire uniforme\index{mouvement!circulaire!uniforme} et de la loi de la gravitation universelle\index{loi!de la gravitation universelle},
est donné par le calcul de l'altitude\index{altitude} nécessaire pour qu'un satellite\index{satellite} soit en orbite\index{orbite} géostationnaire\index{geostationnaire@géostationnaire}.

\section{Théoriquement}

On va donc utiliser les équations suivantes~:

\begin{center}
\begin{tabular}{ll}
\(F=m\cdot a\) &~: seconde loi de Newton \\ 
\(a=\frac{v^{2}}{R}\) &~: mouvement circulaire uniforme \\ 
\(F=G\cdot\frac{M\cdot m}{R^{2}}\) &~: loi de la gravitation universelle
\end{tabular}
\end{center}

De ces trois lois, on tire~:

\[G\cdot\frac{M_{T}\cdot m_{s}}{(R_{T}+h)^{2}}=m_{s}\cdot\frac{v^{2}}{(R_{T}+h)}\]

où~:

\begin{itemize}
\item G est la constante de la gravitation universelle,
\item \(M_{T}\) est la masse de la Terre,
\item \(m_{s}\) est la masse du satellite,
\item \(R_{T}\) est le rayon de la Terre,
\item \(h\) est l'altitude du satellite et
\item \(v\) est la vitesse linéaire du satellite.
\end{itemize}

Avec, par définition de la vitesse, pour une trajectoire circulaire~:

\begin{equation}\label{vitessesatgeostat}
v=\frac{2\cdot\pi\cdot(R_{T}+h)}{T}
\end{equation}

où~: \(T\) est la période\index{periode@période} du mouvement, c'est à dire le temps que doit mettre le satellite pour faire un tour autour de la Terre.

\bigskip
De là on tire (faites les calculs vous-même)~:

\[h=(\frac{G\cdot M_{T}\cdot T^{2}}{4\cdot\pi^{2}})^{\frac{1}{3}}-R_{T}\]

\begin{figure}[t]
\centering
\caption[Satellite]{Satellite\label{satellite} \par \scriptsize{En orbite géostationnaire\endnote{Voir le site de l'encyclopédie Wikipedia~: \url=http://fr.wikipedia.org/wiki/Image:Navstar-2.jpg= notamment pour le copyright de l'image. Remerciements à la NASA.}}}
\includegraphics[width=6cm]{Satellite.eps}
\end{figure}

\section{Numériquement}

Le calcul est simple~:
\begin{align*}
h=\;&(\frac{6,67\cdot10^{-11}\cdot5,97\cdot10^{24}\cdot(24\cdot60\cdot60)^{2}}{4\cdot\pi^{2}})^{\frac{1}{3}}\\
& -6,37\cdot10^{6}=\SI{35857}{\kilo\metre}
\end{align*}

Il s'agit de l'altitude des satellites en orbite géostationnaire au-dessus de l'équateur. Pour des latitudes plus élevées, on comprend bien que plus on monte vers le pôle, plus le satellite sera bas sur l'horizon. Il se peut même qu'ils soient sous l'horizon. C'est pourquoi d'autres types d'orbites sont nécessaires, comme l'orbite de Molniya, qui permet de couvrir à l'aide de plusieurs satellites les régions polaires vingt-quatre heures sur vingt-quatre.

\smallskip
L'équation \ref{vitessesatgeostat} permet alors de déterminer la vitesse du satellite sur son orbite. Pour un rayon de la terre \(R_T=\SI{6,37e6}{\metre}\) et une altitude \(h=\SI{35,857e6}{\metre}\), on a~:
\begin{align*}
v&=\frac{2\cdot\pi\cdot(6,37\cdot 10^6+35,857\cdot 10^6)}{24\cdot 60\cdot 60}\\
&= \SI{3071}{\metre\per\second}\cong\SI{3}{\kilo\metre\per\second}\cong\SI{11055}{\kilo\metre\per\hour}
\end{align*}


\section{Loi de Kepler}\label{keplergeostat}
Une autre manière de parvenir au même résultat consiste à utiliser la troisième loi de Kepler et la lune. En effet, selon l'équation \ref{keplertroisieme} de la page \pageref{keplertroisieme}, on peut écrire~:

\begin{align*}
\frac{R_{terre-lune}^3}{T_{lune}^2}&=\frac{(R_T+h)^3}{T_{sat}^2}\;\Rightarrow\\
R_T+h&=R_{terre-lune}\cdot \sqrt[3]{\frac{T_{sat}^2}{T_{lune}^2}}\\
&=384'404\cdot \sqrt[3]{\frac{24^2}{(27\cdot 24+8)^2}}\\
&=\SI{42364}{\kilo\metre}\,\Rightarrow\\
h&=42'364-R_T=\SI{35993}{\kilo\metre}
\end{align*}

Et on retrouve (aux arrondis près) le même résultat que précédemment.