\myclearpage

\chapter{Mesures de distances}
\lettrine{I}{l est ici question} de présenter quelques exemples de mesures de distances\index{mesure!de distance}. L'objectif est de se rendre compte à la fois de l'ingéniosité des raisonnements mis en \oe uvre et des difficultés techniques qui se sont présentées. Cela permet de travailler avec des ordres de grandeurs qui ne nous sont pas familiers et de mieux apprécier les connaissances qui se trouvent derrière les chiffres que les tables mettent à notre disposition.

\section{La taille de la Terre}\label{tailleterre}
La mesure du rayon de la Terre la plus connue, car très simple et historiquement très ancienne, est celle d'Érathosthène\index{Eratosthene@Ératosthène} (284-192 av. J.-C.). Le principe est simple, mais la mise en \oe uvre beaucoup moins évidente. Nous allons successivement voir chacune de ces deux étapes.

\subsection{Le principe}
Il faut tout d'abord relever que nous ne connaissons le travail d'Érathosthène qu'à travers d'autres auteurs (L'astronome Cléomède\index{Cleomede@Cléomède}, l'historien Strabon\index{Strabon} et le naturaliste romain Pline l'ancien\index{Pline l'Ancien}). Ces sources, aussi éminentes soient-elles, ne doivent pas être exemptes de toute critique, comme nous le verrons par la suite.

Le principe de la mesure est simple. Considérons la figure \ref{erathostenetailleterre}. On y voit la Terre dont l'axe de rotation est incliné par rapport à la perpendiculaire au plan de l'écliptique\index{ecliptique@écliptique}. On y voit l'ancienne ville de Syène\index{Syene@Syène}, c'est-à-dire l'actuelle Assouan\index{Assouan}, qui se trouve sur le tropique du Cancer\index{tropique!du Cancer}, au moment du solstice d'été\index{solstice!d'été}. Le Soleil se trouve alors au zénith\index{zenith@zénith}, c'est-à-dire à la perpendiculaire de l'horizon du lieu. Ses rayons pourraient alors pénétrer jusqu'au plus profond d'un puits construit verticalement. Au même moment, on peut considérer l'ombre au sol d'un gnomon (un bâton vertical) planté plus au nord à Alexandrie\index{Alexandrie}. Sa longueur et celle du gnomon\index{gnomon} permettent de déterminer l'angle \(\alpha\) de la figure \ref{erathostenetailleterre}. Or, celui-ci a un correspondant au centre de la Terre qui sous-tend l'arc de cercle entre Syène et Alexandrie. Si, par ailleurs, on connaît la mesure de cet arc, il est aisé de déterminer la circonférence de la Terre\index{circonference@circonférence!de la terre} et, ainsi, son rayon.

\begin{figure}[t]
\centering
\caption{Taille de la terre\label{erathostenetailleterre}}
\input{Annexe-MesuresDistances/Images/Erathostene.pst}
\end{figure}

Comme Érathosthène à évalué la distance entre Syène et Alexandrie à \SI{5000}{stades} et la longueur de l'ombre portée par le gnomon\index{gnomon} à \(1/8\) de sa hauteur, on peut en déduire~:
\begin{enumerate}
\item que l'angle \(\alpha\) est (anachroniquement car Érathosthène l'a simplement obtenu à partir de mesures faites avec un scaphé\index{scaphe@scaphé}\footnote{\label{scaphe}Le scaphé est une sorte de bol en forme de demi-sphère muni en son centre d'un gnomon, c'est-à-dire un petit stylet vertical.}) donné par~: \[\tan(\alpha)=\frac{1/8}{1}=1/8\;\Rightarrow\;\alpha=7,13^{\circ}=7^{\circ}8'\]
\item que la circonférence de la Terre vaut~: \[C=5000\cdot \frac{360}{7,13}=\SI{252631}{stades}\]
\item et que son rayon vaut alors, \[C=2\cdot \pi\cdot R\,\Rightarrow\;R=\frac{252'631}{2\cdot \pi}=\SI{40208}{stades}\]
\end{enumerate}
Évidemment, il faut savoir ce que vaut un stade\index{stade}. Et là apparaît le premier problème. En effet, les estimations varient entre 157,5 et \SI{192,27}{\metre}. Si on utilise la première valeur, on a~:
\[R=40'208\cdot 157,5=\SI{6333}{\kilo\metre}\]
Ce qui représente, par rapport à la valeur actuelle, une erreur de~:
\[e=\frac{6'378-6'333}{6'378}\cdot 100=0,7\%\]
alors qu'avec la seconde valeur, on a~:
\[R=40'208\cdot 192,27=\SI{7730}{\kilo\metre}\]
Ce qui représente une erreur de~:
\[e=\frac{7'730-6378}{6378}\cdot 100=21,2\%\]

\subsection{Techniquement}
On loue souvent la méthode d'Érathosthène pour la précision de sa mesure. Mais comme on ne connaît pas de manière certaine la valeur du stade, il faut être très prudent.

D'autant plus prudent que ce n'est pas le seul problème posé par la mesure d'Érathosthène\footnote{Les points ci-dessus sont aussi développés dans \cite[p. 1193-1196]{AS02}.}. Jean-Baptiste Joseph Delambre\index{Delambre} dans \cite[p. 92-96.]{JJD21} développe une critique de la mesure d'Érathosthène basée sur les points suivants~:
\begin{description}
\item[La mesure de l'angle], tout d'abord. On ne sait pas précisément comment Érathosthène a mesuré cet angle. Il a pu utiliser un scaphé\footnote{Op. cit. \ref{scaphe}} ou d'un gnomon de grande taille (environ cinq mètres). Évidemment, la mesure avec un gnomon de cinq mètres est plus précise que celle avec un scaphé de plus petite taille. Or, avec un gnomon de cinq mètres, en imaginant une précision de \(\pm \SI{3}{\milli\metre}\) sur l'ombre portée, on peut déterminer l'imprécision sur l'angle en considérant un triangle rectangle de côté adjacent valant cinq mètres et de côté opposé valant \SI{3}{\milli\metre}. Ainsi, on a~:
\[tan(\Delta \alpha)=\frac{3}{5000}\;\Rightarrow\;\Delta \alpha=0,034^{\circ}=2'\]
Avec un gnomon de \SI{1}{\metre}, l'incertitude angulaire, correspondant à \SI{3}{\milli\metre} sur l'ombre, vaut plus de \(10'\). Ce qui met en évidence la difficulté à réaliser la mesure.

D'autant plus que l'ombre du Soleil n'est pas nette en raison de son diamètre apparent. En effet, le diamètre apparent\index{diametre@diamètre!apparent} du Soleil, c'est-à-dire l'angle sous lequel on le voit, est de \(32'\). Cela signifie que par rapport à l'ombre d'un gnomon idéal pointant directement vers le centre d'un soleil ponctuel, l'ombre d'un gnomon réel sera \(15'\) plus longue (par un flou de l'extrémité de l'ombre), en raison des rayons provenant du bord du disque solaire.

Au minimum donc, l'erreur est de l'ordre de \(17'\). Or, cela correspond alors à une erreur sur le rayon de la Terre d'environ \(5\%\) dans le cas d'un stade à \SI{157,5}{\metre} et à \(26\%\) au maximum dans le cas d'un stade de \SI{192,27}{\metre} !
\item[La mesure de l'arc], ensuite. Les calculs précédents montrent que la valeur du stade est déterminante pour la qualité de la mesure effectuée par Érathosthène. Mais, la valeur de l'arc aussi. Car, outre le fait que notre connaissance de la valeur du stade\index{stade} est incertaine, il semble que les mesures de distances aient pu se faire en durée de marche. Par exemple, à une journée de marche correspondrait \SI{200}{stades}. Ce qui permet de comprendre l'incertitude de la mesure. Par ailleurs, Érathosthène a lui-même fait un arrondi significatif. Considérant la distance de \SI{250000}{stades} pour la circonférence de la Terre (obtenue avec un angle de \(7^{\circ}12'\)), il détermine la distance par degré~: \(250000/360=\SI{694,44}{stades}\) et \dots l'arrondit à \SI{700}{stades}. Il recalcule alors la circonférence terrestre\index{circonference@circonférence!de la terre} et obtient \(700\cdot 360=\SI{252000}{stades}\).
\end{description}
Si ce qui précède montre qu'il faut être très prudent avec cette mesure de la Terre, cela permet aussi de se rendre compte qu'une mesure est toujours liée à l'incertitude\index{incertitude@incertitude} des termes qui ont permis de l'obtenir. Sans l'évaluation de ces incertitudes, la mesure peut ne pas être significative. Bien entendu la méthode utilisée par Érathosthène est remarquable. Mais sa portée n'en reste pas moins limitée par la précision des mesures qu'elle utilise. Et, parallèlement, la portée historique de cette mesure est aussi à évaluer à l'aulne de ces incertitudes. C'est pourquoi, tout historien consciencieux ne peut se passer d'avoir une bonne connaissances des méthodes de mesures utilisées à l'époque et des moyens mathématiques nécessaires pour évaluer leur précision.

\section{La taille de la Lune}\label{taillelune}
Une première méthode simple consiste à observer une éclipse de Lune\index{eclipse@éclipse!de Lune}. On peut voir alors l'ombre de la Terre sur la Lune. En reproduisant un cercle qui épouse la forme de cette ombre, on peut déterminer le rapport de taille entre ces deux corps. La méthode paraît simple. Cependant, elle se complique quand on considère les deux problèmes suivants~:
\begin{itemize}
\item La distance du Soleil à la Terre est finie et le Soleil a une taille importante par rapport à la Terre. Ainsi, l'ombre portée par la Terre sur la Lune n'a pas exactement la taille de la Terre.
\item Sans photographie d'éclipses de Lune, il est très difficile de trouver le rapport de la taille de l'ombre de la Terre à celle de la Lune.
\end{itemize}
Même de nos jours, si on utilise des images trop petites, l'incertitude sur le rayon du cercle qui sous-tend l'ombre de la Terre sur la Lune est important. La figure \ref{tailledelalune} montre en effet, suivant l'image choisie, un rapport de \(95/28,9=3,3\) à \(60/28,9=2,1\).

\begin{figure}[ht]
\centering
\caption[Taille de la lune]{Taille de la lune\label{tailledelalune} \par \scriptsize{Une taille incertaine\endnote{Voir le site de l'encyclopédie Wikipedia~: \url=http://commons.wikimedia.org/wiki/Image:Eclipse_lune.jpg=. notamment pour le copyright de l'image. Remerciements à son auteur Luc Viatour.}}}
\includegraphics[width=6cm]{RayonLune.eps}
\end{figure}

Et cela sans tenir compte du premier point mentionné ci-dessus.

Cependant, cette méthode permet une première évaluation de la taille de la Lune. Pour cela, il faut encore tenir compte du premier point évoqué ci-dessus. On peut le faire de manière très rapide en constatant que lors des éclipses de Soleil\index{eclipse@éclipse!de Soleil}, l'ombre portée par la Lune sur la Terre est très petite. On estime que sur la distance Terre-Lune l'ombre de la Lune est réduite d'environ 70\%. On peut donc supposer qu'il en va de même pour l'ombre de la Terre portée sur la Lune lors d'éclipses de Lune\index{eclipse@éclipse!de Lune}. Ainsi, en prenant un rapport de diamètre de l'ombre de la Terre sur la Lune à la taille de la Lune de \(2,5\,\times\) environ, le rapport de la taille de la Terre (et non de son ombre sur la Lune) à celle de la Lune vaut \(3,5\,\times\) (car \(0,7^{-1}=1,4\) et \(2,5\cdot 1,4=3,5\,\times\)).

On peut alors calculer la taille de la Lune à partir de la mesure du rayon terrestre d'Érathosthène. Si on admet qu'il ait pu obtenir une valeur du rayon de la Terre \(R=\SI{6333}{\kilo\metre}\), le rayon de la Lune \(r\) est alors~:
\[r=\frac{R}{3,5}=\frac{6333}{3,5}=\SI{1809}{\kilo\metre}\]
Soit, par rapport à la valeur admise actuellement de \SI{1738}{\kilo\metre}, une erreur de 4\%.

\section{La distance Terre-Lune}
A partir de la valeur du rayon de la Terre, la mesure de la distance Terre-Lune est aisée. Il faut considérer que l'angle \(\alpha\) sous lequel on voit la lune est d'un demi-degré, soit en radian \(\pi/360=\SI{0,0087}{\radian}\). Alors, à l'aide d'une simple relation liant la longueur d'un arc \(L\) à son angle \(\alpha\) au centre en radian et au rayon \(R\) du cercle, on tire~:
\begin{align*}
L&=\alpha\cdot R\;\Rightarrow\\
R&=\frac{L}{\alpha}=\frac{\phi_{lune}}{\alpha}\\
R&=d_{Terre-Lune}=\frac{3618}{0,0087}=\SI{415862}{\kilo\metre}
\end{align*}
Ce qui représente environ 8\% d'écart avec la valeur d'aujourd'hui.

\medskip
De nos jours, la distance de la Terre à la Lune est mesuré par la réflexion de faisceaux laser sur des miroirs déposés par les missions Apollo\index{Apollo}. Elle atteint une précision de l'ordre de quelques millimètres.

\section{La distance Terre-Soleil}
La mesure de distances plus importantes que celle de la Terre à la Lune fut réalisée par la suite à l'aide de la méthode de la parallaxe\index{parallaxe}. On peut évoquer ici cette méthode qui nécessite une observation en deux points différents de la Terre. Une variante de cette méthode sera utilisée pour la mesure des distances aux étoiles.

La mesure de la parallaxe repose sur l'idée suivante. Quand on observe son pouce levé devant soi à bout de bras avec l'\oe il droit puis l'\oe il gauche alternativement, on voit qu'il se déplace par rapport aux objets éloignés. Si le pouce est proche des yeux, il se déplace fortement et s'il est éloigné, il se déplace faiblement. L'observation de son mouvement permet donc de se faire une idée de la distance entre les yeux et le pouce.

En termes astronomiques, on voit sur la figure \ref{parallaxemars} que l'angle \(\alpha\) entre une étoile lointaine et la Polaire\index{Polaire} est le même pour deux points d'observation \(C\) et \(P\) sur la Terre. Pour un astre peu éloigné \(M\), ces deux angles \(\beta\) et \(\gamma\) sont différents. On appelle parallaxe\index{parallaxe} de l'astre \(M\) l'angle \(\delta\) qui vaut~:
\[\delta=\gamma-\beta\]
en raison du fait que deux droites parallèles sont toujours coupées par une troisième droite selon deux angles égaux.

L'astronome Cassini, qui détermina pour la première fois la distance Terre-Soleil à partir de la parallaxe de Mars, décrit la mesure ainsi~:
\begin{quotation}
``\textit{La meilleure méthode pour chercher la parallaxe de Mars par la correspondance des observations faites à Paris \& en Caïenne auroit été d'observer, par la lunette, la conjonction précise de cette planète avec une étoile fixe. Car si cette conjonction avoit été vue de l'un \& de l'autre lieu au même instant \& précisément de la même manière sans aucune distance, c'eût été une marque qu'il n'y avoit point de parallaxe sensible. S'il y en avoit eu quelque peu, à l'instant que Mars auroit paru toucher par son bord supérieur une Etoile fixe en Caïenne, il auroit paru à Paris un peu éloigné de la même Etoile vers l'Horizon, \& quand il auroit paru à Paris toucher l'Etoile par son bord inférieur, il auroit paru en Caïenne éloigné de la même Etoile vers le Zénit \& cette distance vue d'un lieu \& non pas de l'autre, aurait été attribuée à la parallaxe}'' J. D. Cassini, dans ``Mémoires de l'Académie Royale des Sciences'', volume 8, année 1730.\endnote{\url=http://www.iap.fr/InformationCommunication/ArticlesGrandPublic/Etoiles/Transit/transit_parallaxe_mars_1672.html=}
\end{quotation}

\begin{figure}[ht]
\centering
\caption[Parallaxe de Mars]{Parallaxe de Mars\label{parallaxemars} \par \scriptsize{Première étape pour déterminer la distance Terre-Soleil.}}
\input{Annexe-MesuresDistances/Images/Parallaxe.pst}
\end{figure}

L'application de cette méthode pour déterminer la position d'un astre comme Mars\index{Mars}, \(M\) sur le schéma \ref{parallaxemars}, consiste à observer cette planète simultanément depuis deux endroits éloignés l'un de l'autre à la surface de la terre. Deux observateurs \(P\) et \(C\) (les astronomes Cassini\index{Cassini} à Paris et Richer\index{Richer} à Cayenne, en 1672) mesurent au même moment l'écart angulaire entre Mars M et une étoile E en arrière plan. La somme des deux angles mesurés constitue l'angle \(\delta\) au sommet du triangle \(PMC\), soit la parallaxe\index{parallaxe}. En raison de l'important éloignement de Mars par rapport à la distance \(PC\) (Paris-Cayenne\index{Paris-Cayenne}), on peut poser grâce à la relation \ref{relationdarc}, page \pageref{relationdarc}~:
\[\delta=\frac{PC}{MO}\;\Rightarrow\;MO=\frac{PC}{\delta}\]
La distance \(PC\), approximativement la distance entre Paris et Cayenne, permet alors de trouver \(MO\). La distance de la Terre à Mars vaut alors~:
\begin{equation}\label{dtm}
d_{T-M}=MO+OT=MO+R_T
\end{equation}
où \(R_T\) est le rayon de la Terre.

\smallskip
Le résultat est donné par Cassini\index{Cassini} lui-même~:
\begin{quotation}
``\textit{Le 5 septembre 1672, trois jours avant l'opposition du Soleil à Mars, nous observâmes à Paris trois Etoiles dans l'Eau Aquarius marquées par Bayerus \(\Psi\), vers lesquelles Mars alloit par son mouvement particulier rétrograde, de sorte que l'on jugeoit qu'il en auroit pu cacher une. Il étoit alors un peu plus septentrional que la plus septentrionale des trois. On prit la hauteur Méridienne de celle-ci qui passoit la première; \& celle de la moyenne vers laquelle le mouvement particulier de Mars s'adressoit. Par le choix des Observations les plus exactes \& les plus conformes entre elles, on fixa à 15" la parallaxe que fait Mars de Paris à Caïenne}'' J. D. Cassini, dans ``Mémoires de l'Académie Royale des Sciences'', volume 8, année 1730.\endnote{\url=http://www.iap.fr/InformationCommunication/ArticlesGrandPublic/Etoiles/Transit/transit_parallaxe_mars_1672.html=}
\end{quotation}
Le résultat de la mesure est donc de quinze secondes d'arc. Mais attention, il s'agit de la parallaxe qui est la moitié de l'angle \(\delta\). Celui-ci vaut donc~: \(\delta=0,008332^{\circ}\) ou \SI{1,454e-4}{\radian}. Avec une distance de Paris à Cayenne de \SI{7082,1}{\kilo\metre} cela donne~:
\[MO=\frac{7,0821\cdot 10^6}{1,454\cdot 10^{-4}}=\SI{4,87e10}{\metre}\]
Soit une distance entre la Terre et Mars de~:
\[d_{T-M}=4,87\cdot 10^{10}+6,371\cdot 10^6=\SI{4,87e10}{\metre}\]
Mais ce n'est là que la distance de la Terre à Mars. Il fallait encore réaliser une condition de mesure pour obtenir la distance Terre-Soleil~: choisir le bon moment. Cassini précise que la mesure fut faite trois jours avant l'opposition\index{opposition} du Soleil à Mars. Cela signifie qu'alors la Terre se trouvait sur une même ligne entre Mars et le Soleil. A ce moment, et seulement à ce moment, on peut écrire~:
\begin{equation}\label{opposition}
d_{S-M}=d_{T-M}+d_{S-T}
\end{equation}
où on n'a pas tenu compte du caractère elliptique des orbites\index{orbite!elliptique}, notamment de celle de Mars (voir ci-dessous). Mais, l'équation \ref{opposition} a deux inconnues~: les distances Mars-Soleil et Terre-Soleil. Pour les déterminer, il faut une seconde équation.

\smallskip
Il s'agit de la troisième loi de Kepler\index{Kepler!troisième loi}, donnée par l'équation \ref{keplertroisieme}, page \pageref{keplertroisieme}, appliquée au cas de la Terre et de Mars~:
\begin{equation}\label{oppositionkepler}
\frac{d_{S-T}^3}{T_T^2}=\frac{d_{S-M}^3}{T_M^2}
\end{equation}
où \(d_{S-T}\) et \(d_{S-M}\) sont les distances Soleil-Terre et Soleil-Mars et \(T_T\) et \(T_M\) leur périodes respectives.

Le système composé des équations \ref{opposition} et \ref{oppositionkepler} est un système de deux équations à deux inconnues. Pour le résoudre, remplaçons la distance \(d_{S-M}\) de l'équation \ref{opposition} dans la troisième loi de Kepler \ref{oppositionkepler}~:
\begin{align}
\frac{d_{S-T}^3}{T_T^2}&=\frac{d_{S-M}^3}{T_M^2}\;\Rightarrow\nonumber\\
d_{S-T}^3\frac{T_M^2}{T_T^2}&=d_{S-M}^3\;\Rightarrow\nonumber\\
d_{S-T}^3(\frac{T_M}{T_T})^2&=(d_{T-M}+d_{S-T})^3\;\Rightarrow\nonumber\\
d_{S-T}(\frac{T_M}{T_T})^{2/3}&=d_{T-M}+d_{S-T}\;\Rightarrow\nonumber\\
d_{S-T}(\frac{T_M}{T_T})^{2/3}-d_{S-T}&=d_{T-M}\;\Rightarrow\nonumber\\
d_{S-T}((\frac{T_M}{T_T})^{2/3}-1)&=d_{T-M}\;\Rightarrow\nonumber\\
d_{S-T}&=\frac{d_{T-M}}{(T_M/T_T)^{2/3}-1}\label{cassiniua}
\end{align}
Numériquement, avec les période de Mars et de la Terre \(T_T=365\,j\) et \(T_M=686\,j\), on obtient~:
\[d_{S-T}=\frac{4,87\cdot 10^{10}}{(686/365)^{2/3}-1}=\SI{9,31e10}{\metre}\]
Ce qui représente un écart de 38\% par rapport à la valeur de l'unité astronomique connue actuellement~: \(d_{T-S}=\SI{1,496e11}{\metre}\). Cet écart est important. La moitié de celui-ci peut être attribuée à l'excentricité\index{excentricite@excentricité} de Mars. En effet, un calcul\footnote{En opposition, le demi-grand axe de l'orbite de Mars s'exprime en réalité par~: \(a_M=e\cdot a_M+d_{S-T}+d_{T-M}\). La grandeur \(e\cdot a_M\) représentant la distance du centre de l'ellipse\index{ellipse} de Mars au foyer\index{foyer} sur lequel se trouve le Soleil. On ne calcule plus alors dans l'équation de Kepler la distance \(d_{S-M}\), mais le demi-grand axe \(a_M\).} basé sur l'hypothèse d'une orbite terrestre circulaire, mais d'une orbite elliptique\index{orbite!elliptique} de Mars mène au résultat suivant~:
\begin{align}
d_{S-T}&=\frac{d_{T-M}}{(1-e)(T_M/T_T)^{2/3}-1}\label{cassiniuaexcentrique}\\
&=\frac{4,87\cdot 10^{10}}{(1-0,093)(686/365)^{2/3}-1}\nonumber\\
&=\SI{1,27e11}{\metre}\nonumber
\end{align}
où \(e\) est l'excentricité\index{excentricite@excentricité} de l'orbite de Mars. La valeur obtenue à l'aide de l'équation \ref{cassiniuaexcentrique} ne représente plus alors qu'un écart de 15\%.

\section{La distance des étoiles}
On a vu que la parallaxe\index{parallaxe} de Mars est d'environ \angle{;;15} d'arc. Cette valeur est vraiment très petite. Il est donc impossible d'effectuer une mesure de la parallaxe d'une étoile à l'aide de la méthode utilisée pour Mars. Deux observations simultanées en deux endroits différents de la Terre ne permettent pas une telle mesure. Par la méthode de la parallaxe, la seule grandeur qu'il est possible de modifier est la distance entre les deux points d'observation. Comme des distances de l'ordre du rayon de la Terre ne suffisent pas, un effet de parallaxe plus important fut obtenu en effectuant la mesure à six mois d'intervalle. Ainsi, la distance entre les deux ``points de vue'' correspond au diamètre de l'orbite terrestre. La première mesure de la parallaxe d'une étoile (parallaxe stellaire\index{parallaxe!stellaire}) a été faite en 1838 par Friedrich Wilhelm Bessel\index{Bessel} pour la binaire 61 du Cygne. Mais, même pour une telle distance, les parallaxes d'étoiles restent inférieures à la seconde d'arc. Par exemple, pour Proxima du Centaure\index{Proxima du Centaure}, l'étoile la plus proche de nous, la parallaxe vaut 760 millisecondes d'arc.