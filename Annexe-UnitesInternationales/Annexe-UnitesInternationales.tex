\myclearpage

\twocolumn

\chapter[Systèmes d'unités]{Systèmes d'unités}\label{SI}

\section{Introduction}

\lettrine{L}{es unités} sont des \og objets\fg{} d'une importance capitale pour exprimer la valeur d'une grandeur. À chacune d'elle peut correspondre beaucoup d'unités. C'est parfaitement compréhensible étant donné la variété des domaines auxquels elles s'appliquent et à priori il est naturel que chacun définisse les unités qui lui sont les plus pratiques. Mais, deux problèmes se posent alors. Le premier tient dans le fait que plus le nombre d'unités est grand, plus il est difficile de les faire correspondre entre elles. Le second tient dans le fait qu'effectuer des calculs complexes génère des unités complexes. On se heurte donc à des problèmes de \emph{conversion} et à des problèmes de \emph{construction} des unités. C'est pour régler ces deux types de problèmes qu'a été inventé le \emph{Système International d'unités}.

\section{Opérateur d'unités}

\medskip
Auparavant, voyons comment se construisent les unités. On n'envisage pas ici de décrire comment les unités de base ont été définies historiquement. Leur histoire est intéressante, mais complexe. Pour le mètre, par exemple, on pourra consulter \cite{AK05} ou \cite{AS06}.

La construction des unités dérivées des unités de base se fait à l'aide de l'opérateur d'unités [\dots] (il s'agit des ``crochets'') dont la signification est \emph{\og l'unité de~\dots\fg{}}. Ainsi, écrire~:
\[[L]=\si{\metre}\]
signifie que l'unité de la grandeur \(L\) est le mètre. Il en va de même avec des unités dérivées comme~:
\[[a]=\si{\metre\per\square\second}\]
qui signifie que l'unité de la grandeur \(a\) est le mètre par seconde au carré.

\smallskip
On peut relever à cette occasion que cette notation permet de savoir comment reporter les unités sur les axes d'un graphique. En effet, de manière générale, on note une grandeur \(X\) ainsi~:
\begin{equation}\label{express_unite}
X=x\,[X]
\end{equation}

où \(X\) est la grandeur, \(x\) sa valeur et \([X]\) son unité. Or, comme ce sont les valeurs d'une grandeur qu'on reporte en regard de l'axe d'un graphique, on peut isoler la valeur de l'équation \ref{express_unite}~:
\[x=X/[X]\]
pour trouver l'expression à mettre à côté de l'axe. Ainsi, pour un axe correspondant à une longueur, il faudrait mettre~:
\[\uparrow\,L/\si{\metre}\]
Logiquement, on voit qu'il ne vient pas de crochets autour de l'unité, comme cela est parfois pratiqué. De plus, il faut mettre un symbole de division entre la grandeur et l'unité.

\smallskip
Venons en maintenant à la construction des unités. Commençons par des expressions connues comme la vitesse. Pour déterminer les unités à associer à une vitesse, il faut connaître son expression en termes de grandeurs de base, c'est-à-dire dont les unités sont simples~:
\[v=\frac{d}{t}\]
et en prendre les unités à l'aide de l'opérateur d'unités~:
\[[v]=[\frac{v}{t}]=\frac{[v]}{[t]}=\si{\metre\per\second}\]
On voit que lorsque l'opérateur [\dots] s'applique sur une fraction, il s'applique à chacun de ses termes. On voit aussi la correspondance entre la division des grandeurs et l'expression ``par'' qui lie les deux unités de base de la vitesse.

Pour des expression plus complexes, cette ``algèbre'' des unité s'applique aussi. Ainsi, les unités de la force sont obtenues à partir de la seconde loi de Newton~:
\[[F]=[m\cdot a]=[m]\cdot [a]=\si{\kilo\gram\metre\per\square\second}=\si{\newton}\]
Mais, des situations plus complexes peuvent se présenter. Pour la période d'un pendule, on a par exemple~:
\begin{align*}
[T]&=[2\cdot \pi\cdot \sqrt{\frac{L}{g}}]=[2]\cdot [\pi]\cdot \sqrt{\frac{[L]}{[g]}}\\
&=\sqrt{\frac{\si{\metre}}{\si{\metre\per\second\squared}}}=\sqrt{\si{\metre}\cdot \frac{\si{\second\squared}}{\si{\metre}}}\\
&=\sqrt{\si{\second\squared}}=\si{\second}
\end{align*}
On voit qu'appliquer l'opérateur d'unité à une racine revient à l'appliquer aux termes de la racine et que diviser une unité par une fraction de deux autres revient à la multiplier par l'inverse de cette fraction, comme en algèbre ordinaire. Ainsi aussi, la racine du carré d'une unité correspond-elle à l'unité elle-même.

Ainsi, par la suite nous rencontrerons des combinaisons d'unités s'appuyant sur des opérations arithmétiques et il sera possible de les manipuler en tant que telles. C'est pourquoi nous dirons que l'opérateur d'unité est un opérateur algébrique. Nous allons voir qu'il est possible de se livrer aussi à une étude des lois du point de vue de leurs unités à travers ce qu'on nomme une \emph{analyse dimensionnelle}.

\section{Analyse dimensionnelle}

Pour comprendre les relations entretenues par différentes grandeurs, plusieurs méthodes sont à notre disposition. Les deux principales sont la déduction mathématique utilisée à partir des axiomes d'une théorie et l'induction expérimentale. L'analyse dimensionnelle est un outil supplémentaire permettant la vérification des lois. On entend par analyse dimensionnelle l'étude des dimensions des grandeurs impliquées, c'est-à-dire l'analyse de leurs unités. Par exemple, si on envisage la force centripète F exercée par la corde qui retient un objet en rotation, on peut écrire~:
\[F=m\cdot \frac{v}{R}\]
où m est la masse de l'objet, v sa vitesse et R le rayon du cercle parcouru. En effet, intuitivement, la force doit être forte pour une grande masse et une vitesse importante. Par contre, en imaginant une voiture qui prend un virage, on s'imagine bien que c'est pour un rayon petit que cette force doit être grande. D'où la forme de la relation proposée.

Mais, cette relation est-elle correcte ? Pour en savoir plus procédons à une analyse dimensionnelle. La seconde loi de Newton nous permet d'écrire~:
\[[F]=[m]\cdot [a]=\si{\kilogram\metre\per\second\squared}\]
D'autre part, le terme de droite s'écrit~:
\[[m]\cdot \frac{[v]}{[R]}=\si{\kilo\gram}\cdot \frac{\si{\metre\per\second}}{\si{\metre}}=\si{\kilo\gram\per\second}\]
On a donc~:
\[\si{\kilogram\metre\per\second\squared\neq\kilogram\per\second}\]
et on peut en déduire que la relation est fausse, uniquement sur la base d'une analyse dimensionnelle.

La correction est assez facile à trouver. Comme il faut l'inverse d'un temps au carré et que le temps n'apparaît que dans la vitesse, on peut essayer~:
\[F=m\cdot \frac{v^2}{R}\]
Et l'analyse dimensionnelle~:
\[\si{\kilogram\metre\per\second\squared}=\si{\kilogram}\cdot \frac{\si{\metre\squared\per\second\squared}}{\si{\metre}}\]
confirme alors l'exactitude de cette relation.

\smallskip
Un tel outil est très puissant et si simple à utiliser qu'il est important de le faire systématiquement.

\section{Les unités du Système International}
Nous avons dit précédemment que le Système International d'unités\index{Système!International d'unités} (SI\index{SI}) a sa raison d'être, non pas dans l'uniformisation (qui n'a pas de sens véritable puisqu'à chaque type de problème un système d'unité adéquat doit être choisi pour simplifier la représentation numérique) mais dans la simplification des calculs. En effet, tous les calculs effectués dans ce système sont prévus (au niveau des constantes utilisées) pour donner des résultats dont les unités restent dans ce système.

\subsection{Exemple}

Par exemple, on peut calculer la période \(T\) d'un pendule simple de longueur \(L\) grâce à l'équation suivante~:
\begin{equation*}
T=2\cdot \pi \sqrt{\frac{L}{g}}
\end{equation*}
Pour un pendule de longueur \(L=\SI{2}{\foot}\), on peut alors calculer la période~:
\begin{equation*}
T=2\cdot \pi \sqrt{\frac{2}{9,81}}=\SI{2,837}{?}
\end{equation*}

\begin{table*}[ht]
\centering
\caption{Les unités du Système International}
\label{AL}
\begin{tabular}{|l|c||c|c||c|}
\hline 
Grandeur & Symbole & Nom unité & Symbole & Unités de base\\
\hline
\hline 
Longueur & L & mètre & \si{\metre} & \si{\metre}\\
\hline 
Masse & m & kilogramme & \si{\kilo\gram} & \si{\kilo\gram}\\
\hline 
Temps & t & seconde & \si{\second} & \si{\second}\\
\hline 
Température & T & kelvin & \si{\kelvin} & \si{\kelvin}\\
\hline 
Quantité de matière & n & mole & \si{\mole} & \si{\mole}\\
\hline 
Angle & \(\alpha\) & radian & \si{\radian} & -\\
\hline 
Fréquence & f & hertz & \si{\hertz} & \si{\per\second}\\
\hline 
Force & F & newton & \si{\newton} & \si{\kilo\gram\metre\per\second\squared}\\
\hline 
Énergie, travail & E, A & joule & \si{\joule} & \si{\kilo\gram\metre\squared\per\second\squared}\\
\hline 
Puissance & P & watt & \si{\watt} & \si{\kilo\gram\metre\squared\per\second\cubed}\\
\hline 
\end{tabular}
\end{table*}

La question est de savoir quelle unité attribuer au résultat. Formellement, on peut écrire en utilisant l'opérateur d'unités~:
\begin{equation*}
[T]=[2]\cdot [\pi]\cdot \sqrt{\frac{[L]}{[g]}}=\sqrt{\frac{\si{\foot}}{\si{\metre\per\square\second}}}
\end{equation*}
Il est évident que ces unités sont complexes et que le résultat ne peut être exprimé ainsi dans une unité connue. En n'utilisant pas un système cohérent d'unités, il est nécessaire de faire suivre l'ensemble des calculs réalisés par leur équivalent en terme d'unités afin de savoir quelles sont les unités à attribuer aux résultats. Quand les grandeurs utilisées sont complexes, cela devient vite très pénible.

Le ``Système International d'unités'' est là pour y remédier. En effet, en travaillant exclusivement avec des grandeurs exprimées en unités internationales, on peut garantir que le résultat s'exprimera dans l'unité internationale correspondant à sa grandeur. Ainsi, pour une longueur \(L=\SI{2}{\metre}\), l'exemple précédent donne le résultat suivant~:
\begin{equation*}
T=2\cdot \pi\cdot \sqrt{\frac{2}{9,81}}=\SI{2,837}{\second}
\end{equation*}
puisque l'unité de temps du Système International est la seconde. Et en effet, comme vu précédemment, on peut écrire~:
\begin{equation*}
[T]=[2]\cdot [\pi]\cdot \sqrt{\frac{[L]}{[g]}}=\sqrt{\frac{\si{\metre}}{\si{\metre\per\square\second}}}=\sqrt{\si{\second\squared}}=\si{\second}
\end{equation*}

\smallskip
Ainsi, on peut imaginer une grandeur issue d'un calcul faisant intervenir les deux grandeurs suivantes~: une force et une masse. Si ce calcul se fait à partir de ces deux grandeurs exprimées dans les unités du système international, dans le cas présent des newtons (\si{\newton}) pour la force et des kilogrammes (\si{\kilogram}) pour la masse, alors le résultat est forcément exprimé dans les unités du Système International. Comme il s'agit ici d'une accélération, ces unités sont des \si{\metre\per\square\second}.

\section{Conversions\index{conversion}}

Les unités\index{unité} de la table \ref{grandeurs} ne font pas partie du Système International mais restent utiles~:

\begin{table}[ht]
\centering
\caption{Conversions d'unités}
\label{grandeurs}
\begin{tabular}{|lcc|}
\hline 
\textbf{Longueur} & & \textbf{\'Equivalent SI}\\
\hline
\hline 
\SI{1}{\si{\angstrom}} (angstr\oe m) & = & \SI{1e-10}{\metre}\\
\hline 
\SI{1}{\si{\micro}} (micron) & = & \SI{1e-6}{\micro\metre}\\
\hline 
\SI{1}{\inch} (pouce) & = & \SI{2,54e-2}{\metre}\\
\hline 
\SI{1}{\foot} (pied) = \SI{12}{\inch} & = & \SI{0,3048}{\metre}\\
\hline
\SI{1}{\astronomicalunit} (unité astro.) & = & \SI{1,496e11}{\metre}\\
\hline
\SI{1}{\lightyear} (année lumière) & = & \SI{9,4607e15}{\metre}\\
\hline
\SI{1}{\parsec} (parsec) & = & \SI{3,0857e16}{\metre}\\
\hline
\end{tabular}
\end{table}

\medskip{}
L'unité astronomique correspond à la longueur du demi-grand axe de l'orbite terrestre.

Le parsec\index{parsec} est la distance à laquelle \SI{1}{\astronomicalunit}\index{UA} est vue sous un angle de \SI{1}{\arcsecond} (une seconde\index{seconde!d'arc}) d'arc. Comme \SI{1}{\degree} est divisé en \SI{60}{\arcminute} (minutes\index{minute d'arc}) d'arc et \SI{1}{\arcminute} d'arc en \SI{60}{\arcsecond}, une seconde d'arc (\SI{1}{\arcsecond}) représente \SI{1/3600}{\degree}. Pour calculer ce que vaut \SI{1}{\parsec}\index{pc}, il faut une relation entre la distance réelle L de \SI{1}{\astronomicalunit} et l'angle \(\alpha\) (\SI{1}{\arcsecond}) sous lequel cette distance est vue. Cette relation est (voir figure \ref{arc}): 

\begin{equation}\label{relationdarc}
L=\alpha\cdot R
\end{equation}

\begin{figure}[ht]
\centering
\caption{Relation de l'arc de cercle\label{arc}}
\includegraphics{Arc.eps}
\end{figure}

où \(R\) est le rayon de l'arc de cercle de longueur \(L\) et d'angle au centre \(\alpha\). Mais, attention, \(\alpha\) doit être en radians. Or, comme \SI{180}{\degree}=\SI{\pi}{\radian}, \SI{1}{\degree}=\SI{\pi}{\radian}/\SI{180}{\radian} et \SI{1}{\arcsecond}=\SI{\pi}{\radian}/(\SI[output-product = \cdot]{180 x 3600}{\radian}). Ainsi on obtient la valeur du parsec~:
\begin{align*}
R&=\frac{L}{\alpha}=\frac{L\cdot 180\cdot 3600}{\pi}\\
&=\frac{1,496\cdot10^{11}\cdot 180\cdot 3600}{\pi}=\SI{3,0857e16}{\metre}
\end{align*}

\begin{table}[ht]
\centering
\begin{tabular}{|ccc|}
\hline 
\textbf{Volume} & & \textbf{\'Equivalent SI}\\
\hline
\hline 
Litre~: \SI{1}{\litre}=\SI{1}{\deci\cubic\metre} & = & \SI{1e-3}{\cubic\metre}\\
\hline
\end{tabular}

\bigskip{}

\begin{tabular}{|ccc|}
\hline 
\textbf{Énergie} & & \textbf{\'Equivalent SI}\\
\hline
\hline 
Calorie~: \SI{1}{\calorie} & = & \SI{4,186}{\joule}\\
\hline 
\SI{1}{\kilo\watt\hour} & = & \SI{3,6e6}{\joule}\\
\hline
\end{tabular}

\bigskip{}

\begin{tabular}{|ccc|}
\hline 
\textbf{Puissance} & & \textbf{\'Equivalent SI}\tabularnewline
\hline
\hline 
Cheval-vapeur~: \SI{1}{\horsepower} & = & \SI{736}{\watt}\\
\hline
\end{tabular}

\bigskip{}

\begin{tabular}{|ccc|}
\hline 
\textbf{Température} & & \textbf{\'Equivalent SI}\\
\hline
\hline 
\SI{0}{\celsius} & = & \SI{273,15}{\kelvin}\\
\hline
\end{tabular}

\caption{Quelques équivalents}

\end{table}

\section{Sous-multiples}

On trouvera dans la table \ref{prefixes} les principales notations pour les multiples\index{multiple} et les sous-multiples\index{sous-multiple}. Ces notations sont bien évidemment liées à la notation scientifique\index{notation@notation!scientifique}. Elle est aussi liée à un autre type de notation, dite notation d'ingénieur\index{notation@notation!d'ingénieur}, qu'il faut mentionner au moins une fois. En effet, si cette notation est relativement peu utilisée hors des cercles d'ingénieurs, elle est assez souvent présente sur les machines à calculer. Pour qu'elle ne pose pas de problèmes, il est donc plus nécessaire de savoir ne pas l'activer que de savoir l'utiliser. En fait, c'est une notation scientifique, dont les exposants du facteur 10 sont des multiples de 3. Ainsi, par exemple, les mètres (\(10^{0}\)) et les millimètres (\(10^{-3}\)) sont utilisés dans cette notation, mais pas les centimètres (\(10^{-2}\)).

\begin{table}[ht]
\centering
\begin{tabular}{|c|c|c|}
\hline 
\textbf{Préfixe} & \textbf{Symbole} & \textbf{Facteur}\\
\hline
\hline 
peta & P & \(10^{15}\)\\
\hline 
téra & T & \(10^{12}\)\\
\hline 
giga & G & \(10^{9}\)\\
\hline 
méga & M & \(10^{6}\)\\
\hline 
kilo & k & \(10^{3}\)\\
\hline 
hecto & h & \(10^{2}\)\\
\hline 
déca & da & \(10^{1}\)\\
\hline 
- & - & -\\
\hline 
déci & d & \(10^{-1}\)\\
\hline 
centi & c & \(10^{-2}\)\\
\hline 
milli & m & \(10^{-3}\)\\
\hline 
micro & \(\mu\) & \(10^{-6}\)\\
\hline 
nano & n & \(10^{-9}\)\\
\hline 
pico & p & \(10^{-12}\)\\
\hline 
femto & f & \(10^{-15}\)\\
\hline
\end{tabular}

\caption{Multiples et sous-multiples}

\label{prefixes}

\end{table}

\section{Notation scientifique\index{notation@notation!scientifique}}

A ne pas confondre avec la notation d'ingénieur\index{notation@notation!d'ingénieur} (par multiples de \(10^3\)), la notation scientifique~: \(\cdot10^{x}\), ou x est un nombre entier positif ou négatif, peut être utilisée sur une machine à calculer à l'aide de la touche \fbox{EXP}\index{EXP} ou \fbox{EE}\index{EE}~. Notez que l'affichage peut alors donner, par exemple~:
\begin{center}
\fbox{\hspace*{2cm}5~E~2} ou même \fbox{\hspace*{2cm}\(5\,^{2}\)}
\end{center}
pour \(5\cdot10^{2}\), sans marquer le \(10\) et avec un \(2\) sur la même ligne d'affichage que le \(5\). Ce type d'affichage pose des problème de compréhension et génère des fautes de calcul. Par exemple, pour calculer~:
\[(10^{-10})^2\]
l'erreur courante est de taper~: 10~\fbox{EE}~-10. L'affichage donne alors malheureusement~:
\begin{center}
\fbox{\hspace*{2cm}10~E~-10}
\end{center}
ce qui est trompeur puisque cela signifie \(10\cdot 10^{-10}\) et non \(10^{-10}\), c'est-à-dire \(1\cdot 10^{-10}\). Pour cela, il aurait fallu taper~: 1~\fbox{EE}~-10 et l'affichage aurait donné~:
\begin{center}
\fbox{\hspace*{2cm}1~E~-10}
\end{center}

\section{Règles de calcul}
Remarquons encore les règles mathématiques très utiles suivantes (voir annexe \ref{exos}):

\[10^{a}\cdot10^{b}=10^{a+b}\;\;\text{et}\;\;\frac{1}{10^{a}}=10^{-a}\]

Elles permettent de réaliser de tête et rapidement des calculs complexes comme~:

\begin{align*}
F&=9\cdot 10^9\cdot \frac{2\cdot 10^{-3}\cdot 2\cdot 10^{-2}}{(2\cdot 10^4)^2}\\
&=9\cdot \frac{2\cdot 2}{4}\cdot 10^9\cdot 10^{-3}\cdot 10^{-2}\cdot ((10^4)^2)^{-1}\\
&=9\cdot 10^{9-3-2}\cdot 10^{2\cdot (-4)}=9\cdot 10^4\cdot 10^{-8}\\
&=9\cdot 10^{-4}\;N
\end{align*}